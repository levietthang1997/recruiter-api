package com.chechen.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author thanglv on 4/10/2022
 * @project recruiter-api
 */
public class DateUtils {
    public static LocalDateTime toDate(String date) {
        if (date == null) {
            return null;
        }
        var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        return LocalDateTime.parse(date, formatter);
    }

    public static String fromDate(LocalDateTime date) {
        if (date == null) {
            return null;
        }
        var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        return formatter.format(date);
    }

    public static LocalDateTime toDate(String date, String format) {
        if (date == null) {
            return null;
        }
        var formatter = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.parse(date, formatter);
    }

    public static String fromDate(LocalDateTime date, String format) {
        if (date == null) {
            return null;
        }
        var formatter = DateTimeFormatter.ofPattern(format);
        return formatter.format(date);
    }
}
