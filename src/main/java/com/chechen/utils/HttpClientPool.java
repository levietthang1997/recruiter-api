package com.chechen.utils;

import com.chechen.exception.BusinessException;
import org.apache.http.HttpHeaders;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.http.HttpStatus;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class HttpClientPool {
    private static final PoolingHttpClientConnectionManager POOLING_CONN_MANAGER = new PoolingHttpClientConnectionManager();

    static {
        POOLING_CONN_MANAGER.setMaxTotal(20_000);
        POOLING_CONN_MANAGER.setDefaultMaxPerRoute(15_000);
    }

    private static final CloseableHttpClient client = HttpClients
            .custom().setConnectionManager(POOLING_CONN_MANAGER)
            .build();

    public static String sendPostJson(String request, String urlApi, int timeout, String contentType) throws IOException, BusinessException {
        HttpPost httpPost = new HttpPost(urlApi);
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(timeout)
                .setConnectTimeout(timeout)
                .setSocketTimeout(timeout)
                .build();

        StringEntity stringEntity = new StringEntity(request, StandardCharsets.UTF_8);
        httpPost.setConfig(requestConfig);
        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, contentType);
        httpPost.setEntity(stringEntity);
        StringBuilder response = new StringBuilder();
        InputStreamReader isr = null;
        BufferedReader in = null;
        OutputStream out = null;
        try (
                CloseableHttpResponse httpResponse = client.execute(httpPost);
        ) {
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.OK.value())
                throw new BusinessException("Call http failed");
            InputStream inputStream = httpResponse.getEntity().getContent();
            isr = new InputStreamReader(inputStream);
            in = new BufferedReader(isr);
            String value = null;
            while ((value = in.readLine()) != null) {
                response.append(value);
            }

            in.close();
            return response.toString();
        } finally {

            if (out != null) {
                try {
                    out.close();
                } catch (IOException var26) {
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException var25) {
                }
            }

            if (in != null) {
                try {
                    in.close();
                } catch (IOException var24) {
                }
            }
        }
    }

    public static String sendGet(String urlApi, int timeout, String contentType) throws IOException, BusinessException {
        HttpGet httpGet = new HttpGet(urlApi);
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(timeout)
                .setConnectTimeout(timeout)
                .setSocketTimeout(timeout)
                .build();

        httpGet.setConfig(requestConfig);
        httpGet.setHeader(HttpHeaders.CONTENT_TYPE, contentType);
        StringBuilder response = new StringBuilder();
        InputStreamReader isr = null;
        BufferedReader in = null;
        OutputStream out = null;
        try (
                CloseableHttpResponse httpResponse = client.execute(httpGet);
        ) {
            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.OK.value())
                throw new BusinessException("Call http failed");
            InputStream inputStream = httpResponse.getEntity().getContent();
            isr = new InputStreamReader(inputStream);
            in = new BufferedReader(isr);
            String value = null;
            while ((value = in.readLine()) != null) {
                response.append(value);
            }

            in.close();
            return response.toString();
        } finally {

            if (out != null) {
                try {
                    out.close();
                } catch (IOException var26) {
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException var25) {
                }
            }

            if (in != null) {
                try {
                    in.close();
                } catch (IOException var24) {
                }
            }
        }
    }
}
