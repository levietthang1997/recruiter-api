package com.chechen.repository;

import com.chechen.entity.JobInfo;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author thanglv on 26/06/2022
 * @project recruiter-api
 */
@Repository
public interface JobInfoPaginationRepository extends PagingAndSortingRepository<JobInfo, String> {
    // đối với fetch join thì cần phải có count query
    @Query(value = "SELECT DISTINCT a FROM JobInfo a LEFT JOIN FETCH a.levels LEFT JOIN FETCH a.locations LEFT JOIN FETCH a.skills LEFT JOIN FETCH a.companyProfile e LEFT JOIN FETCH e.accountInfo f LEFT JOIN FETCH e.benefitDetails LEFT JOIN FETCH e.gallery LEFT JOIN FETCH e.companyIndustries WHERE (:title IS NULL OR a.title LIKE CONCAT('%',:title,'%')) AND (:approveStatus IS NULL OR a.approveStatus = :approveStatus) AND (:jobType IS NULL OR a.jobType LIKE CONCAT('%',:jobType,'%')) AND a.isDeleted = :isDeleted AND f.keycloakUserId = :userId")
    List<JobInfo> findAllCustom(Pageable pageable, @Param("title") String title, @Param("approveStatus") String approveStatus, @Param("jobType") String jobType, @Param("isDeleted") String isDeleted, @Param("userId") String userId);

    @Query(value = "SELECT COUNT(DISTINCT a), a FROM JobInfo a JOIN FETCH a.levels JOIN FETCH a.locations JOIN FETCH a.skills JOIN FETCH a.companyProfile e JOIN FETCH e.accountInfo f WHERE (:title IS NULL OR a.title LIKE CONCAT('%',:title,'%')) AND (:approveStatus IS NULL OR a.approveStatus = :approveStatus) AND (:jobType IS NULL OR a.jobType LIKE CONCAT('%',:jobType,'%')) AND a.isDeleted = :isDeleted AND f.keycloakUserId = :userId")
    long findCustomCount(@Param("title") String title, @Param("approveStatus") String approveStatus, @Param("jobType") String jobType, @Param("isDeleted") String isDeleted, @Param("userId") String userId);
}
