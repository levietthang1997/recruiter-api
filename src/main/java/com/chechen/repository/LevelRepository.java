package com.chechen.repository;

import com.chechen.entity.Level;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Repository
public interface LevelRepository extends JpaRepository<Level, String> {
    Set<Level> findAllByIdInAndIsDeleted(Set<String> lstId, String isDeleted);
    List<Level> findAllByIsDeleted(String isDeleted);
}
