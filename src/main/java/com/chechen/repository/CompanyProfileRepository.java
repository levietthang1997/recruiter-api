package com.chechen.repository;

import com.chechen.entity.CompanyProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Repository
public interface CompanyProfileRepository extends JpaRepository<CompanyProfile, String> {

    @Query("SELECT a FROM CompanyProfile a LEFT JOIN FETCH a.companyIndustries LEFT JOIN FETCH a.benefitDetails c LEFT JOIN FETCH a.gallery d LEFT JOIN FETCH a.accountInfo e LEFT JOIN FETCH a.jobs f WHERE e.keycloakUserId = ?1")
    Optional<CompanyProfile> findByAccountInfoKeycloakUserId(String keycloakUserId);

    @Query(value = "SELECT * FROM company_profile", nativeQuery = true)
    List<CompanyProfile> findAllCustom();
}
