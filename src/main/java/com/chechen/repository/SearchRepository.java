package com.chechen.repository;

import com.chechen.dto.PaginationDto;
import com.chechen.services.vm.BaseObjPagination;

import javax.validation.constraints.NotNull;

/**
 * @author thanglv on 07/04/2022
 * @project recruiter-api
 */
public interface SearchRepository<T, E extends BaseObjPagination> {
    PaginationDto<T> searchData(@NotNull E objSearch, Class<T> entityClass);
}
