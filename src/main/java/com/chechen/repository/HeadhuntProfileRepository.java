package com.chechen.repository;

import com.chechen.entity.HeadhuntProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Repository
public interface HeadhuntProfileRepository extends JpaRepository<HeadhuntProfile, String> {
}
