package com.chechen.repository;

import com.chechen.entity.CompanyBenefitDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
@Repository
public interface CompanyBenefitDetailRepository extends JpaRepository<CompanyBenefitDetail, String> {
}
