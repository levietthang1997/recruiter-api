package com.chechen.repository;

import com.chechen.entity.Industry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
@Repository
public interface IndustryRepository extends JpaRepository<Industry, String> {
    Optional<Industry> findByIdAndIsDeleted(String id, String isDeleted);
    Set<Industry> findAllByIdInAndIsDeleted(Set<String> lstId, String isDeleted);
    Set<Industry> findAllByIsDeleted(String isDeleted);
    Set<Industry> findAllByIdIn(List<String> lstId);
}
