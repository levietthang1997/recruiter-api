package com.chechen.repository;

import com.chechen.entity.JobRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface JobRoleRepository extends JpaRepository<JobRole, String> {
    List<JobRole> findAllByIsDeleted(String isDeleted);

    Set<JobRole> findAllByIdInAndIsDeleted(Set<String> jobRoles, String strN);
}
