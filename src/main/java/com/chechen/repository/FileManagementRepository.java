package com.chechen.repository;

import com.chechen.entity.FileManagement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Repository
public interface FileManagementRepository extends JpaRepository<FileManagement, String> {
    long countByIdAndCreatedByAndIsDeleted(String fileId, String userId, String isDeleted);
    Optional<FileManagement> findByIdAndCreatedByAndIsDeleted(String fileId, String userId, String isDeleted);
    Set<FileManagement> findAllByIdInAndIsDeleted(List<String> fileId, String isDeleted);
}
