package com.chechen.repository;

import com.chechen.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface LocationRepository extends JpaRepository<Location, String> {
    Set<Location> findAllByIdInAndIsDeleted(Set<String> lstId, String isDeleted);
    List<Location> findAllByIsDeleted(String isDeleted);
}
