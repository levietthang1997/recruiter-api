package com.chechen.repository;

import com.chechen.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface SkillRepository extends JpaRepository<Skill, String> {
    Set<Skill> findAllByIdInAndIsDeleted(Set<String> lstId, String isDeleted);
    List<Skill> findAllByIsDeleted(String isDeleted);
}
