package com.chechen.repository;

import com.chechen.entity.AccountInfo;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Repository
public interface AccountInfoRepository extends JpaRepository<AccountInfo, String> {
    @Query("SELECT a FROM AccountInfo a LEFT JOIN FETCH  a.companyProfile b LEFT JOIN FETCH b.companyIndustries LEFT JOIN FETCH b.benefitDetails LEFT JOIN FETCH b.gallery LEFT JOIN FETCH b.accountInfo LEFT JOIN FETCH  a.headhuntProfile c WHERE a.keycloakUserId = ?1 AND a.isDeleted = ?2")
    Optional<AccountInfo> findByKeycloakUserId(String keycloakUserId, String isDeleted);

    long countAccountInfoByPhone(String phone);
}
