package com.chechen.repository;

import com.chechen.entity.CvInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CvInformationRepository extends JpaRepository<CvInformation, String> {
}
