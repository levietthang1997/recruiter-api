package com.chechen.repository;

import com.chechen.entity.FileUploadPolicy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Repository
public interface FileUploadPolicyRepository extends JpaRepository<FileUploadPolicy, String> {
    Optional<FileUploadPolicy> findByExt(String ext);
}
