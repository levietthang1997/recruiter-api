package com.chechen.repository;

import com.chechen.entity.HeadhuntBrand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HeadhuntBrandRepository extends JpaRepository<HeadhuntBrand, String> {
}
