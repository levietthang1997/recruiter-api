package com.chechen.repository;

import com.chechen.entity.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface LanguageRepository extends JpaRepository<Language, String> {
    List<Language> findAllByIsDeleted(String strN);
    Set<Language> findAllByIdInAndIsDeleted(Set<String> headhuntBrandLanguage, String strN);
}
