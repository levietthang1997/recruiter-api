package com.chechen.repository;

import com.chechen.entity.PendingTask;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author thanglv on 08/06/2022
 * @project recruiter-api
 */
@Repository
public interface PendingTaskRepository extends JpaRepository<PendingTask, String>, PagingAndSortingRepository<PendingTask, String> {
    Page<PendingTask> findAllByIsDeleteAndWfStatusCode(Pageable pageable, String isDeleted, String status);

    Page<PendingTask> findAllByIsDelete(Pageable pageable, String isDeleted);

    @Query(value = "SELECT a.* FROM pending_task a INNER JOIN JOB_INFO b ON a.string1 = b.id INNER JOIN company_profile c ON c.id = b.fk_company_profile_id WHERE a.is_deleted = :isDeleted AND a.wf_status_code = :wfStatusCode AND c.id = :companyId ORDER BY a.created_date ASC LIMIT :pageSize OFFSET :pageNumber", nativeQuery = true)
    List<PendingTask> findAllByIsDeleteAndWfStatusCodeAndCompanyId(String isDeleted, String wfStatusCode, String companyId, int pageSize, int pageNumber);

    @Query(value = "SELECT a.* FROM pending_task a INNER JOIN JOB_INFO b ON a.string1 = b.id INNER JOIN company_profile c ON c.id = b.fk_company_profile_id WHERE a.is_deleted = :isDeleted AND a.wf_status_code = :wfStatusCode AND c.id = :companyId ORDER BY a.created_date ASC LIMIT :pageSize OFFSET :pageNumber", nativeQuery = true)
    List<PendingTask> findAllByIsDeleteAndCompanyId(String isDeleted, String companyId, int pageSize, int pageNumber);

    @Query(value = "SELECT count(a.id) FROM pending_task a INNER JOIN JOB_INFO b ON a.string1 = b.id INNER JOIN company_profile c ON c.id = b.fk_company_profile_id WHERE a.is_deleted = :isDeleted AND a.wf_status_code = :wfStatusCode AND c.id = :companyId", nativeQuery = true)
    long countByIsDeleteAndWfStatusCodeAndCompanyId(String isDeleted, String wfStatusCode, String companyId);

    @Query(value = "SELECT count(a.id) FROM pending_task a INNER JOIN JOB_INFO b ON a.string1 = b.id INNER JOIN company_profile c ON c.id = b.fk_company_profile_id WHERE a.is_deleted = :isDeleted AND c.id = :companyId", nativeQuery = true)
    long countByIsDeleteAndCompanyId(String isDeleted, String companyId);

    Optional<PendingTask> findByString1(String jobId);
}
