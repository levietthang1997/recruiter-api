package com.chechen.repository;

import com.chechen.entity.JobCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface JobCategoryRepository extends JpaRepository<JobCategory, String> {
    List<JobCategory> findAllByIsDeleted(String isDeleted);
    Set<JobCategory> findAllByIdInAndIsDeleted(Set<String> lstId, String isDeleted);
}
