package com.chechen.repository;

import com.chechen.entity.JobInfo;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author thanglv on 26/03/2022
 * @project recruiter-api
 */
@Repository
public interface JobInfoRepository extends JpaRepository<JobInfo, String> {

    @EntityGraph(attributePaths = {"skills", "locations", "levels", "companyProfile"})
    Optional<JobInfo> findByIdAndIsDeleted(String id, String isDeleted);

    @EntityGraph(attributePaths = {"skills", "locations", "levels", "companyProfile"})
    Optional<JobInfo> findByIdAndApproveStatusAndIsDeleted(String id, String status, String isDeleted);

    @EntityGraph(attributePaths = {"skills", "locations", "levels", "companyProfile"})
    List<JobInfo> findAllByIsDeletedAndApproveStatusOrderByPriorityJobDesc(Pageable pageable, String isDeleted, String approveStatus);

    @EntityGraph(attributePaths = {"skills", "locations", "levels", "companyProfile"})
    List<JobInfo> findAllByIsDeletedAndApproveStatusAndCompanyProfileIdOrderByPriorityJobDesc(Pageable pageable, String isDeleted, String approveStatus, String companyProfileId);

    @Query(value = "SELECT SUM(a.vacancies) FROM JobInfo a INNER JOIN  a.companyProfile b WHERE b.id = ?1 AND a.isDeleted = ?2 ")
    long sumAllVacanciesByCompanyProfileIdAndIsDeleted(String companyId, String isDeleted);
}
