package com.chechen.repository;

import com.chechen.entity.CpBenefit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
@Repository
public interface CompanyBenefitRepository extends JpaRepository<CpBenefit, String> {
    Optional<CpBenefit> findByIdAndIsDeleted(String id, String isDeleted);
    Set<CpBenefit> findAllByIdInAndIsDeleted(List<String> lstId, String isDeleted);
    Set<CpBenefit> findAllByIsDeleted(String isDeleted);
}
