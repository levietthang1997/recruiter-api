package com.chechen.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * @author thanglv on 27/06/2022
 * @project recruiter-api
 */
@Data
public class CompanyProfileDtoV2 implements Serializable {
    private String id;
    private String companyName;
    private String fileLogoId;
    private String viewLinkLogo;
    private Set<CompanyBenefitDetailDto> benefitDetails;
}
