package com.chechen.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class HeadhuntProfileDto implements Serializable {
    private String id;
    private String avatarId;
    private String avatarUrl;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String hotline;
    private String address;
    private String gender;
    private String linkedIn;
    private String introduction;
}
