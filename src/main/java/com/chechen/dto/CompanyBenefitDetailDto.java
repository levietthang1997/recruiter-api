package com.chechen.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
@Data
public class CompanyBenefitDetailDto implements Serializable {
    private String benefitId;
    private String benefitName;
    private String icon;
    private String detail;
}
