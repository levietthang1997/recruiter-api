package com.chechen.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author thanglv on 30/06/2022
 * @project recruiter-api
 */
@Data
public class PendingTaskDto implements Serializable {
    private String id;
    // lưu job id cần phê duyệt
    private String string1;
    // Lưu tên job cần phê duyệt
    private String string2;
    // trạng thái mã code
    private String wfStatusCode;
    private String createDate;
    private String type;
}
