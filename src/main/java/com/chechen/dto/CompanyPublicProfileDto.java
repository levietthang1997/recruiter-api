package com.chechen.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CompanyPublicProfileDto implements Serializable {
    private String id;
    private String companyName;
    private String website;
    private String companyAddress;
    private String viewLinkThumbnail;
    private Integer companySize;
    private Long vacancies;
    private String description;
    private List<JobInfoDtoV2> popularJob;
}
