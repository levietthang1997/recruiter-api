package com.chechen.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
public class CompanyProfileDto implements Serializable {
    private String id;
    private String companyName;
    private String companyPhone;
    private String companyAddress;
    private Integer companySize;
    private String companyEmail;
    private String ccMail;
    private String website;
    private List<IndustryDto> companyIndustries;
    private Integer workingHours;
    private Set<CompanyBenefitDetailDto> benefitDetails;
    private String fileLogoId;
    private String viewLinkLogo;
    private String description;
    private List<FileManagementDto> gallery;
    private String thumbnailLogoId;
    private String viewLinkThumbnail;
    private String thumbnailDescription;
    private String linkedIn;
    private String facebook;
    private String firstNameRepresentative;
    private String lastNameRepresentative;
    private String headline;
    private String emailRepresentative;
    private String phoneRepresentative;
}
