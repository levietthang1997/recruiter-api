package com.chechen.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author thanglv on 27/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
public class JobInfoDtoV2 implements Serializable {
    private String id;
    private String title;
    private List<SkillDto> skills;
    private BigDecimal salaryFrom;
    private BigDecimal salaryTo;
    private BigDecimal reward;
    private List<LocationDto> locations;
    private CompanyProfileDtoV2 companyProfile;
}
