package com.chechen.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
public class CpBenefitDto implements Serializable {
    private String name;
    private String icon;
    private String id;
}
