package com.chechen.dto;

import lombok.Data;
import java.io.Serializable;

@Data
public class FileUploadPolicyDto implements Serializable {
    private String id;
    private String ext;
    private Long maxUploadSize;
}
