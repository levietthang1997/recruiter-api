package com.chechen.dto;

import lombok.Data;
import java.io.Serializable;

@Data
public class JobRoleDto implements Serializable {
    private String id;
    private String name;
}
