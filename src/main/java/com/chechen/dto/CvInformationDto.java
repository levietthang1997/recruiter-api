package com.chechen.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CvInformationDto implements Serializable {
    private String id;
}
