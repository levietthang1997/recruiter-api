package com.chechen.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class MsCvDetectionDto implements Serializable {
    private String name;
    private String phone;
    private String email;
    private Set<String> website;
    private String hiddenResume;
}
