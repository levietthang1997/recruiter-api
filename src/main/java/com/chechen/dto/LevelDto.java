package com.chechen.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Data
public class LevelDto implements Serializable {
    private String id;
    private String name;
}
