package com.chechen.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class LanguageDto implements Serializable {
    private String id;
    private String name;
}
