package com.chechen.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author thanglv on 05/06/2022
 * @project recruiter-api
 */
@NoArgsConstructor
@Data
public class LocationDto implements Serializable {
    private String id;
    private String name;
}
