package com.chechen.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
public class IndustryDto implements Serializable {
    private String id;
    private String name;
}
