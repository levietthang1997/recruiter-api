package com.chechen.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CvDetectionDto implements Serializable {
    private String fileIdOriginal;
    private String fileIdHidden;
    private String name;
    private String phone;
    private String email;
    private Set<String> website;
}
