package com.chechen.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

/**
 * @author thanglv on 26/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
public class JobInfoDto implements Serializable {

    private String id;

    private String isPlatform;

    private String title;

    private Set<SkillDto> skills;

    // Có thoả thuận mức lương hay không , 1 là có, 0 là Không
    private String isNegotiableSalary;

    // mức lương max
    private BigDecimal salaryTo;

    // mức lương min
    private BigDecimal salaryFrom;

    // số lượng vị trí còn trống muốn tuyển (đang trống 50 => muốn tuyển thêm 50)
    private Integer vacancies;

    // số lượng thành viên của nhóm trong công ty hiện có
    private Integer teamSize;

    // phòng ban
    private String department;

    // loại job it, non it, high level
    private String jobType;

    // nơi làm việc (tỉnh thành phố) Hà Nội, Singapore...

    private Set<LocationDto> locations;

    // địa chỉ chi tiết
    private String address;

    // mô tả qua về job
    private String jobOverView;

    // yêu cầu về kỹ năng
    private String jobRequirement;

    // những kỹ năng được ưu tiên
    private String prioritySkill;

    // lý do tại sao nên làm ở công ty
    private String whyWorkingHere;

    // quá trình phỏng vấn
    private String interviewingProcess;

    // loại thời gian làm việc (fulltime, partime, other)
    private String employeeType;

    // level
    private Set<LevelDto> levels;

    // Thông báo cho headhunter
    private String noticeToHeadhunter;

    private String isDeleted;
}
