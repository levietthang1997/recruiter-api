package com.chechen.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
public class FileManagementDto implements Serializable {
    private String id;
    private String name;
    private String viewLink;
}
