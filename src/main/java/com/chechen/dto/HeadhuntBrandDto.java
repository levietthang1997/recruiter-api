package com.chechen.dto;

import com.chechen.entity.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
public class HeadhuntBrandDto implements Serializable {
    private String id;
    private String brandLogoId;
    private String viewLinkBrandLogo;
    private String name;
    private String phone;
    private String teamLeadEmail;
    private String address;
    private String ccMail;

    // contact
    private String contactFirstName;
    private String contactLastName;
    private String contactTitle;
    private String contactPhone;
    private String contactEmail;

    // profile
    private String message;
    private Set<JobRoleDto> jobRoles;

    // level
    private Set<LevelDto> levels;

    // ngành nghề mà hr am hiểu
    private Set<IndustryDto> headhuntBrandIndustries;

    // kỹ năng mà hr am hiểu của job (C++, Java, ...)
    private Set<SkillDto> skills;

    // ngôn ngữ thành thạo
    private Set<LanguageDto> headhuntBrandLanguage;

    // khu vực tuyển dụng
    private Set<LocationDto> locations;

    private String linkedIn;
    private String facebook;
    private String skype;
}
