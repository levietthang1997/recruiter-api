package com.chechen.mapper;

import com.chechen.dto.FileUploadPolicyDto;
import com.chechen.entity.FileUploadPolicy;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FileUploadPolicyMapper {
    FileUploadPolicyDto toDto(FileUploadPolicy fileUploadPolicy);
    List<FileUploadPolicyDto> toLstDto(List<FileUploadPolicy> fileUploadPolicyList);
}
