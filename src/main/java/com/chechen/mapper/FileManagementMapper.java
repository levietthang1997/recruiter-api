package com.chechen.mapper;

import com.chechen.dto.FileManagementDto;
import com.chechen.entity.FileManagement;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Mapper(componentModel = "spring")
public interface FileManagementMapper {
    FileManagementDto toDto(FileManagement fileManagement);
    List<FileManagementDto> toListDto(List<FileManagement> fileManagementList);
}
