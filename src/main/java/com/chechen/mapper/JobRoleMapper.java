package com.chechen.mapper;

import com.chechen.dto.JobRoleDto;
import com.chechen.dto.LevelDto;
import com.chechen.dto.PaginationDto;
import com.chechen.entity.JobRole;
import com.chechen.entity.Level;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface JobRoleMapper {
    JobRoleDto toDto(JobRole jobRole);
    List<JobRoleDto> toListDto(List<JobRole> jobRoles);
    PaginationDto<JobRoleDto> paginationToDto(PaginationDto<JobRole> result);
}
