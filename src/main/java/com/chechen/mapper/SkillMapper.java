package com.chechen.mapper;

import com.chechen.dto.PaginationDto;
import com.chechen.dto.SkillDto;
import com.chechen.entity.Skill;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SkillMapper {

    PaginationDto<SkillDto> paginationToDto(PaginationDto<Skill> paginationSkill);

    List<SkillDto> toListDto(List<Skill> all);
}
