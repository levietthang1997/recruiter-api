package com.chechen.mapper;

import com.chechen.dto.LanguageDto;
import com.chechen.dto.PaginationDto;
import com.chechen.entity.Language;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LanguageMapper {
    LanguageDto toDto(Language language);
    PaginationDto<LanguageDto> paginationToDto(PaginationDto<Language> result);
    List<LanguageDto> toListDto(List<Language> languageList);
}
