package com.chechen.mapper;

import com.chechen.dto.IndustryDto;
import com.chechen.entity.Industry;
import org.mapstruct.Mapper;

import java.util.Set;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
@Mapper(componentModel = "spring")
public interface IndustryMapper {
    Set<IndustryDto> toListDto(Set<Industry> industryList);
}
