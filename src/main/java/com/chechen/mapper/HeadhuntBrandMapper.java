package com.chechen.mapper;

import com.chechen.dto.HeadhuntBrandDto;
import com.chechen.entity.HeadhuntBrand;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface HeadhuntBrandMapper {
    HeadhuntBrandDto toDto(HeadhuntBrand headhuntBrand);
}
