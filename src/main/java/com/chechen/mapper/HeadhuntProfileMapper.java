package com.chechen.mapper;

import com.chechen.dto.HeadhuntProfileDto;
import com.chechen.entity.HeadhuntProfile;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface HeadhuntProfileMapper {
    HeadhuntProfileDto toDto(HeadhuntProfile headhuntProfile);
}
