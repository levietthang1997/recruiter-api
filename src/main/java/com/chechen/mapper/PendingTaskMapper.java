package com.chechen.mapper;

import com.chechen.dto.PendingTaskDto;
import com.chechen.entity.PendingTask;
import org.mapstruct.Mapper;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author thanglv on 30/06/2022
 * @project recruiter-api
 */
@Mapper(
        componentModel = "spring"
)
public interface PendingTaskMapper {


    default List<PendingTaskDto> toListDto(List<PendingTask> pendingTaskList) {
        if ( pendingTaskList == null ) {
            return null;
        }

        List<PendingTaskDto> list = new ArrayList<PendingTaskDto>( pendingTaskList.size() );
        for ( PendingTask pendingTask : pendingTaskList ) {
            list.add( pendingTaskToPendingTaskDto( pendingTask ) );
        }

        return list;
    }

    default PendingTaskDto pendingTaskToPendingTaskDto(PendingTask pendingTask) {
        if ( pendingTask == null ) {
            return null;
        }

        PendingTaskDto pendingTaskDto = new PendingTaskDto();
        pendingTaskDto.setId( pendingTask.getId() );
        pendingTaskDto.setString1( pendingTask.getString1() );
        pendingTaskDto.setString2( pendingTask.getString2() );
        pendingTaskDto.setWfStatusCode( pendingTask.getWfStatusCode() );
        if ( pendingTask.getCreateDate() != null ) {
            pendingTaskDto.setCreateDate( DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").format( pendingTask.getCreateDate() ) );
        }
        pendingTaskDto.setType( pendingTask.getType() );

        return pendingTaskDto;
    }
}
