package com.chechen.mapper;

import com.chechen.dto.LevelDto;
import com.chechen.dto.PaginationDto;
import com.chechen.entity.Level;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Mapper(componentModel = "spring")
public interface LevelMapper {
    PaginationDto<LevelDto> paginationToDto(PaginationDto<Level> result);

    List<LevelDto> toListDto(List<Level> all);
}
