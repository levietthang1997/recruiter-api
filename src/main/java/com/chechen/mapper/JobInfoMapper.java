package com.chechen.mapper;

import com.chechen.dto.*;
import com.chechen.entity.*;
import org.mapstruct.Mapper;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author thanglv on 26/06/2022
 * @project recruiter-api
 */
@Mapper(componentModel = "spring")
public interface JobInfoMapper {

    default List<JobInfoDto> toDtoList(List<JobInfo> jobInfoList) {
        if ( jobInfoList == null ) {
            return null;
        }

        List<JobInfoDto> list = new ArrayList<JobInfoDto>( jobInfoList.size() );
        for ( JobInfo jobInfo : jobInfoList ) {
            list.add( jobInfoToJobInfoDto( jobInfo ) );
        }

        return list;
    }

    default List<JobInfoDtoV2> toDtoV2List(List<JobInfo> jobInfoList) {
        if ( jobInfoList == null ) {
            return null;
        }

        List<JobInfoDtoV2> list = new ArrayList<JobInfoDtoV2>( jobInfoList.size() );
        for ( JobInfo jobInfo : jobInfoList ) {
            list.add( jobInfoToJobInfoDtoV2( jobInfo ) );
        }

        return list;
    }

    default JobInfoDtoV3 toDtoV3(JobInfo jobInfo) {
        if ( jobInfo == null ) {
            return null;
        }

        JobInfoDtoV3 jobInfoDtoV3 = new JobInfoDtoV3();

        jobInfoDtoV3.setId( jobInfo.getId() );
        jobInfoDtoV3.setIsPlatform( jobInfo.getIsPlatform() );
        jobInfoDtoV3.setTitle( jobInfo.getTitle() );
        jobInfoDtoV3.setSkills( skillSetToSkillDtoSet( jobInfo.getSkills() ) );
        jobInfoDtoV3.setIsNegotiableSalary( jobInfo.getIsNegotiableSalary() );
        jobInfoDtoV3.setSalaryTo( jobInfo.getSalaryTo() );
        jobInfoDtoV3.setSalaryFrom( jobInfo.getSalaryFrom() );
        jobInfoDtoV3.setVacancies( jobInfo.getVacancies() );
        jobInfoDtoV3.setTeamSize( jobInfo.getTeamSize() );
        jobInfoDtoV3.setDepartment( jobInfo.getDepartment() );
        jobInfoDtoV3.setJobType( jobInfo.getJobType() );
        jobInfoDtoV3.setLocations( locationSetToLocationDtoSet( jobInfo.getLocations() ) );
        jobInfoDtoV3.setAddress( jobInfo.getAddress() );
        jobInfoDtoV3.setJobOverView( jobInfo.getJobOverView() );
        jobInfoDtoV3.setJobRequirement( jobInfo.getJobRequirement() );
        jobInfoDtoV3.setPrioritySkill( jobInfo.getPrioritySkill() );
        jobInfoDtoV3.setWhyWorkingHere( jobInfo.getWhyWorkingHere() );
        jobInfoDtoV3.setInterviewingProcess( jobInfo.getInterviewingProcess() );
        jobInfoDtoV3.setEmployeeType( jobInfo.getEmployeeType() );
        jobInfoDtoV3.setLevels( levelSetToLevelDtoSet( jobInfo.getLevels() ) );
        jobInfoDtoV3.setNoticeToHeadhunter( jobInfo.getNoticeToHeadhunter() );
        jobInfoDtoV3.setIsDeleted( jobInfo.getIsDeleted() );
        jobInfoDtoV3.setCompanyProfile( companyProfileToCompanyProfileDto( jobInfo.getCompanyProfile() ) );
        jobInfoDtoV3.setReward( jobInfo.getReward() );
        jobInfoDtoV3.setPriorityJob( jobInfo.getPriorityJob() );
        jobInfoDtoV3.setApproveStatus( jobInfo.getApproveStatus() );

        return jobInfoDtoV3;
    }

    default JobInfoDtoV4 toDtoV4(JobInfo jobInfo) {
        if ( jobInfo == null ) {
            return null;
        }

        JobInfoDtoV4 jobInfoDtoV4 = new JobInfoDtoV4();

        jobInfoDtoV4.setId( jobInfo.getId() );
        jobInfoDtoV4.setTitle( jobInfo.getTitle() );
        jobInfoDtoV4.setSkills( skillSetToSkillDtoSet( jobInfo.getSkills() ) );
        jobInfoDtoV4.setIsNegotiableSalary( jobInfo.getIsNegotiableSalary() );
        jobInfoDtoV4.setSalaryTo( jobInfo.getSalaryTo() );
        jobInfoDtoV4.setSalaryFrom( jobInfo.getSalaryFrom() );
        jobInfoDtoV4.setVacancies( jobInfo.getVacancies() );
        jobInfoDtoV4.setTeamSize( jobInfo.getTeamSize() );
        jobInfoDtoV4.setDepartment( jobInfo.getDepartment() );
        jobInfoDtoV4.setJobType( jobInfo.getJobType() );
        jobInfoDtoV4.setLocations( locationSetToLocationDtoSet( jobInfo.getLocations() ) );
        jobInfoDtoV4.setAddress( jobInfo.getAddress() );
        jobInfoDtoV4.setJobOverView( jobInfo.getJobOverView() );
        jobInfoDtoV4.setJobRequirement( jobInfo.getJobRequirement() );
        jobInfoDtoV4.setPrioritySkill( jobInfo.getPrioritySkill() );
        jobInfoDtoV4.setWhyWorkingHere( jobInfo.getWhyWorkingHere() );
        jobInfoDtoV4.setEmployeeType( jobInfo.getEmployeeType() );
        jobInfoDtoV4.setLevels( levelSetToLevelDtoSet( jobInfo.getLevels() ) );
        jobInfoDtoV4.setCompanyProfile( companyProfileToCompanyProfileDtoV2( jobInfo.getCompanyProfile() ) );
        jobInfoDtoV4.setReward( jobInfo.getReward() );
        jobInfoDtoV4.setPriorityJob( jobInfo.getPriorityJob() );

        return jobInfoDtoV4;
    }

    default SkillDto skillToSkillDto(Skill skill) {
        if ( skill == null ) {
            return null;
        }

        SkillDto skillDto = new SkillDto();

        skillDto.setId( skill.getId() );
        skillDto.setName( skill.getName() );

        return skillDto;
    }

    default Set<SkillDto> skillSetToSkillDtoSet(Set<Skill> set) {
        if ( set == null ) {
            return null;
        }

        Set<SkillDto> set1 = new LinkedHashSet<SkillDto>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Skill skill : set ) {
            set1.add( skillToSkillDto( skill ) );
        }

        return set1;
    }

    default LocationDto locationToLocationDto(Location location) {
        if ( location == null ) {
            return null;
        }

        LocationDto locationDto = new LocationDto();

        if ( location.getId() != null ) {
            locationDto.setId( location.getId() );
        }
        locationDto.setName( location.getName() );

        return locationDto;
    }

    default Set<LocationDto> locationSetToLocationDtoSet(Set<Location> set) {
        if ( set == null ) {
            return null;
        }

        Set<LocationDto> set1 = new LinkedHashSet<LocationDto>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Location location : set ) {
            set1.add( locationToLocationDto( location ) );
        }

        return set1;
    }

    default LevelDto levelToLevelDto(Level level) {
        if ( level == null ) {
            return null;
        }

        LevelDto levelDto = new LevelDto();

        levelDto.setId( level.getId() );
        levelDto.setName( level.getName() );

        return levelDto;
    }

    default Set<LevelDto> levelSetToLevelDtoSet(Set<Level> set) {
        if ( set == null ) {
            return null;
        }

        Set<LevelDto> set1 = new LinkedHashSet<LevelDto>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Level level : set ) {
            set1.add( levelToLevelDto( level ) );
        }

        return set1;
    }

    default JobInfoDto jobInfoToJobInfoDto(JobInfo jobInfo) {
        if ( jobInfo == null ) {
            return null;
        }

        JobInfoDto jobInfoDto = new JobInfoDto();

        jobInfoDto.setId( jobInfo.getId() );
        jobInfoDto.setIsPlatform( jobInfo.getIsPlatform() );
        jobInfoDto.setTitle( jobInfo.getTitle() );
        jobInfoDto.setSkills( skillSetToSkillDtoSet( jobInfo.getSkills() ) );
        jobInfoDto.setIsNegotiableSalary( jobInfo.getIsNegotiableSalary() );
        jobInfoDto.setSalaryTo( jobInfo.getSalaryTo() );
        jobInfoDto.setSalaryFrom( jobInfo.getSalaryFrom() );
        jobInfoDto.setVacancies( jobInfo.getVacancies() );
        jobInfoDto.setTeamSize( jobInfo.getTeamSize() );
        jobInfoDto.setDepartment( jobInfo.getDepartment() );
        jobInfoDto.setJobType( jobInfo.getJobType() );
        jobInfoDto.setLocations( locationSetToLocationDtoSet( jobInfo.getLocations() ) );
        jobInfoDto.setAddress( jobInfo.getAddress() );
        jobInfoDto.setJobOverView( jobInfo.getJobOverView() );
        jobInfoDto.setJobRequirement( jobInfo.getJobRequirement() );
        jobInfoDto.setPrioritySkill( jobInfo.getPrioritySkill() );
        jobInfoDto.setWhyWorkingHere( jobInfo.getWhyWorkingHere() );
        jobInfoDto.setInterviewingProcess( jobInfo.getInterviewingProcess() );
        jobInfoDto.setEmployeeType( jobInfo.getEmployeeType() );
        jobInfoDto.setLevels( levelSetToLevelDtoSet( jobInfo.getLevels() ) );
        jobInfoDto.setNoticeToHeadhunter( jobInfo.getNoticeToHeadhunter() );
        jobInfoDto.setIsDeleted( jobInfo.getIsDeleted() );

        return jobInfoDto;
    }

    default List<SkillDto> skillSetToSkillDtoList(Set<Skill> set) {
        if ( set == null ) {
            return null;
        }

        List<SkillDto> list = new ArrayList<SkillDto>( set.size() );
        for ( Skill skill : set ) {
            list.add( skillToSkillDto( skill ) );
        }

        return list;
    }

    default List<LocationDto> locationSetToLocationDtoList(Set<Location> set) {
        if ( set == null ) {
            return null;
        }

        List<LocationDto> list = new ArrayList<LocationDto>( set.size() );
        for ( Location location : set ) {
            list.add( locationToLocationDto( location ) );
        }

        return list;
    }

    default CompanyBenefitDetailDto companyBenefitDetailToCompanyBenefitDetailDto(CompanyBenefitDetail companyBenefitDetail) {
        if ( companyBenefitDetail == null ) {
            return null;
        }

        CompanyBenefitDetailDto companyBenefitDetailDto = new CompanyBenefitDetailDto();

        companyBenefitDetailDto.setDetail( companyBenefitDetail.getDetail() );
        companyBenefitDetailDto.setBenefitId(companyBenefitDetail.getBenefit().getId());
        companyBenefitDetailDto.setBenefitName(companyBenefitDetail.getBenefit().getName());
        companyBenefitDetailDto.setIcon(companyBenefitDetail.getBenefit().getIcon());

        return companyBenefitDetailDto;
    }

    default Set<CompanyBenefitDetailDto> companyBenefitDetailSetToCompanyBenefitDetailDtoSet(Set<CompanyBenefitDetail> set) {
        if ( set == null ) {
            return null;
        }

        Set<CompanyBenefitDetailDto> set1 = new LinkedHashSet<CompanyBenefitDetailDto>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( CompanyBenefitDetail companyBenefitDetail : set ) {
            set1.add( companyBenefitDetailToCompanyBenefitDetailDto( companyBenefitDetail ) );
        }

        return set1;
    }

    default CompanyProfileDtoV2 companyProfileToCompanyProfileDtoV2(CompanyProfile companyProfile) {
        if ( companyProfile == null ) {
            return null;
        }

        CompanyProfileDtoV2 companyProfileDtoV2 = new CompanyProfileDtoV2();

        companyProfileDtoV2.setId( companyProfile.getId() );
        companyProfileDtoV2.setCompanyName( companyProfile.getCompanyName() );
        companyProfileDtoV2.setFileLogoId( companyProfile.getFileLogoId() );
        companyProfileDtoV2.setViewLinkLogo( companyProfile.getViewLinkLogo() );
        companyProfileDtoV2.setBenefitDetails( companyBenefitDetailSetToCompanyBenefitDetailDtoSet( companyProfile.getBenefitDetails() ) );

        return companyProfileDtoV2;
    }

    default JobInfoDtoV2 jobInfoToJobInfoDtoV2(JobInfo jobInfo) {
        if ( jobInfo == null ) {
            return null;
        }

        JobInfoDtoV2 jobInfoDtoV2 = new JobInfoDtoV2();

        jobInfoDtoV2.setId( jobInfo.getId() );
        jobInfoDtoV2.setTitle( jobInfo.getTitle() );
        jobInfoDtoV2.setSkills( skillSetToSkillDtoList( jobInfo.getSkills() ) );
        jobInfoDtoV2.setSalaryFrom( jobInfo.getSalaryFrom() );
        jobInfoDtoV2.setSalaryTo( jobInfo.getSalaryTo() );
        jobInfoDtoV2.setReward( jobInfo.getReward() );
        jobInfoDtoV2.setLocations( locationSetToLocationDtoList( jobInfo.getLocations() ) );
        jobInfoDtoV2.setCompanyProfile( companyProfileToCompanyProfileDtoV2( jobInfo.getCompanyProfile() ) );

        return jobInfoDtoV2;
    }

    default IndustryDto cpIndustryToCpIndustryDto(Industry industry) {
        if ( industry == null ) {
            return null;
        }

        IndustryDto industryDto = new IndustryDto();

        industryDto.setId( industry.getId() );
        industryDto.setName( industry.getName() );

        return industryDto;
    }

    default List<IndustryDto> cpIndustrySetToCpIndustryDtoList(Set<Industry> set) {
        if ( set == null ) {
            return null;
        }

        List<IndustryDto> list = new ArrayList<IndustryDto>( set.size() );
        for ( Industry industry : set ) {
            list.add( cpIndustryToCpIndustryDto(industry) );
        }

        return list;
    }

    default FileManagementDto fileManagementToFileManagementDto(FileManagement fileManagement) {
        if ( fileManagement == null ) {
            return null;
        }

        FileManagementDto fileManagementDto = new FileManagementDto();

        fileManagementDto.setId( fileManagement.getId() );
        fileManagementDto.setName( fileManagement.getName() );
        fileManagementDto.setViewLink( fileManagement.getViewLink() );

        return fileManagementDto;
    }

    default List<FileManagementDto> fileManagementSetToFileManagementDtoList(Set<FileManagement> set) {
        if ( set == null ) {
            return null;
        }

        List<FileManagementDto> list = new ArrayList<FileManagementDto>( set.size() );
        for ( FileManagement fileManagement : set ) {
            list.add( fileManagementToFileManagementDto( fileManagement ) );
        }

        return list;
    }

    default CompanyProfileDto companyProfileToCompanyProfileDto(CompanyProfile companyProfile) {
        if ( companyProfile == null ) {
            return null;
        }

        CompanyProfileDto companyProfileDto = new CompanyProfileDto();

        companyProfileDto.setId( companyProfile.getId() );
        companyProfileDto.setCompanyName( companyProfile.getCompanyName() );
        companyProfileDto.setCompanyPhone( companyProfile.getCompanyPhone() );
        companyProfileDto.setCompanyAddress( companyProfile.getCompanyAddress() );
        companyProfileDto.setCompanySize( companyProfile.getCompanySize() );
        companyProfileDto.setCompanyEmail( companyProfile.getCompanyEmail() );
        companyProfileDto.setCcMail( companyProfile.getCcMail() );
        companyProfileDto.setWebsite( companyProfile.getWebsite() );
        companyProfileDto.setCompanyIndustries( cpIndustrySetToCpIndustryDtoList( companyProfile.getCompanyIndustries() ) );
        companyProfileDto.setWorkingHours( companyProfile.getWorkingHours() );
        companyProfileDto.setBenefitDetails( companyBenefitDetailSetToCompanyBenefitDetailDtoSet( companyProfile.getBenefitDetails() ) );
        companyProfileDto.setFileLogoId( companyProfile.getFileLogoId() );
        companyProfileDto.setViewLinkLogo( companyProfile.getViewLinkLogo() );
        companyProfileDto.setDescription( companyProfile.getDescription() );
        companyProfileDto.setGallery( fileManagementSetToFileManagementDtoList( companyProfile.getGallery() ) );
        companyProfileDto.setThumbnailLogoId( companyProfile.getThumbnailLogoId() );
        companyProfileDto.setViewLinkThumbnail( companyProfile.getViewLinkThumbnail() );
        companyProfileDto.setThumbnailDescription( companyProfile.getThumbnailDescription() );
        companyProfileDto.setLinkedIn( companyProfile.getLinkedIn() );
        companyProfileDto.setFacebook( companyProfile.getFacebook() );
        companyProfileDto.setFirstNameRepresentative( companyProfile.getFirstNameRepresentative() );
        companyProfileDto.setLastNameRepresentative( companyProfile.getLastNameRepresentative() );
        companyProfileDto.setHeadline( companyProfile.getHeadline() );
        companyProfileDto.setEmailRepresentative( companyProfile.getEmailRepresentative() );
        companyProfileDto.setPhoneRepresentative( companyProfile.getPhoneRepresentative() );

        return companyProfileDto;
    }
}
