package com.chechen.mapper;

import com.chechen.dto.CpBenefitDto;
import com.chechen.entity.CpBenefit;
import org.mapstruct.Mapper;

import java.util.Set;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
@Mapper(componentModel = "spring")
public interface CompanyBenefitMapper {
    CpBenefitDto toDto(CpBenefit cpBenefit);
    Set<CpBenefitDto> toListDto(Set<CpBenefit> cpBenefitList);
}
