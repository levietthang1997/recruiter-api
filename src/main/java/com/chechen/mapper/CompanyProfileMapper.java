package com.chechen.mapper;

import com.chechen.dto.*;
import com.chechen.entity.CompanyBenefitDetail;
import com.chechen.entity.CompanyProfile;
import com.chechen.entity.Industry;
import com.chechen.entity.FileManagement;
import org.mapstruct.Mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
@Mapper(componentModel = "spring")
public interface CompanyProfileMapper {
   default CompanyProfileDto toDto(CompanyProfile companyProfile) {
      if ( companyProfile == null ) {
         return null;
      }

      CompanyProfileDto companyProfileDto = new CompanyProfileDto();
      companyProfileDto.setId(companyProfile.getId());
      companyProfileDto.setCompanyName( companyProfile.getCompanyName() );
      companyProfileDto.setCompanyPhone( companyProfile.getCompanyPhone() );
      companyProfileDto.setCompanyAddress( companyProfile.getCompanyAddress() );
      companyProfileDto.setCompanySize( companyProfile.getCompanySize() );
      companyProfileDto.setCompanyEmail( companyProfile.getCompanyEmail() );
      companyProfileDto.setCcMail( companyProfile.getCcMail() );
      companyProfileDto.setWebsite( companyProfile.getWebsite() );
      companyProfileDto.setCompanyIndustries( cpIndustrySetToCpIndustryDtoList( companyProfile.getCompanyIndustries() ) );
      companyProfileDto.setWorkingHours( companyProfile.getWorkingHours() );
      companyProfileDto.setBenefitDetails( companyBenefitDetailSetToCompanyBenefitDetailDtoSet( companyProfile.getBenefitDetails() ) );
      companyProfileDto.setFileLogoId( companyProfile.getFileLogoId() );
      companyProfileDto.setDescription( companyProfile.getDescription() );
      companyProfileDto.setGallery( fileManagementSetToFileManagementDtoList( companyProfile.getGallery() ) );
      companyProfileDto.setThumbnailLogoId( companyProfile.getThumbnailLogoId() );
      companyProfileDto.setThumbnailDescription( companyProfile.getThumbnailDescription() );
      companyProfileDto.setLinkedIn( companyProfile.getLinkedIn() );
      companyProfileDto.setFacebook( companyProfile.getFacebook() );
      companyProfileDto.setFirstNameRepresentative( companyProfile.getFirstNameRepresentative() );
      companyProfileDto.setLastNameRepresentative( companyProfile.getLastNameRepresentative() );
      companyProfileDto.setHeadline( companyProfile.getHeadline() );
      companyProfileDto.setEmailRepresentative( companyProfile.getEmailRepresentative() );
      companyProfileDto.setPhoneRepresentative( companyProfile.getPhoneRepresentative() );
      companyProfileDto.setViewLinkLogo(companyProfile.getViewLinkLogo());
      companyProfileDto.setViewLinkThumbnail(companyProfile.getViewLinkThumbnail());

      return companyProfileDto;
   }

   default IndustryDto cpIndustryToCpIndustryDto(Industry industry) {
      if ( industry == null ) {
         return null;
      }

      IndustryDto industryDto = new IndustryDto();

      industryDto.setId( industry.getId() );
      industryDto.setName( industry.getName() );

      return industryDto;
   }

   default List<IndustryDto> cpIndustrySetToCpIndustryDtoList(Set<Industry> set) {
      if ( set == null ) {
         return null;
      }

      List<IndustryDto> list = new ArrayList<IndustryDto>( set.size() );
      for ( Industry industry : set ) {
         list.add( cpIndustryToCpIndustryDto(industry) );
      }

      return list;
   }

   default CompanyBenefitDetailDto companyBenefitDetailToCompanyBenefitDetailDto(CompanyBenefitDetail companyBenefitDetail) {
      if ( companyBenefitDetail == null ) {
         return null;
      }

      CompanyBenefitDetailDto companyBenefitDetailDto = new CompanyBenefitDetailDto();
      companyBenefitDetailDto.setBenefitId(companyBenefitDetail.getBenefit().getId());
      companyBenefitDetailDto.setBenefitName(companyBenefitDetail.getBenefit().getName());
      companyBenefitDetailDto.setIcon(companyBenefitDetail.getBenefit().getIcon());
      companyBenefitDetailDto.setDetail( companyBenefitDetail.getDetail() );

      return companyBenefitDetailDto;
   }

   default Set<CompanyBenefitDetailDto> companyBenefitDetailSetToCompanyBenefitDetailDtoSet(Set<CompanyBenefitDetail> set) {
      if ( set == null ) {
         return null;
      }

      Set<CompanyBenefitDetailDto> set1 = new HashSet<CompanyBenefitDetailDto>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
      for ( CompanyBenefitDetail companyBenefitDetail : set ) {
         set1.add( companyBenefitDetailToCompanyBenefitDetailDto( companyBenefitDetail ) );
      }

      return set1;
   }

   default FileManagementDto fileManagementToFileManagementDto(FileManagement fileManagement) {
      if ( fileManagement == null ) {
         return null;
      }

      FileManagementDto fileManagementDto = new FileManagementDto();

      fileManagementDto.setId( fileManagement.getId() );
      fileManagementDto.setName( fileManagement.getName() );
      fileManagementDto.setViewLink( fileManagement.getViewLink());

      return fileManagementDto;
   }

   default List<FileManagementDto> fileManagementSetToFileManagementDtoList(Set<FileManagement> set) {
      if ( set == null ) {
         return null;
      }

      List<FileManagementDto> list = new ArrayList<FileManagementDto>( set.size() );
      for ( FileManagement fileManagement : set ) {
         list.add( fileManagementToFileManagementDto( fileManagement ) );
      }

      return list;
   }

   List<CompanyProfileDtoV2> toLstDtoV2(List<CompanyProfile> lstCompnayProfile);

    CompanyPublicProfileDto toPublicProfileDto(CompanyProfile companyProfile);
}
