package com.chechen.mapper;

import com.chechen.dto.JobCategoryDto;
import com.chechen.dto.JobRoleDto;
import com.chechen.dto.PaginationDto;
import com.chechen.entity.JobCategory;
import com.chechen.entity.JobRole;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface JobCategoryMapper {
    JobCategoryDto toDto(JobCategory jobCategory);
    List<JobCategoryDto> toListDto(List<JobCategory> jobCategories);
    PaginationDto<JobCategoryDto> paginationToDto(PaginationDto<JobCategory> result);
}
