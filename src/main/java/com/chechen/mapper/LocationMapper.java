package com.chechen.mapper;

import com.chechen.dto.LocationDto;
import com.chechen.dto.PaginationDto;
import com.chechen.entity.Location;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LocationMapper {
    PaginationDto<LocationDto> toPaginationDto(PaginationDto<Location> result);

    List<LocationDto> toListDto(List<Location> all);
}
