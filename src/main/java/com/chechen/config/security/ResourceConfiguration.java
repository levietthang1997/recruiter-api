package com.chechen.config.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.concurrent.TimeUnit;

/**
 * @author thanglv on 29/06/2022
 * @project recruiter-api
 */
@Configuration
@EnableWebMvc
public class ResourceConfiguration extends WebMvcConfigurerAdapter  {

    @Value("${upload.public-path}")
    private String publicPath;

    @Value("${upload.static-serve-url}")
    private String staticServeUrl;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        // Resources controlled by Spring Security, which
        registry.addResourceHandler(staticServeUrl + "/**")
                .addResourceLocations("file:" + publicPath)
                .setCacheControl(CacheControl.maxAge(2, TimeUnit.HOURS))
                .setCachePeriod(3600*24);
    }
}
