package com.chechen.config.security;

import com.chechen.obj.Oauth2UserPrincipal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author thanglv on 29/05/2022
 * @project recruiter-api
 */
public class CustomJWTConverter implements Converter<Jwt, AbstractAuthenticationToken> {
    private final Logger logger = LogManager.getLogger();
    private static final String REALM_ACCESS_CLAIM = "realm_access";
    private static final String ROLES_CLAIM = "roles";
    private static final String SCOPE_CLAIM = "scope";
    private static final String USERNAME_CLAIM = "preferred_username";
    private static final String USERID_CLAIM = "user_id";
    private static final String FULLNAME_CLAIM = "name";
    private static final String EMAIL_CLAIM = "email";
    private static final String DEFAULT_ROLE_PREFIX = "ROLE_";
    private static final String DEFAULT_SCOPE_PREFIX = "SCOPE_";
    private static final String SCOPE_SEPARATOR = " ";

    public CustomJWTConverter() {
    }

    @Override
    public AbstractAuthenticationToken convert(@NonNull Jwt jwt) {
        List<GrantedAuthority> authoritiesFromJwt = getAuthoritiesFromJwt(jwt);
        String username = jwt.getClaimAsString(USERNAME_CLAIM);
        String userId = jwt.getClaimAsString(USERID_CLAIM);
        String email = jwt.getClaimAsString(EMAIL_CLAIM);
        if (Strings.isEmpty(username) || Strings.isEmpty(userId) || Strings.isEmpty(email))
            throw new OAuth2AuthenticationException("Token invalid!");
        Oauth2UserPrincipal oauth2User = new Oauth2UserPrincipal(username, userId, email, jwt.getClaimAsString(FULLNAME_CLAIM), authoritiesFromJwt);
        logger.info("Oauth2 user info: {}", oauth2User);
        return new UsernamePasswordAuthenticationToken(oauth2User, "N/A", authoritiesFromJwt);
    }

    private List<GrantedAuthority> getAuthoritiesFromJwt(Jwt jwt) {
        return getCombinedAuthorities(jwt).stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    private List<String> getCombinedAuthorities(Jwt jwt) {
        List<String> authorities = new ArrayList<>(getScopes(jwt));
        authorities.addAll(getRoles(jwt));
        return authorities;
    }

    @SuppressWarnings("unchecked")
    private List<String> getRoles(Jwt jwt) {
        Object roles = ((Map<String, Object>) jwt.getClaims().get(REALM_ACCESS_CLAIM)).get(ROLES_CLAIM);
        if (roles instanceof Collection) {
            return ((Collection<String>) roles).stream()
                    .map(authority -> DEFAULT_ROLE_PREFIX + authority)
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private List<String> getScopes(Jwt jwt) {
        Object scopes = jwt.getClaims().get(SCOPE_CLAIM);
        if (scopes instanceof String) {
            return Arrays.stream(((String) scopes).split(SCOPE_SEPARATOR))
                    .map(authority -> DEFAULT_SCOPE_PREFIX + authority)
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }
}
