package com.chechen.config.security;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.OAuthFlow;
import io.swagger.v3.oas.annotations.security.OAuthFlows;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.web.filter.CorsFilter;

/**
 * @author thanglv on 5/24/2021
 * @project webapp
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@OpenAPIDefinition(servers = {@Server(url = "http://localhost:9999"), @Server(url = "https://api.kttk.xyz/")})
@SecurityScheme(name = "Authorization" , scheme = "Bearer", type = SecuritySchemeType.OAUTH2, in = SecuritySchemeIn.HEADER,
        flows = @OAuthFlows(implicit = @OAuthFlow(authorizationUrl = "https://sso.kttk.xyz/auth/realms/recruitery_realm/protocol/openid-connect/auth",
                tokenUrl = "https://sso.kttk.xyz/auth/realms/recruitery_realm/protocol/openid-connect/token")))
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${upload.static-serve-url}")
    private String staticServeUrl;

    private final CorsFilter corsFilter;
    private final OAuth2ResourceServerProperties oAuth2ResourceServerProperties;

    public SecurityConfiguration(CorsFilter corsFilter, OAuth2ResourceServerProperties oAuth2ResourceServerProperties) {
        this.corsFilter = corsFilter;
        this.oAuth2ResourceServerProperties = oAuth2ResourceServerProperties;
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers("/i18n/**")
                .antMatchers("/test/**")
                .antMatchers("/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources/**",
                        "/configuration/security",
                        "/swagger-ui.html",
                        "/webjars/**",
                        "/v3/api-docs/**",
                        "/swagger-ui/**");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().addFilter(corsFilter)
                . sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .anonymous().and()
                .authorizeRequests()
                .antMatchers("/api/v1/file-management/view/**", "/api/v1/user-info/normal-register",
                        "/api/v1/job/search",
                        "/api/v1/job/popular-job",
                        "/api/v1/location/search",
                        "/api/v1/location/all",
                        "/api/v1/static/public/**",
                        "/api/v1/job/info/**",
                        "/api/v1/company/public-profile/**").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .oauth2ResourceServer()
                .jwt()
                .jwtAuthenticationConverter(new CustomJWTConverter());
    }

    @Bean
    public JwtDecoder jwtDecoder() {
        NimbusJwtDecoder jwtDecoder = (NimbusJwtDecoder) JwtDecoders.fromOidcIssuerLocation(
                oAuth2ResourceServerProperties.getJwt().getIssuerUri());
        OAuth2TokenValidator<Jwt> withIssuer =
                JwtValidators.createDefaultWithIssuer(
                        oAuth2ResourceServerProperties.getJwt().getIssuerUri());
        OAuth2TokenValidator<Jwt> withAudience =
                new DelegatingOAuth2TokenValidator<>(withIssuer);
        jwtDecoder.setJwtValidator(withAudience);
        return jwtDecoder;
    }
}
