package com.chechen.config.security;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

/**
 * @author thanglv on 29/05/2022
 * @project recruiter-api
 */
@Qualifier("AudienceValidator")
@Component
public class AudienceValidator implements OAuth2TokenValidator<Jwt> {
    @Override
    public OAuth2TokenValidatorResult validate(Jwt jwt) {
        return OAuth2TokenValidatorResult.success();
    }
}
