package com.chechen.config.hibernate;

import com.chechen.entity.*;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

/**
 * @author thanglv on 26/03/2022
 * @project recruiter-api
 */
@Component
public class SearchEngineConfiguration implements CommandLineRunner {
    private final EntityManager entityManager;

    public SearchEngineConfiguration(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        SearchSession searchSession = Search.session(entityManager);
        searchSession.massIndexer(Skill.class, Location.class, Level.class, JobInfo.class, CompanyProfile.class, CompanyBenefitDetail.class, CpBenefit.class, Industry.class).startAndWait();
    }
}
