package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.*;
import com.chechen.exception.BusinessException;
import com.chechen.services.JobService;
import com.chechen.services.vm.jobinfo.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_JOB)
@RequiredArgsConstructor
@SecurityRequirement(name = "Authorization")
@Tag(name = "JobInfoController - Api liên quan tới Job")
public class JobInfoController {
    private final JobService jobService;

    @PostMapping("/company-create")
    @PreAuthorize("hasAnyAuthority('ROLE_company')")
    @Operation(description = "Api thêm job dành riêng cho company", summary = "Api thêm job dành riêng cho company")
    public BasicResponseDto save(@Valid @RequestBody CreateJobInfoVM request) throws BusinessException {
        return jobService.save(request);
    }

    @PostMapping("/company-update")
    @PreAuthorize("hasAuthority('ROLE_company')")
    @Operation(description = "Api sửa job", summary = "Api sửa job")
    public BasicResponseDto update(@Valid @RequestBody UpdateJobInfoVM request) throws BusinessException {
        return jobService.update(request);
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAnyAuthority('ROLE_company', 'ROLE_admin')")
    @Operation(description = "Api xoá job", summary = "Api xoá job")
    public BasicResponseDto delete(@Valid @RequestBody DeleteJobInfoVM request) throws  BusinessException {
        return jobService.delete(request);
    }

    @PostMapping("/search")
    @Operation(description = "Api tìm kiếm job", summary = "Api tìm kiếm job")
    public BasicResponseDto<PaginationDto<JobInfoDtoV2>> search(@RequestBody @Valid SearchJobInfoVM request) {
        return jobService.search(request);
    }

    @PostMapping("/company-job")
    @PreAuthorize("hasAuthority('ROLE_company')")
    @Operation(description = "Api get job theo company (lấy danh sách job của riêng công ty)", summary = "Api get job theo company (lấy danh sách job của riêng công ty)")
    public BasicResponseDto<PaginationDto<JobInfoDto>> paginationJobInfoCompany(@RequestBody @Valid SearchJobInfoCompanyOwnVM request) {
        return jobService.searchJobInfoCompanyOwn(request);
    }

    @GetMapping("/popular-job")
    @Operation(description = "Api get job lên trang chủ (10 job có độ ưu tiên cao nhất lên trang chủ)", summary = "Api lấy job có độ ưu tiên cao lên trang chủ")
    public BasicResponseDto<List<JobInfoDtoV2>> get10JobHomePage() {
        return jobService.get10JobHomePage();
    }

    @GetMapping("/job-pending-detail/{pendingTaskId}")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api get chi tiết job (admin)", summary = "Api get chi tiết job (admin)")
    public BasicResponseDto<JobInfoDtoV3> getJobPendingDetail(@PathVariable String pendingTaskId) throws BusinessException {
        return jobService.getJobPendingDetail(pendingTaskId);
    }

    @GetMapping("/info/{jobId}")
    @Operation(description = "Api get chi tiết job (normal user)", summary = "Api get chi tiết job (normal user)")
    public BasicResponseDto<JobInfoDtoV4> getJobInfoDetail(@PathVariable String jobId) throws BusinessException {
        return jobService.getJobInfoDetail(jobId);
    }
}
