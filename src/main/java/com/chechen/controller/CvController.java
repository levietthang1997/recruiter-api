package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.CvDetectionDto;
import com.chechen.dto.CvInformationDto;
import com.chechen.entity.CvInformation;
import com.chechen.exception.BusinessException;
import com.chechen.services.CvService;
import com.chechen.services.vm.cv.CreateCvVM;
import com.chechen.services.vm.file.UploadCvVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;

/**
 * @author thanglv on 30/07/2022
 * @project recruiter-api
 */

@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.CV)
@SecurityRequirement(name = "Authorization")
@Tag(name = "CvController - Api liên quan tới upload và xử lý cv của ứng viên (company và hr đều upload được cv)")
public class CvController {

    private final CvService cvService;

    public CvController(CvService cvService) {
        this.cvService = cvService;
    }

    @PostMapping(value = "/upload-cv", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @PreAuthorize("hasAnyAuthority('ROLE_company', 'ROLE_headhunt')")
    @Operation(description = "Api upload cv của ứng viên cho company và hr", summary = "Api upload cv của ứng viên cho company và hr", requestBody = @RequestBody(content = {@Content(mediaType = MediaType.MULTIPART_FORM_DATA_VALUE)}))
    public BasicResponseDto<CvDetectionDto> uploadCv(@ModelAttribute @Valid UploadCvVM request) throws BusinessException, IOException {
        return cvService.uploadCv(request);
    }

    @PostMapping(value = "/create-cv")
    @PreAuthorize("hasAnyAuthority('ROLE_company', 'ROLE_headhunt')")
    @Operation(description = "Api create cv của ứng viên cho company và hr", summary = "Api create cv của ứng viên cho company và hr")
    public BasicResponseDto<CvInformationDto> createCv(@RequestBody @Valid CreateCvVM request) throws BusinessException, IOException {
        return cvService.createCv(request);
    }
}
