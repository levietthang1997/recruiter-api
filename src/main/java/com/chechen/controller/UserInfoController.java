package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.UserInfoService;
import com.chechen.services.vm.userinfo.ChangePasswordVM;
import com.chechen.services.vm.userinfo.UserRegisterVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;

@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_USER_INFO)
@SecurityRequirement(name = "Authorization")
@Tag(name = "UserInfoController - Api liên quan tới người dùng")
public class UserInfoController {
    private final Logger logger = LogManager.getLogger();

    private final UserInfoService userInfoService;

    public UserInfoController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    /**
     * Api cho normal user đăng ký, đăng ký làm headhunt hoặc là đăng ký account công ty
     * @param request
     * @return
     */
    @PostMapping("/normal-register")
    @Operation(description = "Api cho normal user đăng ký, đăng ký làm headhunt hoặc là đăng ký account công ty", summary = "Api cho normal user đăng ký")
    public BasicResponseDto normalRegister(@RequestBody @Valid UserRegisterVM request) throws BusinessException {
        return userInfoService.normalRegister(request);
    }

    @PreAuthorize("hasAuthority('ROLE_user')")
    @PostMapping("/change-password")
    @Operation(description = "Api cho user đổi password", summary = "Api cho user đổi password")
    public BasicResponseDto changePassword(@RequestBody @Valid ChangePasswordVM request) throws BusinessException {
        return userInfoService.changePassword(request);
    }
}
