package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.FileUploadPolicyDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.FileUploadPolicyService;
import com.chechen.services.vm.file.CreateFileUploadPolicyVM;
import com.chechen.services.vm.file.DeleteFileUploadPolicyVM;
import com.chechen.services.vm.level.DeleteLevelVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_FILE_UPLOAD_POLICY)
@SecurityRequirement(name = "Authorization")
@Tag(name = "FileUploadPolicyController - Api liên quan tới rule upload file")
public class FileUploadPolicyController {

    private final FileUploadPolicyService fileUploadPolicyService;

    public FileUploadPolicyController(FileUploadPolicyService fileUploadPolicyService) {
        this.fileUploadPolicyService = fileUploadPolicyService;
    }

    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api tạo rule upload file", summary = "Api tạo rule upload file")
    public BasicResponseDto uploadSingleFile(@RequestBody @Valid CreateFileUploadPolicyVM fileUploadPolicyVM) throws BusinessException, IOException {
        return fileUploadPolicyService.create(fileUploadPolicyVM);
    }

    @GetMapping(value = "/all")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api get rule upload file", summary = "Api get rule upload file")
    public BasicResponseDto<List<FileUploadPolicyDto>> findAll() throws BusinessException, IOException {
        return fileUploadPolicyService.findAll();
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api xoá file upload policy", summary = "Api xoá file upload policy")
    public BasicResponseDto delete(@Valid @RequestBody DeleteFileUploadPolicyVM request) throws  BusinessException {
        return fileUploadPolicyService.delete(request);
    }
}
