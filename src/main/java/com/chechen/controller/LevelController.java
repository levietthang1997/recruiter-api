package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.LevelDto;
import com.chechen.dto.PaginationDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.LevelService;
import com.chechen.services.vm.level.CreateLevelVM;
import com.chechen.services.vm.level.DeleteLevelVM;
import com.chechen.services.vm.level.SearchLevelVM;
import com.chechen.services.vm.level.UpdateLevelVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_LEVEL)
@RequiredArgsConstructor
@SecurityRequirement(name = "Authorization")
@Tag(name = "LevelController - Api liên quan tới nơi level ứng viên")
public class LevelController {
    private final LevelService levelService;

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api thêm level", summary = "Api thêm level")
    public BasicResponseDto save(@Valid @RequestBody CreateLevelVM request) {
        return levelService.save(request);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api sửa level", summary = "Api sửa level")
    public BasicResponseDto update(@Valid @RequestBody UpdateLevelVM request) throws BusinessException {
        return levelService.update(request);
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api xoá level", summary = "Api xoá level")
    public BasicResponseDto delete(@Valid @RequestBody DeleteLevelVM request) throws  BusinessException {
        return levelService.delete(request);
    }

    @PostMapping("/search")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api tìm kiếm level", summary = "Api tìm kiếm level")
    public BasicResponseDto<PaginationDto<LevelDto>> search(@RequestBody SearchLevelVM searchSkillVM) {
        return levelService.search(searchSkillVM);
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('ROLE_admin', 'ROLE_company')")
    @Operation(description = "Api tìm kiếm all level", summary = "Api tìm kiếm all level")
    public BasicResponseDto<List<LevelDto>> findAll() {
        return levelService.findAll();
    }
}
