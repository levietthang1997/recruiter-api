package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.CompanyProfileDto;
import com.chechen.dto.CompanyProfileDtoV2;
import com.chechen.dto.CompanyPublicProfileDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.CompanyService;
import com.chechen.services.vm.company.UpdateCompanyProfileVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_COMPANY)
@SecurityRequirement(name = "Authorization")
@Tag(name = "CompanyController - Api liên quan tới company")
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @PostMapping("/update-profile")
    @PreAuthorize("hasAnyAuthority('ROLE_company')")
    @Operation(description = "Api update profile company", summary = "Api update profile company")
    public BasicResponseDto<CompanyProfileDto> updateProfile(@RequestBody @Valid UpdateCompanyProfileVM request) throws BusinessException {
        return companyService.updateProfile(request);
    }

    @GetMapping("/profile")
    @PreAuthorize("hasAnyAuthority('ROLE_company')")
    @Operation(description = "Api get profile company", summary = "Api get profile company")
    public BasicResponseDto<CompanyProfileDto> getProfile() throws IOException, BusinessException {
        return companyService.getProfileCompany();
    }

    @GetMapping("/all-company")
    @PreAuthorize("hasAnyAuthority('ROLE_admin')")
    @Operation(description = "Api get profile company", summary = "Api get profile company")
    public BasicResponseDto<List<CompanyProfileDtoV2>> getAllCompany() {
        return companyService.getAllCompany();
    }

    @GetMapping("/public-profile/{companyId}")
    @Operation(description = "Api get public profile company", summary = " Api get public profile comany")
    public BasicResponseDto<CompanyPublicProfileDto> getPublicProfile(@PathVariable String companyId) throws BusinessException {
        return companyService.getPublicProfile(companyId);
    }
}
