package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.CpBenefitDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.CompanyBenefitService;
import com.chechen.services.vm.company.CreateCompanyBenefitVM;
import com.chechen.services.vm.company.DeleteCompanyBenefitVM;
import com.chechen.services.vm.company.UpdateCompanyBenefitVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_COMPANY_BENEFIT)
@SecurityRequirement(name = "Authorization")
@Tag(name = "CompanyBenefitController - Api liên quan tới lợi ích công ty")
public class CompanyBenefitController {

    private final CompanyBenefitService companyBenefitService;

    public CompanyBenefitController(CompanyBenefitService companyBenefitService) {
        this.companyBenefitService = companyBenefitService;
    }

    @PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('ROLE_admin')")
    @Operation(description = "Api tạo lợi ích công ty", summary = "Api tạo lợi ích công ty")
    public BasicResponseDto create(@RequestBody @Valid CreateCompanyBenefitVM request) {
        return companyBenefitService.create(request);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAnyAuthority('ROLE_admin')")
    @Operation(description = "Api sửa lợi ích công ty", summary = "Api sửa lợi ích công ty")
    public BasicResponseDto update(@RequestBody @Valid UpdateCompanyBenefitVM request) throws BusinessException {
        return companyBenefitService.update(request);
    }

    @GetMapping("/get-all")
    @PreAuthorize("hasAnyAuthority('ROLE_company', 'ROLE_admin')")
    @Operation(description = "Api lấy danh sách lợi ích công ty", summary = "Api lấy danh sách lợi ích công ty")
    public BasicResponseDto<Set<CpBenefitDto>> getAll() throws BusinessException {
        return companyBenefitService.getAll();
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAnyAuthority('ROLE_admin')")
    @Operation(description = "Api xoá lợi công ty", summary = "Api xoá lợi ích công ty")
    public BasicResponseDto delete(@RequestBody @Valid DeleteCompanyBenefitVM request) throws BusinessException {
        return companyBenefitService.delete(request);
    }
}
