package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.LocationDto;
import com.chechen.dto.PaginationDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.LocationService;
import com.chechen.services.vm.location.CreateLocationVM;
import com.chechen.services.vm.location.DeleteLocationVM;
import com.chechen.services.vm.location.SearchLocationVM;
import com.chechen.services.vm.location.UpdateLocationVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_LOCATION)
@RequiredArgsConstructor
@SecurityRequirement(name = "Authorization")
@Tag(name = "LocationController - Api liên quan tới nơi làm việc của Job")
public class LocationController {
    private final LocationService locationService;

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api thêm location", summary = "Api thêm location")
    public BasicResponseDto save(@Valid @RequestBody CreateLocationVM request) {
        return locationService.save(request);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api sửa location", summary = "Api sửa location")
    public BasicResponseDto update(@Valid @RequestBody UpdateLocationVM request) throws BusinessException {
        return locationService.update(request);
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api xoá location", summary = "Api xoá location")
    public BasicResponseDto delete(@Valid @RequestBody DeleteLocationVM request) throws BusinessException {
        return locationService.delete(request);
    }

    @PostMapping("/search")
    @Operation(description = "Api tìm kiếm location", summary = "Api tìm kiếm location")
    public BasicResponseDto<PaginationDto<LocationDto>> search(@RequestBody @Valid SearchLocationVM searchLocationVM) {
        return locationService.search(searchLocationVM);
    }

    @GetMapping("/all")
    @Operation(description = "Api tìm kiếm all location", summary = "Api tìm kiếm all location")
    public BasicResponseDto<List<LocationDto>> findAll() {
        return locationService.findAll();
    }
}
