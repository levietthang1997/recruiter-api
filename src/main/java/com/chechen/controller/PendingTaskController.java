package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.PaginationDto;
import com.chechen.dto.PendingTaskDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.PendingTaskService;
import com.chechen.services.vm.pendingtask.SearchPendingTaskJobVM;
import com.chechen.services.vm.pendingtask.UpdateStatusPendingJobVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author thanglv on 28/06/2022
 * @project recruiter-api
 */

@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.PENDING_TASK)
@SecurityRequirement(name = "Authorization")
@Tag(name = "PendingTaskController - Api liên quan tới nơi làm việc admin duyệt")
public class PendingTaskController {

    private final PendingTaskService pendingTaskService;

    public PendingTaskController(PendingTaskService pendingTaskService) {
        this.pendingTaskService = pendingTaskService;
    }

    @PostMapping("/get-pending-job")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api lấy danh sách job theo tất cả trạng thái", summary = "Api lấy danh sách job cần duyệt")
    public BasicResponseDto<PaginationDto<PendingTaskDto>> getPendingTaskJob(@RequestBody @Valid SearchPendingTaskJobVM request) {
        return pendingTaskService.getPendingTaskJob(request);
    }

    @PostMapping("/update-status-pending-job")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api cập nhật trạng thái job cho admin", summary = "Api cập nhật trạng thái job dành cho admin")
    public BasicResponseDto updateStatusPendingJob(@RequestBody @Valid UpdateStatusPendingJobVM request) throws BusinessException {
        return pendingTaskService.updateStatusPendingJob(request);
    }
}
