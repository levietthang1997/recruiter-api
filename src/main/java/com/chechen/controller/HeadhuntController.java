package com.chechen.controller;


import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.CompanyProfileDto;
import com.chechen.dto.HeadhuntBrandDto;
import com.chechen.dto.HeadhuntProfileDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.HeadhuntService;
import com.chechen.services.vm.company.UpdateCompanyProfileVM;
import com.chechen.services.vm.headhunt.UpdateHeadhuntBrandVM;
import com.chechen.services.vm.headhunt.UpdateHeadhuntProfileVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_HEADHUNT)
@SecurityRequirement(name = "Authorization")
@Tag(name = "Headhunt controller - Api liên quan tới headhunt (Headhunt profile và brand)")
public class HeadhuntController {

    private final HeadhuntService headhuntService;

    public HeadhuntController(HeadhuntService headhuntService) {
        this.headhuntService = headhuntService;
    }

    @PostMapping("/update-profile")
    @PreAuthorize("hasAnyAuthority('ROLE_headhunt')")
    @Operation(description = "Api update profile headhunt", summary = "Api update profile headhunt")
    public BasicResponseDto<HeadhuntProfileDto> updateProfile(@RequestBody @Valid UpdateHeadhuntProfileVM request) throws IOException, BusinessException {
        return headhuntService.updateProfile(request);
    }

    @GetMapping("/profile")
    @PreAuthorize("hasAnyAuthority('ROLE_headhunt')")
    @Operation(description = "Api get profile headhunt", summary = "Api get profile headhunt")
    public BasicResponseDto<HeadhuntProfileDto> getProfile() throws BusinessException {
        return headhuntService.getProfile();
    }

    @GetMapping("/brand")
    @PreAuthorize("hasAnyAuthority('ROLE_headhunt')")
    @Operation(description = "Api get brand headhunt", summary = "Api get brand headhunt")
    public BasicResponseDto<HeadhuntBrandDto> getBrandInfo() throws BusinessException {
        return headhuntService.getBrandInfo();
    }

    @PostMapping("/update-brand")
    @PreAuthorize("hasAnyAuthority('ROLE_headhunt')")
    @Operation(description = "Api update brand headhunt", summary = "Api update brand headhunt")
    public BasicResponseDto<HeadhuntBrandDto> updateBrandInfo(@RequestBody @Valid UpdateHeadhuntBrandVM request) throws IOException, BusinessException {
        return headhuntService.updateBrandInfo(request);
    }
}
