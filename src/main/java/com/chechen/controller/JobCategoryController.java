package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.JobCategoryDto;
import com.chechen.dto.JobRoleDto;
import com.chechen.dto.PaginationDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.JobCategoryService;
import com.chechen.services.vm.jobcategory.CreateJobCategoryVM;
import com.chechen.services.vm.jobcategory.DeleteJobCategoryVM;
import com.chechen.services.vm.jobcategory.SearchJobCategoryVM;
import com.chechen.services.vm.jobcategory.UpdateJobCategoryVM;
import com.chechen.services.vm.jobrole.CreateJobRoleVM;
import com.chechen.services.vm.jobrole.DeleteJobRoleVM;
import com.chechen.services.vm.jobrole.SearchJobRoleVM;
import com.chechen.services.vm.jobrole.UpdateJobRoleVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_JOB_CATEGORY)
@RequiredArgsConstructor
@SecurityRequirement(name = "Authorization")
@Tag(name = "JobCategoryController - Api liên quan tới upload cv của headhunt")
public class JobCategoryController {
    private final JobCategoryService jobCategoryService;

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api thêm job category", summary = "Api thêm job category")
    public BasicResponseDto save(@Valid @RequestBody CreateJobCategoryVM request) {
        return jobCategoryService.save(request);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api sửa job category", summary = "Api sửa job category")
    public BasicResponseDto update(@Valid @RequestBody UpdateJobCategoryVM request) throws BusinessException {
        return jobCategoryService.update(request);
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api xoá job category", summary = "Api xoá job category")
    public BasicResponseDto delete(@Valid @RequestBody DeleteJobCategoryVM request) throws  BusinessException {
        return jobCategoryService.delete(request);
    }

    @PostMapping("/search")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api tìm kiếm job category", summary = "Api tìm kiếm job category")
    public BasicResponseDto<PaginationDto<JobCategoryDto>> search(@RequestBody SearchJobCategoryVM request) {
        return jobCategoryService.search(request);
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('ROLE_admin', 'ROLE_company', 'ROLE_headhunt')")
    @Operation(description = "Api tìm kiếm all job category", summary = "Api tìm kiếm all job category")
    public BasicResponseDto<List<JobCategoryDto>> findAll() {
        return jobCategoryService.findAll();
    }
}
