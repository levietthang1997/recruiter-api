package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.FileManagementDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.FileManagementService;
import com.chechen.services.vm.file.UploadFileVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_FILE_MANAGEMENT)
@SecurityRequirement(name = "Authorization")
@Tag(name = "FileManagementController - Api liên quan tới upload file")
public class FileManagementController {

    private final FileManagementService fileManagementService;

    public FileManagementController(FileManagementService fileManagementService) {
        this.fileManagementService = fileManagementService;
    }

    @PostMapping(value = "/upload", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @PreAuthorize("hasAnyAuthority('ROLE_admin', 'ROLE_company', 'ROLE_headhunt')")
    @Operation(description = "Api upload file", summary = "Api upload file", requestBody = @RequestBody(content = {@Content(mediaType = MediaType.MULTIPART_FORM_DATA_VALUE)}))
    public BasicResponseDto<List<FileManagementDto>> uploadMultipleFile(@ModelAttribute @Valid UploadFileVM uploadFileVM) throws BusinessException, IOException {
        return fileManagementService.uploadMultipleFile(uploadFileVM, false);
    }

    @PostMapping(value = "/upload-logo", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @PreAuthorize("hasAnyAuthority('ROLE_admin', 'ROLE_company', 'ROLE_headhunt')")
    @Operation(description = "Api upload file", summary = "Api upload file", requestBody = @RequestBody(content = {@Content(mediaType = MediaType.MULTIPART_FORM_DATA_VALUE)}))
    public BasicResponseDto<List<FileManagementDto>> uploadLogoFile(@ModelAttribute @Valid UploadFileVM uploadFileVM) throws BusinessException, IOException {
        return fileManagementService.uploadFileLogo(uploadFileVM);
    }

    @GetMapping("/view/{fileId}")
    @Operation(description = "Api view file", summary = "Api view file")
    public void viewUploadFile(@PathVariable String fileId, HttpServletResponse httpServletResponse) throws IOException {
        fileManagementService.viewFileUpload(fileId, httpServletResponse);
    }

}
