package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.PaginationDto;
import com.chechen.dto.SkillDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.SkillService;
import com.chechen.services.vm.skill.CreateSkillVM;
import com.chechen.services.vm.skill.DeleteSkillVM;
import com.chechen.services.vm.skill.SearchSkillVM;
import com.chechen.services.vm.skill.UpdateSkillVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_SKILL)
@RequiredArgsConstructor
@SecurityRequirement(name = "Authorization")
@Tag(name = "SkillController - Api liên quan tới Job skill")
public class SkillController {
    private final SkillService skillService;

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api thêm skill", summary = "Api thêm skill")
    public BasicResponseDto save(@Valid @RequestBody CreateSkillVM request) {
        return skillService.save(request);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api sửa skill", summary = "Api sửa skill")
    public BasicResponseDto update(@Valid @RequestBody UpdateSkillVM request) throws  BusinessException {
        return skillService.update(request);
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api xoá skill", summary = "Api xoá skill")
    public BasicResponseDto delete(@Valid @RequestBody DeleteSkillVM request) throws  BusinessException {
        return skillService.delete(request);
    }

    @PostMapping("/search")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api tìm kiếm skill", summary = "Api tìm kiếm skill")
    public BasicResponseDto<PaginationDto<SkillDto>> search(@RequestBody SearchSkillVM searchSkillVM) {
        return skillService.search(searchSkillVM);
    }
    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('ROLE_admin', 'ROLE_company')")
    @Operation(description = "Api tìm kiếm all skill", summary = "Api tìm kiếm all skill")
    public BasicResponseDto<List<SkillDto>> findAll() {
        return skillService.findAll();
    }

}
