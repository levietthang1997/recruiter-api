package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.LanguageDto;
import com.chechen.dto.LevelDto;
import com.chechen.dto.PaginationDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.LanguageService;
import com.chechen.services.LevelService;
import com.chechen.services.vm.language.CreateLanguageVM;
import com.chechen.services.vm.language.DeleteLanguageVM;
import com.chechen.services.vm.language.SearchLanguageVM;
import com.chechen.services.vm.language.UpdateLanguageVM;
import com.chechen.services.vm.level.CreateLevelVM;
import com.chechen.services.vm.level.DeleteLevelVM;
import com.chechen.services.vm.level.SearchLevelVM;
import com.chechen.services.vm.level.UpdateLevelVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_LANGUAGE)
@RequiredArgsConstructor
@SecurityRequirement(name = "Authorization")
@Tag(name = "LanguageController - Api liên quan tới headhunt brand (Ngôn ngữ mà hr biết)")
public class LanguageController {
    private final LanguageService languageService;

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api thêm language", summary = "Api thêm language")
    public BasicResponseDto save(@Valid @RequestBody CreateLanguageVM request) {
        return languageService.save(request);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api sửa language", summary = "Api sửa language")
    public BasicResponseDto update(@Valid @RequestBody UpdateLanguageVM request) throws BusinessException {
        return languageService.update(request);
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api xoá language", summary = "Api xoá language")
    public BasicResponseDto delete(@Valid @RequestBody DeleteLanguageVM request) throws  BusinessException {
        return languageService.delete(request);
    }

    @PostMapping("/search")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api tìm kiếm language", summary = "Api tìm kiếm language")
    public BasicResponseDto<PaginationDto<LanguageDto>> search(@RequestBody SearchLanguageVM request) {
        return languageService.search(request);
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('ROLE_admin', 'ROLE_company')")
    @Operation(description = "Api tìm kiếm all language", summary = "Api tìm kiếm all language")
    public BasicResponseDto<List<LanguageDto>> findAll() {
        return languageService.findAll();
    }
}
