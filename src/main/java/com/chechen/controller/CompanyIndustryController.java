package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.IndustryDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.IndustryService;
import com.chechen.services.vm.company.CreateCompanyIndustryVM;
import com.chechen.services.vm.company.DeleteIndustryVM;
import com.chechen.services.vm.company.UpdateCompanyIndustryVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_COMPANY_INDUSTRY)
@SecurityRequirement(name = "Authorization")
@Tag(name = "CompanyIndustryController - Api liên quan tới ngành nghề company")
public class CompanyIndustryController {

    private final IndustryService industryService;

    public CompanyIndustryController(IndustryService industryService) {
        this.industryService = industryService;
    }

    @PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('ROLE_admin')")
    @Operation(description = "Api tạo ngành nghề công ty", summary = "Api tạo ngành nghề công ty")
    public BasicResponseDto create(@RequestBody @Valid CreateCompanyIndustryVM request) {
        return industryService.create(request);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAnyAuthority('ROLE_admin')")
    @Operation(description = "Api sửa ngành nghề công ty", summary = "Api sửa ngành nghề công ty")
    public BasicResponseDto update(@RequestBody @Valid UpdateCompanyIndustryVM request) throws BusinessException {
        return industryService.update(request);
    }

    @GetMapping("/get-all")
    @PreAuthorize("hasAnyAuthority('ROLE_company', 'ROLE_admin')")
    @Operation(description = "Api lấy danh sách ngành nghề công ty", summary = "Api lấy danh sách ngành nghề công ty")
    public BasicResponseDto<Set<IndustryDto>> getAll() throws BusinessException {
        return industryService.getAll();
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAnyAuthority('ROLE_admin')")
    @Operation(description = "Api xoá ngành nghề công ty", summary = "Api xoá ngành nghề công ty")
    public BasicResponseDto delete(@RequestBody @Valid DeleteIndustryVM request) throws BusinessException {
        return industryService.delete(request);
    }
}
