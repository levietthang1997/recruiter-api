package com.chechen.controller;

import com.chechen.constant.UrlConstant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.JobRoleDto;
import com.chechen.dto.LevelDto;
import com.chechen.dto.PaginationDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.JobRoleService;
import com.chechen.services.LevelService;
import com.chechen.services.vm.jobrole.CreateJobRoleVM;
import com.chechen.services.vm.jobrole.DeleteJobRoleVM;
import com.chechen.services.vm.jobrole.SearchJobRoleVM;
import com.chechen.services.vm.jobrole.UpdateJobRoleVM;
import com.chechen.services.vm.level.CreateLevelVM;
import com.chechen.services.vm.level.DeleteLevelVM;
import com.chechen.services.vm.level.SearchLevelVM;
import com.chechen.services.vm.level.UpdateLevelVM;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@RestController
@RequestMapping(UrlConstant.API_VERSION + UrlConstant.API_JOB_ROLE)
@RequiredArgsConstructor
@SecurityRequirement(name = "Authorization")
@Tag(name = "JobRoleController - Api liên quan tới job role của headhunt brand (Lĩnh vực ngành nghề - banking, finance, petro, ...)")
public class JobRoleController {
    private final JobRoleService jobRoleService;

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api thêm job role", summary = "Api thêm job role")
    public BasicResponseDto save(@Valid @RequestBody CreateJobRoleVM request) {
        return jobRoleService.save(request);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api sửa job role", summary = "Api sửa job role")
    public BasicResponseDto update(@Valid @RequestBody UpdateJobRoleVM request) throws BusinessException {
        return jobRoleService.update(request);
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api xoá job role", summary = "Api xoá job role")
    public BasicResponseDto delete(@Valid @RequestBody DeleteJobRoleVM request) throws  BusinessException {
        return jobRoleService.delete(request);
    }

    @PostMapping("/search")
    @PreAuthorize("hasAuthority('ROLE_admin')")
    @Operation(description = "Api tìm kiếm job role", summary = "Api tìm kiếm job role")
    public BasicResponseDto<PaginationDto<JobRoleDto>> search(@RequestBody SearchJobRoleVM request) {
        return jobRoleService.search(request);
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('ROLE_admin', 'ROLE_company', 'ROLE_headhunt')")
    @Operation(description = "Api tìm kiếm all job role", summary = "Api tìm kiếm all job role")
    public BasicResponseDto<List<JobRoleDto>> findAll() {
        return jobRoleService.findAll();
    }
}
