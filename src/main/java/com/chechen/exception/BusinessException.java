package com.chechen.exception;

/**
 * @author thanglv on 5/15/2022
 * @project recruiter-api
 */
public class BusinessException extends Exception {
    public BusinessException(String msg) {
        super(msg);
    }
}
