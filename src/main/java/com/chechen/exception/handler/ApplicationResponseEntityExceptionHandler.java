package com.chechen.exception.handler;

import com.chechen.dto.BasicResponseDto;
import com.chechen.exception.BusinessException;
import com.chechen.utils.ErrorCode;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@RestController
public class ApplicationResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<BasicResponseDto<Object>> handleBadCredentialsException(Exception ex, WebRequest request) {
        logger.error("Error", ex);
        BasicResponseDto<Object> res = new BasicResponseDto<>(ErrorCode.ERR_400, ex.getMessage(), null);
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(res);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<BasicResponseDto<Object>> handleAccessDeniedException(Exception ex, WebRequest request) {
        logger.error("Error", ex);
        BasicResponseDto<Object> res = new BasicResponseDto<>(ErrorCode.ERR_403, ex.getMessage(), null);
        return ResponseEntity.status(HttpStatus.FORBIDDEN).contentType(MediaType.APPLICATION_JSON).body(res);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        logger.error("Error", ex);
        BasicResponseDto<Object> res = new BasicResponseDto<>(ErrorCode.ERR_500, "Lỗi hệ thống", null);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON).body(res);
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleBusiness(Exception ex, WebRequest request) {
        logger.error("Error", ex);
        BasicResponseDto<Object> res = new BasicResponseDto<>(ErrorCode.ERR_400, ex.getMessage(), null);
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(res);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = new ArrayList<>();
        setError(errors, ex);
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    private void setError(List<String> errors, BindException ex) {
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + " : " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + " : " + error.getDefaultMessage());
        }
    }
}
