package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.PaginationDto;
import com.chechen.dto.SkillDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.skill.CreateSkillVM;
import com.chechen.services.vm.skill.DeleteSkillVM;
import com.chechen.services.vm.skill.SearchSkillVM;
import com.chechen.services.vm.skill.UpdateSkillVM;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface SkillService {

    BasicResponseDto save(CreateSkillVM request);

    BasicResponseDto update(UpdateSkillVM request) throws BusinessException;

    BasicResponseDto delete(DeleteSkillVM request) throws BusinessException;

    BasicResponseDto<PaginationDto<SkillDto>> search(@RequestBody SearchSkillVM searchSkillVM);

    BasicResponseDto<List<SkillDto>> findAll();
}
