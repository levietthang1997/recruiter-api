package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.JobRoleDto;
import com.chechen.dto.PaginationDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.jobrole.CreateJobRoleVM;
import com.chechen.services.vm.jobrole.DeleteJobRoleVM;
import com.chechen.services.vm.jobrole.SearchJobRoleVM;
import com.chechen.services.vm.jobrole.UpdateJobRoleVM;

import java.util.List;

public interface JobRoleService {
    BasicResponseDto save(CreateJobRoleVM request);

    BasicResponseDto update(UpdateJobRoleVM request) throws BusinessException;

    BasicResponseDto delete(DeleteJobRoleVM request) throws BusinessException;

    BasicResponseDto<PaginationDto<JobRoleDto>> search(SearchJobRoleVM request);

    BasicResponseDto<List<JobRoleDto>> findAll();
}
