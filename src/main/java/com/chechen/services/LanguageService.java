package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.LanguageDto;
import com.chechen.dto.LevelDto;
import com.chechen.dto.PaginationDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.language.CreateLanguageVM;
import com.chechen.services.vm.language.DeleteLanguageVM;
import com.chechen.services.vm.language.SearchLanguageVM;
import com.chechen.services.vm.language.UpdateLanguageVM;

import java.util.List;

public interface LanguageService {
    BasicResponseDto save(CreateLanguageVM request);

    BasicResponseDto update(UpdateLanguageVM request) throws BusinessException;

    BasicResponseDto<PaginationDto<LanguageDto>> search(SearchLanguageVM request);

    BasicResponseDto delete(DeleteLanguageVM request) throws BusinessException;

    BasicResponseDto<List<LanguageDto>> findAll();
}
