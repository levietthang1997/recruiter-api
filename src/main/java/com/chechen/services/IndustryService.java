package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.IndustryDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.company.CreateCompanyIndustryVM;
import com.chechen.services.vm.company.DeleteIndustryVM;
import com.chechen.services.vm.company.UpdateCompanyIndustryVM;

import java.util.Set;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
public interface IndustryService {
    BasicResponseDto create(CreateCompanyIndustryVM request);

    BasicResponseDto update(UpdateCompanyIndustryVM request) throws BusinessException;

    BasicResponseDto<Set<IndustryDto>> getAll();

    BasicResponseDto delete(DeleteIndustryVM request) throws BusinessException;
}
