package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.HeadhuntBrandDto;
import com.chechen.dto.HeadhuntProfileDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.company.UpdateCompanyProfileVM;
import com.chechen.services.vm.headhunt.UpdateHeadhuntBrandVM;
import com.chechen.services.vm.headhunt.UpdateHeadhuntProfileVM;

public interface HeadhuntService {
    BasicResponseDto<HeadhuntProfileDto> updateProfile(UpdateHeadhuntProfileVM request) throws BusinessException;

    BasicResponseDto<HeadhuntProfileDto> getProfile() throws BusinessException;

    BasicResponseDto<HeadhuntBrandDto> getBrandInfo() throws BusinessException;

    BasicResponseDto<HeadhuntBrandDto> updateBrandInfo(UpdateHeadhuntBrandVM request) throws BusinessException;
}
