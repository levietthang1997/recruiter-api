package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.CvDetectionDto;
import com.chechen.dto.CvInformationDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.cv.CreateCvVM;
import com.chechen.services.vm.file.UploadCvVM;

import java.io.IOException;

public interface CvService {
    BasicResponseDto<CvDetectionDto> uploadCv(UploadCvVM request) throws BusinessException, IOException;

    BasicResponseDto<CvInformationDto> createCv(CreateCvVM request) throws BusinessException;
}
