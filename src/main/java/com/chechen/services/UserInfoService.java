package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.userinfo.ChangePasswordVM;
import com.chechen.services.vm.userinfo.UserRegisterVM;

public interface UserInfoService {
    BasicResponseDto normalRegister(UserRegisterVM request) throws BusinessException;

    BasicResponseDto changePassword(ChangePasswordVM request);
}
