package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.JobCategoryDto;
import com.chechen.dto.JobRoleDto;
import com.chechen.dto.PaginationDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.jobcategory.CreateJobCategoryVM;
import com.chechen.services.vm.jobcategory.DeleteJobCategoryVM;
import com.chechen.services.vm.jobcategory.SearchJobCategoryVM;
import com.chechen.services.vm.jobcategory.UpdateJobCategoryVM;
import com.chechen.services.vm.jobrole.SearchJobRoleVM;

import java.util.List;

public interface JobCategoryService {
    BasicResponseDto save(CreateJobCategoryVM request);

    BasicResponseDto update(UpdateJobCategoryVM request) throws BusinessException;

    BasicResponseDto delete(DeleteJobCategoryVM request) throws BusinessException;

    BasicResponseDto<PaginationDto<JobCategoryDto>> search(SearchJobCategoryVM request);

    BasicResponseDto<List<JobCategoryDto>> findAll();
}
