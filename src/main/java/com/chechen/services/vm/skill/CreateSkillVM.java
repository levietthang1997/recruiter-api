package com.chechen.services.vm.skill;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
public class CreateSkillVM implements Serializable {
    @NotBlank(message = "name không được trống")
    @Size(max = 500, message = "name không quá 500 ký tự")
    private String name;
}
