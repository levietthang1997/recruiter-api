package com.chechen.services.vm.skill;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Data
public class DeleteSkillVM implements Serializable {
    private Set<String> listId;
}
