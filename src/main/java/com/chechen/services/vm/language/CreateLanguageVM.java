package com.chechen.services.vm.language;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Data
public class CreateLanguageVM implements Serializable {
    @NotBlank(message = "name không được trống")
    @Size(max = 200, message = "name không quá 200 ký tự")
    private String name;
}
