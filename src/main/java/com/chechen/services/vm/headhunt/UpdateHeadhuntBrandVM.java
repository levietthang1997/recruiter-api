package com.chechen.services.vm.headhunt;

import com.chechen.dto.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

@Data
public class UpdateHeadhuntBrandVM implements Serializable {
    @Size(max = 255, message = "brandLogoId không qúa 255 ký tự")
    @Schema(name = "brandLogoId", description = "id ảnh brand")
    private String brandLogoId;

    @NotBlank(message = "name không được trống")
    @Size(max = 255, message = "name không quá 255 ký tự")
    @Schema(name = "name", description = "Tên", example = "Lê Việt Thắng")
    private String name;

    @Size(max = 20, message = "phone không quá 20 ký tự")
    @Schema(name = "phone", description = "Số điện thoại", example = "0981576404")
    private String phone;

    @Email(message = "teamLeadMail không đúng định dạng")
    @Size(max = 500, message = "teamLeadMail không quá 500 ký tự")
    @Schema(name = "teamLeadEmail", description = "Email  team lead", example = "teamlead@gmail.com")
    private String teamLeadEmail;

    @Size(max = 500, message = "address không quá 500 ký tự")
    @Schema(name = "address", description = "Địa chỉ", required = false)
    private String address;

    @Email(message = "ccMail không đúng định dạng")
    @Size(max = 500, message = "ccMail không quá 500 ký tự")
    @Schema(name = "ccMail", description = "ccMail ", example = "teamlead@gmail.com")
    private String ccMail;

    // contact
    @NotBlank(message = "contactFirstName không được trống")
    @Size(max = 30, message = "contactFirstName không dài quá 30 ký tự")
    @Schema(name = "contactFirstName",  required = true, description = "First name", defaultValue = "Lê")
    private String contactFirstName;

    @NotBlank(message = "contactLastName không được trống")
    @Size(max = 30, message = "contactLastName không dài quá 30 ký tự")
    @Schema(name = "contactLastName",  required = true, description = "Last name", defaultValue = "La")
    private String contactLastName;

    // chức danh
    @NotBlank(message = "contactTitle không được trống")
    @Size(max = 60, message = "contactTitle không dài quá 30 ký tự")
    @Schema(name = "contactTitle",  required = true, description = "Chức danh", defaultValue = "Tổng giám đốc")
    private String contactTitle;

    @Size(max = 20, message = "contactPhone không quá 20 ký tự")
    @Schema(name = "contactPhone", description = "Số điện thoại", example = "0981576404")
    private String contactPhone;

    @Email(message = "contactEmail không đúng định dạng")
    @Size(max = 200, message = "contactEmail không quá 200 ký tự")
    @Schema(name = "contactEmail", description = "contactEmail ", example = "contactEmail@gmail.com")
    private String contactEmail;

    // profile

    // thông báo tuyển dụng
    @Size(max = 2000, message = "message không quá 2000 ký tự")
    @Schema(name = "message", description = "message ", example = "Thông báo tuyển dụng")
    private String message;

    // Các lĩnh vực mà hr biết
    @Schema(name = "jobRoles", description = "Các lĩnh vực mà hr biết")
    private Set<String> jobRoles;

    // Vị trí mà hr có thể giúp
    @Schema(name = "levels", description = "Vị trí mà hr có thể giúp")
    private Set<String> levels;

    // ngành nghề mà hr am hiểu
    @Schema(name = "headhuntBrandIndustries", description = "Ngành nghề mà hr am hiểu")
    private Set<String> headhuntBrandIndustries;

    // kỹ năng ứng viên hàng đầu mà hr có thể tiếp cận
    @Schema(name = "skills", description = "Kỹ năng ứng viên hàng đầu mà hr có thể tiếp cận")
    private Set<String> skills;

    // ngôn ngữ thành thạo
    @Schema(name = "headhuntBrandLanguage", description = "Ngôn ngữ thành thạo")
    private Set<String> headhuntBrandLanguage;

    // khu vực tuyển dụng
    @Schema(name = "locations", description = "Khu vực tuyển dụng")
    private Set<String> locations;

    @Size(max = 1000, message = "linkedIn không quá 1000 ký tự")
    @Schema(name = "linkedIn", description = "Link linkedin")
    private String linkedIn;

    @Size(max = 1000, message = "facebook không quá 1000 ký tự")
    @Schema(name = "facebook", description = "facebook")
    private String facebook;

    @Size(max = 1000, message = "skype không quá 1000 ký tự")
    @Schema(name = "skype", description = "skype")
    private String skype;
}
