package com.chechen.services.vm.headhunt;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
public class UpdateHeadhuntProfileVM implements Serializable {

    @NotBlank(message = "Họ không được trống")
    @Size(max = 30, message = "firstName không dài quá 30 ký tự")
    @Schema(name = "firstName",  required = true, description = "Họ", defaultValue = "Lê")
    private String firstName;

    @NotBlank(message = "lastName không được trống")
    @Size(max = 30, message = "lastName không dài quá 30 ký tự")
    @Schema(name = "lastName",  required = true, description = "Tên đệm và tên chính", defaultValue = "Việt Thắng")
    private String lastName;

    @Schema(name = "avatarId",  required = false, description = "avatarId")
    private String avatarId;

    @NotBlank(message = "Email không được trống")
    @Email(message = "Email không hợp lệ")
    @Schema(name = "email",  required = true, description = "Email", defaultValue = "levietthang1997@gmail.com")
    private String email;

    @NotBlank(message = "Số điện thoại không được trống")
    @Size(min = 9, max = 11, message = "Phone không dài quá 11 ký tự, ngắn quá 9 ký tự")
    @Schema(name = "phone",  required = true, description = "Số điện thoại", defaultValue = "0981576404")
    private String phone;

    @NotBlank(message = "Số điện thoại không được trống")
    @Size(min = 9, max = 11, message = "Phone không dài quá 11 ký tự, ngắn quá 9 ký tự")
    @Schema(name = "hotline",  required = true, description = "Số điện thoại", defaultValue = "0981576404")
    private String hotline;

    @Size(max = 500, message = "Địa chỉ không quá 500 ký tự")
    @Schema(name = "address", description = "Địa chỉ hr", defaultValue = "Số 33, ngõ 273, Duy Tân, Hà Nội")
    private String address;

    @NotBlank(message = "Giới tính không được trống")
    @Size(max = 10, message = "Giới tính không hợp lệ")
    @Schema(name = "gender", description = "Giới tính của hr", defaultValue = "MALE")
    private String gender;

    @Size(max = 1000, message = "linkedIn không quá 1000 ký tự")
    @Schema(name = "linkedIn", description = "Link linkedin")
    private String linkedIn;

    @Size(max = 3000, message = "Giới thiệu không quá 3000 ký tự")
    @Schema(name = "introduction", description = "Giới thiệu", defaultValue = "Tôi rất đẹp trai")
    private String introduction;
}
