package com.chechen.services.vm.userinfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

/**
 * @author thanglv on 5/15/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserVM implements Serializable {
    @NotBlank
    @Size(max = 100, message = "Username không dài quá 100 ký tự")
    private String username;
    @NotBlank
    @Size(max = 100, message = "Username không dài quá 100 ký tự")
    private String fullName;
    @NotNull
    @Email
    private String email;
    private Set<String> roles;
    @NotBlank
    @Size(max = 100, message = "password không dài quá 8 ký tự")
    private String password;
    @NotBlank
    @Size(max = 11, message = "phone không dài quá 11 ký tự")
    private String phone;
}
