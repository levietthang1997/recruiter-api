package com.chechen.services.vm.userinfo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author thanglv on 05/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
@ToString
public class UserRegisterVM implements Serializable {

    @NotBlank(message = "Họ không được trống")
    @Size(max = 30, message = "firstName không dài quá 30 ký tự")
    @Schema(name = "firstName",  required = true, description = "Họ", defaultValue = "Lê")
    private String firstName;

    @NotBlank
    @Size(max = 30, message = "lastName không dài quá 30 ký tự")
    @Schema(name = "lastName",  required = true, description = "Tên đệm và tên chính", defaultValue = "Việt Thắng")
    private String lastName;

    @NotBlank
    @Size(max = 20, message = "Username không dài quá 20 ký tự")
    @Schema(name = "username",  required = true, description = "Tên đăng nhập", defaultValue = "mlemlem")
    private String username;

    @NotBlank
    @Size(min = 4, max = 20, message = "Password không dài quá 20 ký tự")
    @Schema(name = "password",  required = true, description = "Mật khẩu", defaultValue = "12345678")
    private String password;

    @NotBlank(message = "Email không được trống")
    @Email(message = "Email không hợp lệ")
    @Schema(name = "email",  required = true, description = "Email", defaultValue = "levietthang1997@gmail.com")
    private String email;

    @NotBlank(message = "Số điện thoại không được trống")
    @Size(min = 9, max = 11, message = "Phone không dài quá 11 ký tự, ngắn quá 9 ký tự")
    @Schema(name = "phone",  required = true, description = "Số điện thoại", defaultValue = "0981576404")
    private String phone;

    // đăng ký dưới hình thức công ty hoặc là headhunt
    // Nếu mà là công ty thì required trường này
    private String company;

    @NotBlank
    @Size(max = 10, message = "registrationType không hợp lệ")
    @Schema(name = "registrationType",  required = true, description = "Loại đăng ký (headhunt hay công ty)", defaultValue = "HEADHUNT/COMPANY")
    private String registrationType;
}
