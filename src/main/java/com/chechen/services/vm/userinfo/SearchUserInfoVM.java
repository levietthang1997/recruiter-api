package com.chechen.services.vm.userinfo;

import com.chechen.config.annotation.SearchColumn;
import com.chechen.obj.DataType;
import com.chechen.obj.SearchType;
import com.chechen.services.vm.BaseObjPagination;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author thanglv on 13/04/2022
 * @project recruiter-api
 */
@Data
public class SearchUserInfoVM extends BaseObjPagination {
    @Schema(description = "Tên đăng nhập", defaultValue = "administrator")
    @SearchColumn(columnName = "username", dataType = DataType.STRING, searchType = SearchType.MATCH)
    private String username;
    @Schema(description = "Email", defaultValue = "levietthang1997@gmail.com")
    @SearchColumn(columnName = "email", dataType = DataType.STRING, searchType = SearchType.MATCH)
    private String email;
    @Schema(description = "Họ và tên", defaultValue = "Thắng đẹp trai vkl")
    @SearchColumn(columnName = "fullName", dataType = DataType.STRING, searchType = SearchType.MATCH)
    private String fullName;
    @Schema(description = "Nhóm quyền", defaultValue = "ADMIN")
    @SearchColumn(columnName = "roles.name", dataType = DataType.STRING, searchType = SearchType.MATCH)
    private String roleName;

    @Schema(description = "isDeleted", defaultValue = "0")
    @SearchColumn(columnName = "isDeleted", dataType = DataType.INTEGER, searchType = SearchType.MATCH)
    private String isDeleted;

//    @SearchColumn(columnName = "fromDate", dataType = DataType.DATE, searchType = SearchType.RANGE, position = Position.LOWER)
//    private String fromDate;
//    @SearchColumn(columnName = "toDate", dataType = DataType.DATE, searchType = SearchType.RANGE, position = Position.UPPER)
//    private String toDate;
}
