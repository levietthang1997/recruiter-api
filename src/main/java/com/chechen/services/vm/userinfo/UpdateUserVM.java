package com.chechen.services.vm.userinfo;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

/**
 * @author thanglv on 17/05/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
public class UpdateUserVM implements Serializable {
    @NotNull(message = "Id không được trống")
    @Size(max = 255, message = "id không quá 255 ký tự")
    private String id;
    @NotBlank
    @Size(max = 100, message = "Username không dài quá 100 ký tự")
    private String username;
    private Set<String> roles;
    @NotNull
    @Email
    private String email;
    @NotBlank
    @Size(max = 11, message = "phone không dài quá 11 ký tự")
    private String phone;
    @NotBlank
    @Size(max = 100, message = "Username không dài quá 100 ký tự")
    private String fullName;
}
