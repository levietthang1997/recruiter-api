package com.chechen.services.vm.userinfo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class ChangePasswordVM implements Serializable {
    @NotBlank(message = "Mật khẩu cũ không được trống")
    @Size(min = 4, max = 20, message = "Mật khẩu cũ không dài quá 20 ký tự")
    @Schema(name = "oldPassword",  required = true, description = "Mật khẩu cũ", defaultValue = "12345678")
    private String oldPassword;

    @NotBlank(message = "Mật khẩu mới không được trống")
    @Size(min = 4, max = 20, message = "Mật khẩu mới không dài quá 20 ký tự")
    @Schema(name = "newPassword",  required = true, description = "Mật khẩu mới", defaultValue = "12345678")
    private String newPassword;

    @NotBlank(message = "Mật khẩu xác nhận không được trống")
    @Size(min = 4, max = 20, message = "Mật khẩu xác nhận không dài quá 20 ký tự")
    @Schema(name = "confirmPassword",  required = true, description = "Mật khẩu xác nhận", defaultValue = "12345678")
    private String confirmPassword;
}
