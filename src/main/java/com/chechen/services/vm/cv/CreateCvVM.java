package com.chechen.services.vm.cv;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Data
public class CreateCvVM implements Serializable {
    // họ
    @Schema(name = "firstName", required = true, description = "First name", defaultValue = "Lê")
    @NotBlank(message = "firstName không được phép trống")
    @Size(max = 50, message = "firstName không quá 50 ký tự")
    private String firstName;

    // tên
    @Schema(name = "lastName", required = true, description = "last name", defaultValue = "Văn Vui")
    @NotBlank(message = "lastName không được phép trống")
    @Size(max = 50, message = "lastName không quá 50 ký tự")
    private String lastName;

    // email ứng viên
    @Email(message = "email không đúng định dạng")
    @Size(max = 200, message = "email không quá 500 ký tự")
    @Schema(name = "email", description = "contactEmail ", example = "email@gmail.com")
    private String email;

    // số điện thoại ứng viên
    @NotBlank(message = "Số điện thoại không được trống")
    @Size(min = 9, max = 11, message = "Phone không dài quá 11 ký tự, ngắn quá 9 ký tự")
    @Schema(name = "phone",  required = true, description = "Số điện thoại", defaultValue = "0981576404")
    private String phone;

    // ngày sinh của ứng viên
    @NotBlank(message = "birthDay không được trống")
    @Size(min = 10, max = 10, message = "birthDay không hợp lệ, dd/MM/yyyy")
    @Schema(name = "birthDay",  required = true, description = "Ngày tháng năm sinh", defaultValue = "20/10/2022")
    private String birthDay;

    // website cá nhân của ứng viên
    @Size(max = 10, message = "birthDay không hợp lệ, dd/MM/yyyy")
    @Schema(name = "birthDay",  required = true, description = "Ngày tháng năm sinh", defaultValue = "20/10/2022")
    private String website;

    // tiêu đề
    @NotBlank(message = "displayTitle không được trống")
    @Size(max = 500, message = "displayTitle không quá 500 ký tự")
    @Schema(name = "displayTitle",  required = true, description = "Tiêu đề", defaultValue = "20/10/2022")
    private String displayTitle;

    // khu vực tuyển dụng
    @NotEmpty(message = "locations không được trống")
    @Size(max = 100, message = "locations không quá 100 phần tử")
    @Schema(name = "locations",  required = true, description = "Khu vực tuyển dụng")
    private Set<String> locations = new HashSet<>();

    // Danh mục công việc (ở màn upload cv: UI/UX, backend dev, frontend dev, teacher)
    @NotEmpty(message = "jobCategories không được trống")
    @Size(max = 100, message = "jobCategories không quá 100 phần tử")
    @Schema(name = "jobCategories",  required = true, description = "Loại công việc")
    private Set<String> jobCategories = new HashSet<>();

    // thời gian bắt đầu làm
    @NotBlank(message = "candidateAvailability không được trống")
    @Size(max = 500, message = "candidateAvailability không quá 500 ký tự")
    @Schema(name = "candidateAvailability", description = "Thời gian bắt đầu đi làm của ứng viên ", example = "Sau 1 tháng nữa")
    private String candidateAvailability;

    // Loại công việc (IT hay NON IT)
    @NotBlank(message = "jobType không được trống")
    @Size(max = 10, message = "jobType không quá 10 ký tự")
    @Schema(name = "jobType", description = "Job type")
    private String jobType;

    // Có thoả thuận mức lương hay không , Y là có, N là Không
    @NotBlank(message = "isNegotiableSalary không được trống")
    @Size(max = 1, message = "isNegotiableSalary chỉ được Y/N")
    @Schema(name = "isNegotiableSalary", description = "Lương thoả thuận hay là không", required = true)
    private String isNegotiableSalary;

    // lương thoả thuận cao nhất
    @Min(value = 0, message = "salaryExpectMax phải lớn hơn 0")
    @Max(value = 9_000_000_000_000L, message = "salaryExpectMax không quá 9_000_000_000_000")
    @Schema(name = "salaryExpectMax", description = "Lương max", required = true)
    private BigDecimal salaryExpectMax;

    // lương thoả thuận thấp nhất
    @Min(value = 0, message = "salaryExpectMin phải lớn hơn 0")
    @Max(value = 9_000_000_000_000L, message = "salaryExpectMin không quá 9_000_000_000_000")
    @Schema(name = "salaryExpectMin", description = "Lương min", required = true)
    private BigDecimal salaryExpectMin;

    // sơ lược về cv
    @NotBlank(message = "summary không được trống")
    @Size(max = 1000, message = "summary không quá 1000 ký tự")
    @Schema(name = "summary",  required = true, description = "Sơ lược về cv", defaultValue = "Cv này là cv xịn")
    private String summary;

    // id của file mà chưa che
    @NotBlank(message = "fileIdOriginal không được trống")
    @Size(max = 500, message = "fileIdOriginal không quá 500 ký tự")
    @Schema(name = "fileIdOriginal",  required = true, description = "File cv không bị ẩn thông tin", defaultValue = "File cv không bị ẩn thông tin")
    private String fileIdOriginal;

    // id của file cv mà đã bị che
    @NotBlank(message = "fileIdHidden không được trống")
    @Size(max = 500, message = "fileIdHidden không quá 500 ký tự")
    @Schema(name = "fileIdHidden",  required = true, description = "File cv bị ẩn thông tin", defaultValue = "File cv bị ẩn thông tin")
    private String fileIdHidden;

}
