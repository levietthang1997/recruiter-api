package com.chechen.services.vm.file;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DeleteFileUploadPolicyVM implements Serializable {
    private List<String> listId;
}
