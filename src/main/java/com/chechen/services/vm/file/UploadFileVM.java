package com.chechen.services.vm.file;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
public class UploadFileVM implements Serializable {
    @NotNull(message = "Tệp không được trống")
    @Schema(name = "files",  required = true, description = "Files", type = "object")
    private MultipartFile[] files;
}
