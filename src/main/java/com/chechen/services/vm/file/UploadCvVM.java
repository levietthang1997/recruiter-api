package com.chechen.services.vm.file;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class UploadCvVM implements Serializable {
    @NotNull(message = "Tệp không được trống")
    @Schema(name = "file",  required = true, description = "File", type = "object")
    private MultipartFile file;
}
