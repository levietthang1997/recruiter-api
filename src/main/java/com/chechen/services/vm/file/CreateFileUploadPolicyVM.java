package com.chechen.services.vm.file;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateFileUploadPolicyVM implements Serializable {

    @Schema(name = "ext", required = true, description = "File extension", defaultValue = "pdf")
    @NotBlank(message = "ext không được phép trống")
    @Size(max = 50, message = "ext không quá 50 ký tự")
    private String ext;
    @Schema(name = "maxFileSize", required = true, description = "Max file size", defaultValue = "10485760")
    private Long maxFileSize;
}
