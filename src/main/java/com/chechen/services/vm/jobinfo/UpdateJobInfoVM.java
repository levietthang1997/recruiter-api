package com.chechen.services.vm.jobinfo;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@NoArgsConstructor
@Data
public class UpdateJobInfoVM implements Serializable {

    private String id;

    @NotBlank(message = "Job title không được trống")
    @Size(max = 500, message = "title không quá 500 ký tự")
    private String title;

    @NotNull(message = "skills không được null")
    private List<String> skills;

    // thoả thuận lương
    @NotBlank(message = "isNegotiableSalary không được trống")
    @Size(max = 1, message = "isNegotiableSalary chỉ được Y/N")
    private String isNegotiableSalary;

    @Min(value = 0, message = "Lương cao nhất phải lớn hơn 0")
    @Max(value = 9_000_000_000_000L, message = "Lương cao nhất không quá 9_000_000_000_000")
    private BigDecimal salaryTo;

    @Min(value = 0, message = "Lương tối thiểu phải lớn hơn 0")
    @Max(value = 9_000_000_000_000L, message = "Lương tối thiểu không quá 9_000_000_000_000")
    private BigDecimal salaryFrom;

    @Min(value = 1, message = "Số lượng vị trí còn trống phải lớn hơn 0")
    @Max(value = 999999999, message = "Số lượng vị trí còn trống không quá 999999999")
    private Integer vacancies;

    @Min(value = 1, message = "Team size phải lớn hơn 0")
    @Max(value = 999999999, message = "Team size trống không quá 999999999")
    private Integer teamSize;

    @NotBlank(message = "Phòng ban không được trống")
    @Size(max = 1000, message = "Phòng ban không quá 1000 ký tự")
    private String department;

    @NotBlank(message = "Loại công việc không được trống")
    @Size(max = 10, message = "Loại công việc không quá 10 ký tự")
    private String jobType;

    @NotNull(message = "locations không được null")
    private List<String> locations;

    @NotBlank(message = "Địa chỉ không được trống")
    @Size(max = 1000, message = "Địa chỉ làm việc không được quá 1000 ký tự")
    private String address;

    @NotBlank(message = "Job overview không được trống")
    @Size(max = 30000, message = "Job overview không được quá 30000 ký tự")
    private String jobOverView;

    @NotBlank(message = "Job requirement không được trống")
    @Size(max = 30000, message = "Job requirement không được quá 30000 ký tự")
    private String jobRequirement;

    @NotBlank(message = "Priority skill không được trống")
    @Size(max = 30000, message = "Priority skill không được quá 30000 ký tự")
    private String prioritySkill;

    @Size(max = 30000, message = "Lý do không được quá 30000 ký tự")
    private String whyWorkingHere;

    @NotBlank(message = "Interview process không được trống")
    @Size(max = 30000, message = "Interview process không được quá 30000 ký tự")
    private String interviewingProcess;

    @NotBlank(message = "employeeType không được trống")
    @Size(max = 1000, message = "employeeType không được quá 2000 ký tự")
    private String employeeType;

    @NotNull(message = "levels không được null")
    private List<String> levels;

    @NotBlank(message = "Interview process không được trống")
    @Size(max = 30000, message = "Thông báo cho headhunter không quá 30000 ký tự")
    private String noticeToHeadhunter;
}
