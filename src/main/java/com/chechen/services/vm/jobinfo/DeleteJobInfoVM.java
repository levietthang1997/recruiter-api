package com.chechen.services.vm.jobinfo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Data
public class DeleteJobInfoVM implements Serializable {
    @NotBlank(message = "Id không được null")
    @Size(max = 255, message = "ID không quá 255 ký tự")
    private String id;
}
