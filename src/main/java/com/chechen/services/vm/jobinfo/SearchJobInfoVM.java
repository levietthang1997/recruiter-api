package com.chechen.services.vm.jobinfo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Min;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SearchJobInfoVM implements Serializable {
    @Schema(name = "location", description = "Id vị trí tìm kiếm", defaultValue = "")
    private String location;
    @Schema(name = "jobType", description = "JobType tìm kiếm", defaultValue = "")
    private String jobType;
    @Schema(name = "keyword", description = "keyword tìm kiếm", defaultValue = "")
    private String keyword;
    @Schema(name = "salaryFrom", description = "Mức lương nhỏ nhất tìm kiếm", defaultValue = "")
    private BigDecimal salaryFrom;
    @Schema(name = "salaryTo", description = "Mức lương tối đa tìm kiếm", defaultValue = "")
    private BigDecimal salaryTo;
    @Schema(name = "sort", description = "Tiêu chí sắp xếp", defaultValue = "")
    private String sort;
    @Schema(name = "order", description = "ASC hoặc DESC", defaultValue = "")
    private String order;
    @Schema(name = "employeeType", description = "Loại công việc (Part time/Full time/ Other)", defaultValue = "")
    private String employeeType;
    @Schema(name = "page", description = "Không truyền thì mặc định là page index = 0", defaultValue = "")
    @Min(value = 0, message = "page không được < 0")
    private Integer page;
}
