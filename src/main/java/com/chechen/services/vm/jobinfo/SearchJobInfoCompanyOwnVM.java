package com.chechen.services.vm.jobinfo;

import com.chechen.services.vm.BaseObjPagination;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Size;

/**
 * @author thanglv on 26/06/2022
 * @project recruiter-api
 */
@Data
@ToString
public class SearchJobInfoCompanyOwnVM extends BaseObjPagination {
    @Schema(name = "title", description = "Tên job")
    @Size(max = 500, message = "Tên job không được quá 500 ký tự")
    private String title;
    @Schema(name = "jobType", description = "Loại job")
    @Size(max = 100, message = "Loại job không được quá 100 ký tự")
    private String jobType;
    @Schema(name = "pendingTaskStatus", description = "Trạng thái chờ phê duyệt của job")
    @Size(max = 100, message = "Trạng thái của job không quá 100 ký tự")
    private String pendingTaskStatus;
}
