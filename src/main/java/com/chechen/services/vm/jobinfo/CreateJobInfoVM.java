package com.chechen.services.vm.jobinfo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateJobInfoVM implements Serializable {
    @NotBlank(message = "Job title không được trống")
    @Size(max = 300)
    private String title;

    @NotNull(message = "Danh sách kỹ năng không được trống")
    @NotEmpty(message = "Danh sách kỹ năng không được trống")
    private Set<String> skills;

    @NotBlank(message = "Thoả thuận lương không được trống")
    @Size(max = 1)
    private String isNegotiableSalary;

    @Min(value = 0, message = "Lương cao nhất phải lớn hơn 0")
    @Max(value = 9_000_000_000_000L, message = "Lương cao nhất không quá 9_000_000_000_000")
    private BigDecimal salaryTo;

    @Min(value = 0, message = "Lương tối thiểu phải lớn hơn 0")
    @Max(value = 9_000_000_000_000L, message = "Lương tối thiểu không quá 9_000_000_000_000")
    private BigDecimal salaryFrom;

    @Min(value = 1, message = "Số lượng vị trí còn trống phải lớn hơn 0")
    @Max(value = 999999999, message = "Số lượng vị trí còn trống không quá 999999999")
    private Integer vacancies;

    @Min(value = 1, message = "Team size phải lớn hơn 0")
    @Max(value = 999999999, message = "Team size trống không quá 999999999")
    private Integer teamSize;

    @NotBlank(message = "Phòng ban không được trống")
    @Size(max = 1000, message = "Phòng ban không quá 1000 ký tự")
    private String department;

    @NotBlank(message = "Loại công việc không được trống")
    @Size(max = 10, message = "Loại công việc không quá 10 ký tự")
    private String jobType;

    @NotNull(message = "Danh sách vị trí không được trống")
    @NotEmpty(message = "Danh sách vị trí không được trống")
    private Set<String> locations;

    @NotBlank(message = "Địa chỉ không được trống")
    @Size(max = 1000, message = "Địa chỉ làm việc không được quá 1000 ký tự")
    private String address;

    @NotBlank(message = "Job overview không được trống")
    @Size(max = 30000, message = "Job overview không được quá 30000 ký tự")
    private String jobOverView;

    @NotBlank(message = "Job requirement không được trống")
    @Size(max = 30000, message = "Job requirement không được quá 30000 ký tự")
    private String jobRequirement;

    @NotBlank(message = "prioritySkill không được trống")
    @Size(max = 30000, message = "prioritySkill không được quá 30000 ký tự")
    private String prioritySkill;

    @Size(max = 30000, message = "Lý do không được quá 30000 ký tự")
    private String whyWorkingHere;

    @NotBlank(message = "Interview process không được trống")
    @Size(max = 30000, message = "Interview process không được quá 30000 ký tự")
    private String interviewingProcess;

    @NotBlank(message = "employeeType không được trống")
    @Size(max = 1000, message = "employeeType không được quá 1000 ký tự")
    private String employeeType;

    @NotNull(message = "Danh sách level không được trống")
    @NotEmpty(message = "Danh sách level không được trống")
    private Set<String> levels;

    @NotBlank(message = "Interview process không được trống")
    @Size(max = 30000, message = "Thông báo cho headhunter không quá 30000 ký tự")
    private String noticeToHeadhunter;

    @NotBlank(message = "Bật tìm kiếm recruitery không được trống")
    @Schema(name = "isPlatform", description = "Bật tìm kiếm trên nền tảng Recruitery")
    private String isPlatform;
}
