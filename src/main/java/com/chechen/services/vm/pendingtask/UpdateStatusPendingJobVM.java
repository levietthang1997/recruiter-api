package com.chechen.services.vm.pendingtask;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author thanglv on 10/07/2022
 * @project recruiter-api
 */
@Data
public class UpdateStatusPendingJobVM implements Serializable {
    @NotBlank(message = "pendingTaskId không được trống")
    private String pendingTaskId;
    @NotNull(message = "reward không được null")
    private BigDecimal reward;
    @NotNull(message = "reward không được null")
    private Integer priorityJob;
    @NotBlank(message = "wfStatusCd không được null")
    private String wfStatusCd;
}
