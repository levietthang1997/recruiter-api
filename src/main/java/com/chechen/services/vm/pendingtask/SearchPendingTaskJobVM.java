package com.chechen.services.vm.pendingtask;

import com.chechen.services.vm.BaseObjPagination;
import lombok.Data;

import java.io.Serializable;

/**
 * @author thanglv on 30/06/2022
 * @project recruiter-api
 */
@Data
public class SearchPendingTaskJobVM extends BaseObjPagination implements Serializable {
    private String companyId;
    private String wfStatusCd;
}
