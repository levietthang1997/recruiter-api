package com.chechen.services.vm;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @author thanglv on 08/04/2022
 * @project recruiter-api
 */
@Data
public abstract class BaseObjPagination {
    @Min(value = 0, message = "page min is 0")
    @Max(value = Integer.MAX_VALUE, message = "page max is 2147483647")
    @Schema(defaultValue = "0")
    private int page;

    @Min(value = 1, message = "size min is 1")
    @Max(value = 1000, message = "size max is 1000")
    @Schema(defaultValue = "10")
    private int size;
}
