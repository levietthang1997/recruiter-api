package com.chechen.services.vm.location;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Data
public class CreateLocationVM implements Serializable {
    @NotBlank(message = "name không được trống")
    @Size(max = 500, message = "name không qu 500 ký tự")
    private String name;
}
