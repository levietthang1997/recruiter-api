package com.chechen.services.vm.location;

import com.chechen.config.annotation.SearchColumn;
import com.chechen.obj.DataType;
import com.chechen.obj.SearchType;
import com.chechen.services.vm.BaseObjPagination;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author thanglv on 05/06/2022
 * @project recruiter-api
 */
@NoArgsConstructor
@Data
public class SearchLocationVM extends BaseObjPagination {
    @Schema(description = "Nơi làm việc", defaultValue = "Hà Nội")
    @SearchColumn(columnName = "name", dataType = DataType.STRING, searchType = SearchType.MATCH)
    private String name;

    @Schema(description = "isDeleted", defaultValue = "0")
    @SearchColumn(columnName = "isDeleted", dataType = DataType.STRING, searchType = SearchType.MATCH)
    @JsonIgnore
    private String isDeleted;

}
