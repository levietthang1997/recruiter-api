package com.chechen.services.vm.jobrole;

import com.chechen.config.annotation.SearchColumn;
import com.chechen.obj.DataType;
import com.chechen.obj.SearchType;
import com.chechen.services.vm.BaseObjPagination;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Data
public class SearchJobRoleVM extends BaseObjPagination {
    @Schema(description = "Tên", defaultValue = "name")
    @SearchColumn(columnName = "name", dataType = DataType.STRING, searchType = SearchType.MATCH)
    private String name;

    @Schema(description = "isDeleted", defaultValue = "0")
    @SearchColumn(columnName = "isDeleted", dataType = DataType.STRING, searchType = SearchType.MATCH)
    @JsonIgnore
    private String isDeleted;

}
