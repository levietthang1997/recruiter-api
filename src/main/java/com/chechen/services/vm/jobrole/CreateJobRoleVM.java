package com.chechen.services.vm.jobrole;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class CreateJobRoleVM implements Serializable {
    @NotBlank(message = "name không được trống")
    @Size(max = 200, message = "name không quá 200 ký tự")
    @Schema(name = "name", description = "Tên job role")
    private String name;
}
