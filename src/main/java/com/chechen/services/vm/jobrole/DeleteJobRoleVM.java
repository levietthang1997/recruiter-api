package com.chechen.services.vm.jobrole;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Data
public class DeleteJobRoleVM implements Serializable {
    private Set<String> listId;
}
