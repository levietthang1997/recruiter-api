package com.chechen.services.vm.company;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
public class UpdateCompanyIndustryVM implements Serializable {

    @Schema(name = "id", description = "Mã ngành nghề", defaultValue = "1", required = true)
    @NotNull(message = "Mã ngành nghề không được trống")
    @Size(max = 255, message = "Mã ngành nghề không quá 255 ký tự")
    private String id;

    @NotBlank
    @Size(max = 200, message = "Tên ngành nghề không quá 100 ký tự")
    @Schema(name = "name", description = "Tên ngành nghề", defaultValue = "Buôn bán thuốc lắc", required = true)
    private String name;
}
