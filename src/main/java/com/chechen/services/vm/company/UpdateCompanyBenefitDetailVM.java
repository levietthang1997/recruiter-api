package com.chechen.services.vm.company;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
@Data
public class UpdateCompanyBenefitDetailVM implements Serializable {
    @NotNull(message = "Benefit không được trống")
    @Size(max = 255, message = "Benefit không quá 255 ký tự")
    private String benefitId;
    @Size(max = 500, message = "Chi tiết lợi ích không được quá 500 ký tự")
    private String detail;
}
