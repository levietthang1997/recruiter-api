package com.chechen.services.vm.company;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
public class UpdateCompanyProfileVM implements Serializable {

    @NotBlank
    @Size(max = 500, message = "Tên công ty không quá 500 ký tự")
    @Schema(name = "companyName", description = "Tên công ty", required = true)
    private String companyName;

    @Size(max = 20, message = "Số điện thoại công ty không quá 20 ký tự")
    @Schema(name = "companyPhone", description = "Số điện thoại công ty", required = false)
    private String companyPhone;

    @Size(max = 500, message = "Địa chỉ công ty không quá 500 ký tự")
    @Schema(name = "companyAddress", description = "Địa chỉ công ty", required = false)
    private String companyAddress;

    // mapping với frontend, companySize này là mã code
    // size = 1 là dưới 100
    // size = 2 là từ 100 tới 300 ... cho tới 8
    // frontend dựa vào company size này để mapping hiển thị
    @Schema(name = "companySize", description = "Số lượng người trong công ty (Cấu hình mapping với frontend size = 1 thì là dưới 100 người)", required = true)
    @Min(value = 1, message = "CompanySize từ 1 tới 8")
    @Max(value = 8, message = "CompanySize từ 1 tới 8")
    private Integer companySize;

    @Schema(name = "companyEmail", description = "Business email công ty", required = true)
    @NotBlank(message = "Email không được trống")
    @Email(message = "Email không hợp lệ")
    @Size(max = 200, message = "Email không dài quá 200 ký tự")
    private String companyEmail;

    @Schema(name = "ccMail", description = "Mail phụ email công ty", required = false)
    @Email(message = "Email không hợp lệ")
    @Size(max = 200, message = "Email không dài quá 200 ký tự")
    private String ccMail;

    @Schema(name = "website", description = "Website công ty", required = false)
    @Size(max = 200, message = "Website không dài quá 200 ký tự")
    private String website;

    @Schema(name = "companyIndustries", description = "List các mã ngành nghề", required = false)
    private Set<String> companyIndustries;

    @Schema(name = "workingHours", description = "Số giờ làm việc", required = false)
    @Min(value = 0, message = "Số giờ làm việc không được nhỏ hơn 0")
    @Max(value = 24, message = "Số giờ làm việc không quá 24h")
    private Integer workingHours;

    @NotNull(message = "Danh sách lợi ích không được trống")
    @Schema(name = "benefitDetails", description = "Danh sách lợi ích", required = true)
    private List<UpdateCompanyBenefitDetailVM> benefitDetails;

    @Size(max = 200, message = "LogoId không quá 200 ký tự")
    @Schema(name = "fileLogoId", description = "Logo công ty (Nhận dạng công ty)")
    private String fileLogoId;

    @Size(max = 1000, message = "Mô tả công ty không quá 1000 ký tự")
    @Schema(name = "description", description = "Mô tả (logo) công ty (Nhận dạng công ty) (Angular editor)", example = "<p>Ngân hàng lớn nhất VN</p>")
    private String description;

    @Schema(name = "gallery", description = "List fileId (id ảnh upload)")
    private List<String> gallery;

    @Size(max = 200, message = "thumbnailLogoId không quá 200 ký tự")
    @Schema(name = "thumbnailLogoId", description = "Ảnh công ty dùng để chia sẻ lên mạng xã hội")
    private String thumbnailLogoId;

    @Size(max = 1000, message = "Thumbnail description (Mô tả thumbnail) không quá 1000 ký tự")
    @Schema(name = "thumbnailDescription", description = "Mô tả thumbnail công ty (Tiêu đề dưới ảnh khi chia sẻ lên mạng xã hội)", example = "Mô tả ở thumbnail công ty")
    private String thumbnailDescription;

    @Size(max = 1000, message = "linkedIn không quá 1000 ký tự")
    @Schema(name = "linkedIn", description = "Link linkedin")
    private String linkedIn;

    @Size(max = 1000, message = "facebook không quá 1000 ký tự")
    @Schema(name = "facebook", description = "Link facebook")
    private String facebook;

    @Size(max = 1000, message = "Họ người đại diện không quá 1000 ký tự")
    @Schema(name = "firstNameRepresentative", description = "Họ người đại diện công ty")
    private String firstNameRepresentative;

    @Size(max = 1000, message = "Tên người đại diện không quá 1000 ký tự")
    @Schema(name = "lastNameRepresentative", description = "Tên người đại diện công ty")
    private String lastNameRepresentative;

    @Size(max = 200, message = "Chức danh người đại diện không quá 200 ký tự")
    @Schema(name = "headline", description = "Chức danh người đại diện công ty", example = "Thư ký giám đốc")
    private String headline;

    @Email(message = "Email người đại diện không hợp lệ")
    @Size(max = 200, message = "Email người đại diện không quá 200 ký tự")
    @Schema(name = "emailRepresentative", description = "Email người đại diện công ty", example = "thuky@kttk.xyz")
    private String emailRepresentative;

    @Size(max = 20, message = "Số điện thoại người đại diện công ty không quá 100 ký tự")
    @Schema(name = "phoneRepresentative", description = "Số điện thoại người đại diện công ty", example = "0987654787")
    private String phoneRepresentative;
}
