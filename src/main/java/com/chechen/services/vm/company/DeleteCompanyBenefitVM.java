package com.chechen.services.vm.company;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
@Data
public class DeleteCompanyBenefitVM implements Serializable {
    private List<String> listId;
}
