package com.chechen.services.vm.company;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
@Data
public class DeleteIndustryVM implements Serializable {
    private Set<String> listId;
}
