package com.chechen.services.vm.company;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
@Data
public class UpdateCompanyBenefitVM implements Serializable {
    @NotNull(message = "id không được trống")
    @Size(max = 255, message = "id không quá 255 ký tự")
    private String id;
    @Schema(name = "name", description = "Tên lợi ích", required = true, example = "13th month salary")
    @NotBlank(message = "Tên lợi ích không được trống")
    @Size(max = 500, message = "Tên lợi ích không quá 500 ký tự")
    private String name;
    @Schema(name = "icon", description = "ngZoro icon name", example = "step-forward")
    @Size(max = 100, message = "icon không quá 100 ký tự")
    private String icon;
}
