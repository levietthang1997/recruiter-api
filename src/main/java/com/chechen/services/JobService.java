package com.chechen.services;

import com.chechen.dto.*;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.jobinfo.*;

import java.util.List;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
public interface JobService {
    BasicResponseDto save(CreateJobInfoVM request) throws BusinessException;

    BasicResponseDto update(UpdateJobInfoVM request) throws BusinessException;

    BasicResponseDto delete(DeleteJobInfoVM request) throws BusinessException;

    BasicResponseDto<PaginationDto<JobInfoDtoV2>> search(SearchJobInfoVM request);

    BasicResponseDto<PaginationDto<JobInfoDto>> searchJobInfoCompanyOwn(SearchJobInfoCompanyOwnVM request);

    BasicResponseDto<List<JobInfoDtoV2>> get10JobHomePage();

    BasicResponseDto<JobInfoDtoV3> getJobPendingDetail(String pendingTaskId) throws BusinessException;

    BasicResponseDto<JobInfoDtoV4> getJobInfoDetail(String jobId) throws BusinessException;
}
