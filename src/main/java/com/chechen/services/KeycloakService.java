package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.entity.AccountInfo;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.userinfo.UserRegisterVM;

/**
 * @author thanglv on 05/06/2022
 * @project recruiter-api
 */
public interface KeycloakService {
    BasicResponseDto registerNewAccount(String authUrl, String realm, String username, String password, String clientId, UserRegisterVM userRegisterVM, AccountInfo accountInfo) throws BusinessException;
}
