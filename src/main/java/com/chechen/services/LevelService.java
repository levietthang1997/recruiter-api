package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.LevelDto;
import com.chechen.dto.PaginationDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.level.CreateLevelVM;
import com.chechen.services.vm.level.DeleteLevelVM;
import com.chechen.services.vm.level.SearchLevelVM;
import com.chechen.services.vm.level.UpdateLevelVM;

import java.util.List;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
public interface LevelService {
    BasicResponseDto save(CreateLevelVM request);

    BasicResponseDto update(UpdateLevelVM request) throws BusinessException;

    BasicResponseDto delete(DeleteLevelVM request) throws BusinessException;

    BasicResponseDto<PaginationDto<LevelDto>> search(SearchLevelVM searchSkillVM);

    BasicResponseDto<List<LevelDto>> findAll();
}
