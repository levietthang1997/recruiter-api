package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.CompanyProfileDto;
import com.chechen.dto.CompanyProfileDtoV2;
import com.chechen.dto.CompanyPublicProfileDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.company.UpdateCompanyProfileVM;

import java.util.List;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
public interface CompanyService {
    BasicResponseDto<CompanyProfileDto> updateProfile(UpdateCompanyProfileVM request) throws BusinessException;

    BasicResponseDto<CompanyProfileDto> getProfileCompany() throws BusinessException;

    BasicResponseDto<List<CompanyProfileDtoV2>> getAllCompany();

    BasicResponseDto<CompanyPublicProfileDto> getPublicProfile(String companyId) throws BusinessException;
}
