package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.LocationDto;
import com.chechen.dto.PaginationDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.location.CreateLocationVM;
import com.chechen.services.vm.location.DeleteLocationVM;
import com.chechen.services.vm.location.SearchLocationVM;
import com.chechen.services.vm.location.UpdateLocationVM;

import java.util.List;

public interface LocationService {
    BasicResponseDto save(CreateLocationVM locationVM);

    BasicResponseDto update(UpdateLocationVM request) throws  BusinessException;

    BasicResponseDto delete(DeleteLocationVM request) throws  BusinessException;

    BasicResponseDto<PaginationDto<LocationDto>> search(SearchLocationVM searchLocationVM);

    BasicResponseDto<List<LocationDto>> findAll();
}
