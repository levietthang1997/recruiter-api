package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.FileManagementDto;
import com.chechen.entity.FileManagement;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.file.UploadFileVM;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
public interface FileManagementService {
    BasicResponseDto<List<FileManagementDto>> uploadMultipleFile(UploadFileVM uploadFileVM, boolean isPublic) throws BusinessException, IOException;

    void viewFileUpload(String fileId, HttpServletResponse httpServletResponse) throws IOException;

    BasicResponseDto<List<FileManagementDto>> uploadFileLogo(UploadFileVM uploadFileVM) throws BusinessException, IOException;

    FileManagement uploadSingleFile(MultipartFile file, boolean isPublic);
    void validateFile(MultipartFile file) throws BusinessException;
}
