package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.FileUploadPolicyDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.file.CreateFileUploadPolicyVM;
import com.chechen.services.vm.file.DeleteFileUploadPolicyVM;

import java.util.List;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
public interface FileUploadPolicyService {
    BasicResponseDto create(CreateFileUploadPolicyVM fileUploadPolicyVM) throws BusinessException;

    BasicResponseDto<List<FileUploadPolicyDto>> findAll();

    BasicResponseDto delete(DeleteFileUploadPolicyVM request);
}
