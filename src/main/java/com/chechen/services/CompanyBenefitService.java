package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.CpBenefitDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.company.CreateCompanyBenefitVM;
import com.chechen.services.vm.company.DeleteCompanyBenefitVM;
import com.chechen.services.vm.company.UpdateCompanyBenefitVM;

import java.util.Set;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
public interface CompanyBenefitService {
    BasicResponseDto create(CreateCompanyBenefitVM request);

    BasicResponseDto update(UpdateCompanyBenefitVM request) throws BusinessException;

    BasicResponseDto<Set<CpBenefitDto>> getAll();

    BasicResponseDto delete(DeleteCompanyBenefitVM request) throws BusinessException;
}
