package com.chechen.services;

import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.PaginationDto;
import com.chechen.dto.PendingTaskDto;
import com.chechen.exception.BusinessException;
import com.chechen.services.vm.pendingtask.SearchPendingTaskJobVM;
import com.chechen.services.vm.pendingtask.UpdateStatusPendingJobVM;

/**
 * @author thanglv on 30/06/2022
 * @project recruiter-api
 */
public interface PendingTaskService {
    BasicResponseDto<PaginationDto<PendingTaskDto>> getPendingTaskJob(SearchPendingTaskJobVM request);

    BasicResponseDto updateStatusPendingJob(UpdateStatusPendingJobVM request) throws BusinessException;
}
