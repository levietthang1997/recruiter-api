package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.JobRoleDto;
import com.chechen.dto.LevelDto;
import com.chechen.dto.PaginationDto;
import com.chechen.entity.JobRole;
import com.chechen.entity.Level;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.JobRoleMapper;
import com.chechen.repository.JobRoleRepository;
import com.chechen.repository.SearchRepository;
import com.chechen.services.JobRoleService;
import com.chechen.services.vm.jobrole.CreateJobRoleVM;
import com.chechen.services.vm.jobrole.DeleteJobRoleVM;
import com.chechen.services.vm.jobrole.SearchJobRoleVM;
import com.chechen.services.vm.jobrole.UpdateJobRoleVM;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class JobRoleServiceImpl implements JobRoleService {

    private final JobRoleRepository jobRoleRepository;
    private final SearchRepository<JobRole, SearchJobRoleVM> searchRepository;
    private final JobRoleMapper jobRoleMapper;

    public JobRoleServiceImpl(JobRoleRepository jobRoleRepository, SearchRepository<JobRole, SearchJobRoleVM> searchRepository,
                              JobRoleMapper jobRoleMapper) {
        this.jobRoleRepository = jobRoleRepository;
        this.searchRepository = searchRepository;
        this.jobRoleMapper = jobRoleMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto save(CreateJobRoleVM request) {
        var jobRole = new JobRole();
        jobRole.setName(request.getName());
        jobRole.setIsDeleted(Constant.STR_N);
        jobRoleRepository.save(jobRole);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto update(UpdateJobRoleVM request) throws BusinessException {
        var jobRoleOptional = jobRoleRepository.findById(request.getId());

        if (jobRoleOptional.isEmpty()) {
            throw new BusinessException("Không tìm thấy job role");
        }
        var jobRole = jobRoleOptional.get();
        jobRole.setName(request.getName());
        jobRoleRepository.save(jobRole);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto delete(DeleteJobRoleVM request) throws BusinessException {
        var jobRoles = jobRoleRepository.findAllById(request.getListId());
        if (jobRoles.size() > 0) {
            for (JobRole jobRoleItem : jobRoles)
                jobRoleItem.setIsDeleted(Constant.STR_Y);
            jobRoleRepository.saveAll(jobRoles);
        } else {
            throw new BusinessException("Không tìm thấy Job role");
        }
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto<PaginationDto<JobRoleDto>> search(SearchJobRoleVM request) {
        request.setIsDeleted(Constant.STR_N);
        PaginationDto<JobRole> result = searchRepository.searchData(request, JobRole.class);
        return BasicResponseDto.ok(jobRoleMapper.paginationToDto(result));
    }

    @Override
    @Transactional
    public BasicResponseDto<List<JobRoleDto>> findAll() {
        List<JobRoleDto> response = jobRoleMapper.toListDto(jobRoleRepository.findAllByIsDeleted(Constant.STR_N));
        return BasicResponseDto.ok(response);
    }


}
