package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.entity.AccountInfo;
import com.chechen.entity.CompanyProfile;
import com.chechen.entity.HeadhuntBrand;
import com.chechen.entity.HeadhuntProfile;
import com.chechen.exception.BusinessException;
import com.chechen.repository.AccountInfoRepository;
import com.chechen.repository.CompanyProfileRepository;
import com.chechen.repository.HeadhuntBrandRepository;
import com.chechen.repository.HeadhuntProfileRepository;
import com.chechen.services.KeycloakService;
import com.chechen.services.UserInfoService;
import com.chechen.services.vm.userinfo.ChangePasswordVM;
import com.chechen.services.vm.userinfo.UserRegisterVM;
import com.chechen.utils.ErrorCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserInfoServiceImpl implements UserInfoService {
    private final Logger logger = LogManager.getLogger();

    @Value("${keycloak.auth-url}")
    private String keycloakAuthUrl;
    @Value("${keycloak.realm}")
    private String keycloakRealm;
    @Value("${keycloak.username}")
    private String keycloakUser;
    @Value("${keycloak.password}")
    private String keycloakPassword;
    @Value("${keycloak.client-id}")
    private String keycloakClientId;

    private final KeycloakService keycloakService;
    private final AccountInfoRepository accountInfoRepository;
    private final CompanyProfileRepository companyProfileRepository;
    private final HeadhuntProfileRepository headhuntProfileRepository;
    private final HeadhuntBrandRepository headhuntBrandRepository;

    public UserInfoServiceImpl(KeycloakService keycloakService, AccountInfoRepository accountInfoRepository, CompanyProfileRepository companyProfileRepository, HeadhuntProfileRepository headhuntProfileRepository, HeadhuntBrandRepository headhuntBrandRepository) {
        this.keycloakService = keycloakService;
        this.accountInfoRepository = accountInfoRepository;
        this.companyProfileRepository = companyProfileRepository;
        this.headhuntProfileRepository = headhuntProfileRepository;
        this.headhuntBrandRepository = headhuntBrandRepository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto normalRegister(UserRegisterVM request) throws BusinessException {
        List<String> regType = List.of(Constant.USER_INFO.REG_TYPE_COMPANY, Constant.USER_INFO.REG_TYPE_HEADHUNT);
        if (!regType.contains(request.getRegistrationType()))
            throw new BusinessException("registrationType không hợp lệ!");
        // Lưu thông tin ở db chính
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setEmail(request.getEmail().toLowerCase());
        accountInfo.setUserType(request.getRegistrationType());
        accountInfo.setUsername(request.getUsername());
        accountInfo.setPhone(request.getPhone());
        accountInfo.setIsDeleted(Constant.STR_N);

        BasicResponseDto response =  keycloakService.registerNewAccount(keycloakAuthUrl, keycloakRealm, keycloakUser, keycloakPassword, keycloakClientId, request, accountInfo);
        // Nếu như mà reg type là headhunt thì thêm headhunt profile, reg type là company thì thêm company profile
        if (ErrorCode.SUCC_200 == response.getStatus()) {
            if (Constant.USER_INFO.REG_TYPE_COMPANY.equals(request.getRegistrationType())) {
                //Nếu mà đăng ký công ty thì mới yêu cầu cần phải có tên công ty
                if (request.getCompany() == null)
                    throw new BusinessException("Tên công ty không được trống!");
                if (request.getCompany().length() > 500)
                    throw new BusinessException("Tên công ty không dài quá 500 ký tự");
                CompanyProfile companyProfile = new CompanyProfile();
                companyProfile.setCompanyName(request.getCompany());
                companyProfile.setCompanyEmail(request.getEmail().toLowerCase());
                companyProfile.setCompanyPhone(request.getPhone());
                companyProfile.setCompanySize(Constant.COMPANY_SIZE.SIZE_1);
                companyProfile.setEmailRepresentative(request.getEmail().toLowerCase());
                companyProfile.setPhoneRepresentative(request.getPhone());
                companyProfile.setFirstNameRepresentative(request.getFirstName());
                companyProfile.setLastNameRepresentative(request.getLastName());
                companyProfile.setAccountInfo(accountInfo);
                accountInfo.setCompanyProfile(companyProfile);
                companyProfileRepository.save(companyProfile);
            } else {
                HeadhuntProfile headhuntProfile = new HeadhuntProfile();
                headhuntProfile.setFirstName(request.getFirstName());
                headhuntProfile.setLastName(request.getLastName());
                headhuntProfile.setGender(Constant.GENDER.NONE);

                HeadhuntBrand headhuntBrand = new HeadhuntBrand();
                headhuntBrand.setHeadhuntProfile(headhuntProfile);
                headhuntBrand.setName(headhuntProfile.getFirstName() + " " + headhuntProfile.getLastName());
                headhuntBrand.setContactFirstName(headhuntProfile.getFirstName());
                headhuntBrand.setContactLastName(headhuntProfile.getLastName());
                headhuntBrand.setTeamLeadEmail(accountInfo.getEmail());
                headhuntBrand.setContactEmail(accountInfo.getEmail());
                headhuntBrand.setContactPhone(accountInfo.getPhone());
                headhuntBrandRepository.save(headhuntBrand);

                headhuntProfile.setHeadhuntBrand(headhuntBrand);
                headhuntProfile.setAccountInfo(accountInfo);
                accountInfo.setHeadhuntProfile(headhuntProfile);
                headhuntProfileRepository.save(headhuntProfile);
            }
        }
        return response;
    }

    @Override
    public BasicResponseDto changePassword(ChangePasswordVM request) {
        return null;
    }
}
