package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.FileManagementDto;
import com.chechen.entity.FileManagement;
import com.chechen.entity.FileUploadPolicy;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.FileManagementMapper;
import com.chechen.obj.Oauth2UserPrincipal;
import com.chechen.repository.FileManagementRepository;
import com.chechen.repository.FileUploadPolicyRepository;
import com.chechen.services.FileManagementService;
import com.chechen.services.vm.file.UploadFileVM;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Service
public class FileManagementServiceImpl implements FileManagementService {
    private final Logger logger = LogManager.getLogger();

    @Value("${upload.public-path}")
    private String uploadPathPublic;

    @Value("${upload.private-path}")
    private String uploadPathPrivate;

    @Value("${upload.static-serve-url}")
    private String staticServeUrl;

    private final FileUploadPolicyRepository fileUploadPolicyRepository;
    private final FileManagementRepository fileManagementRepository;
    private final FileManagementMapper fileManagementMapper;

    public FileManagementServiceImpl(FileUploadPolicyRepository fileUploadPolicyRepository,
                                     FileManagementRepository fileManagementRepository, FileManagementMapper fileManagementMapper) {
        this.fileUploadPolicyRepository = fileUploadPolicyRepository;
        this.fileManagementRepository = fileManagementRepository;
        this.fileManagementMapper = fileManagementMapper;
    }

    @Override
    public BasicResponseDto<List<FileManagementDto>> uploadMultipleFile(UploadFileVM uploadFileVM, boolean isPublic) throws BusinessException, IOException {
        MultipartFile[] files = uploadFileVM.getFiles();
        for (MultipartFile file : files) {
            validateFile(file);
        }
        List<FileManagementDto> responses = new ArrayList<>();
        for (MultipartFile file : files) {
            FileManagement fileUploaded = uploadSingleFile(file, isPublic);
            if (fileUploaded != null) {
                responses.add(fileManagementMapper.toDto(fileUploaded));
            }
        }
        return BasicResponseDto.ok(responses);
    }

    @Transactional(rollbackFor = Exception.class)
    public FileManagement uploadSingleFile(MultipartFile file, boolean isPublic) {
        try {
            String ext = FilenameUtils.getExtension(file.getOriginalFilename());
            LocalDateTime now = LocalDateTime.now();
            Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String subDir = now.getYear() + "/" + now.getMonthValue() + "/" + now.getDayOfMonth() + "/" + principal.getUserId();
            Path parentDir = Paths.get((isPublic? uploadPathPublic : uploadPathPrivate) + subDir);
            if (Files.notExists(parentDir))
                Files.createDirectories(parentDir);
            String randomFileName = UUID.randomUUID().toString();
            String filePath = FilenameUtils.separatorsToSystem(parentDir + "/" + randomFileName + "." + ext);
            String urlViewPublicFile = staticServeUrl + subDir + "/" + randomFileName + "." + ext;
            Files.copy(file.getInputStream(), Paths.get(filePath), StandardCopyOption.REPLACE_EXISTING);
            FileManagement fileManagement = new FileManagement();
            fileManagement.setFileExt(ext);
            fileManagement.setIsDeleted(Constant.STR_N);
            fileManagement.setContentType(file.getContentType());
            fileManagement.setName(file.getOriginalFilename());
            fileManagement.setPath(filePath);
            fileManagement.setSize(file.getSize());
            fileManagement.setCreatedBy(principal.getUserId());
            fileManagement.setCreatedDate(new Date());
            fileManagement.setIsPublic(isPublic ? Constant.STR_Y : Constant.STR_N);
            fileManagement.setViewLink(urlViewPublicFile);
            fileManagementRepository.save(fileManagement);
            return fileManagement;
        } catch (Exception e) {
            logger.error("Error", e);
            return null;
        }
    }

    @Override
    public void validateFile(MultipartFile file) throws BusinessException {
        if (file == null)
            throw new BusinessException("Tệp không được trống");
        String ext = FilenameUtils.getExtension(file.getOriginalFilename());
        Optional<FileUploadPolicy> fileUploadPolicyOptional = fileUploadPolicyRepository.findByExt(ext);
        if (fileUploadPolicyOptional.isEmpty())
            throw new BusinessException("Tệp "+ file.getOriginalFilename() +" không được upload lên hệ thống");
        FileUploadPolicy fileUploadPolicy = fileUploadPolicyOptional.get();
        if (file.getSize() > fileUploadPolicy.getMaxUploadSize())
            throw new BusinessException("Tệp " + file.getOriginalFilename() + " vượt quá dung lượng upload, tối đa " + FileUtils.byteCountToDisplaySize(file.getSize()));
    }

    @Override
    @Transactional
    public void viewFileUpload(String fileId, HttpServletResponse httpServletResponse) throws IOException {
        Optional<FileManagement> optionalFileManagement = fileManagementRepository.findById(fileId);
        if (optionalFileManagement.isEmpty()) {
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return;
        } else {
            FileManagement fileManagement = optionalFileManagement.get();
            if (Constant.STR_N.equals(fileManagement.getIsPublic())) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication == null || authentication.getPrincipal() == null || Constant.ANONYMOUS_USER.equalsIgnoreCase(authentication.getName())) {
                    httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
                    return;
                } else {
                    Oauth2UserPrincipal principal = (Oauth2UserPrincipal) authentication.getPrincipal();
                    if (principal.getAuthorities().stream().noneMatch(item -> Constant.AUTHORITY.ADMIN.equals(item.getAuthority()))
                            && !principal.getUserId().equals(fileManagement.getCreatedBy())) {
                        httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
                        return;
                    }
                }
            }
            String filePath = fileManagement.getPath();
            Path path = Paths.get(filePath);
            if (Files.notExists(path)) {
                httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
                return;
            }
            httpServletResponse.setHeader("Content-disposition", "attachment; filename=" + fileManagement.getName());
            httpServletResponse.setContentType(fileManagement.getContentType());
            try (InputStream bis = Files.newInputStream(Paths.get(filePath));
                 OutputStream bos = httpServletResponse.getOutputStream()) {
                byte[] bytes = new byte[1024];
                int byteRead;
                while ((byteRead = (bis.read(bytes))) != -1)
                    bos.write(bytes, 0, byteRead);
            }
        }
    }

    @Override
    @Transactional
    public BasicResponseDto<List<FileManagementDto>> uploadFileLogo(UploadFileVM uploadFileVM) throws BusinessException, IOException {
        MultipartFile[] files = uploadFileVM.getFiles();
        for (MultipartFile file : files) {
            if (file == null)
                throw new BusinessException("Tệp không được trống");
            String ext = FilenameUtils.getExtension(file.getOriginalFilename());
            List<String> extsApprove = List.of("jpg", "png");
            if (!extsApprove.contains(ext))
                throw new BusinessException("Chỉ hỗ trợ tệp jpg và png");
        }
        return uploadMultipleFile(uploadFileVM, true);
    }
}
