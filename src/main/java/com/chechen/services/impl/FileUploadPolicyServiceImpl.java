package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.FileUploadPolicyDto;
import com.chechen.entity.FileUploadPolicy;
import com.chechen.entity.Level;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.FileUploadPolicyMapper;
import com.chechen.repository.FileUploadPolicyRepository;
import com.chechen.services.FileUploadPolicyService;
import com.chechen.services.vm.file.CreateFileUploadPolicyVM;
import com.chechen.services.vm.file.DeleteFileUploadPolicyVM;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Service
public class FileUploadPolicyServiceImpl implements FileUploadPolicyService {
    private final Logger logger = LogManager.getLogger();

    private final FileUploadPolicyRepository fileUploadPolicyRepository;
    private final FileUploadPolicyMapper fileUploadPolicyMapper;

    public FileUploadPolicyServiceImpl(FileUploadPolicyRepository fileUploadPolicyRepository, FileUploadPolicyMapper fileUploadPolicyMapper) {
        this.fileUploadPolicyRepository = fileUploadPolicyRepository;
        this.fileUploadPolicyMapper = fileUploadPolicyMapper;
    }

    @Override
    @Transactional
    public BasicResponseDto create(CreateFileUploadPolicyVM request) throws BusinessException {
        Optional<FileUploadPolicy> fileUploadPolicyOptional = fileUploadPolicyRepository.findByExt(request.getExt());
        FileUploadPolicy fileUploadPolicy = null;
        if (fileUploadPolicyOptional.isPresent()) {
            fileUploadPolicy = fileUploadPolicyOptional.get();
        } else
            fileUploadPolicy = new FileUploadPolicy();

        fileUploadPolicy.setExt(request.getExt());
        fileUploadPolicy.setMaxUploadSize(request.getMaxFileSize());
        logger.info("File upload policy create/update {}", fileUploadPolicy);
        fileUploadPolicyRepository.save(fileUploadPolicy);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto<List<FileUploadPolicyDto>> findAll() {
        List<FileUploadPolicy> fileUploadPolicyList = fileUploadPolicyRepository.findAll();
        return BasicResponseDto.ok(fileUploadPolicyMapper.toLstDto(fileUploadPolicyList));
    }

    @Override
    @Transactional
    public BasicResponseDto delete(DeleteFileUploadPolicyVM request) {
        var fileUploadPolicyList = fileUploadPolicyRepository.findAllById(request.getListId());
       fileUploadPolicyRepository.deleteAll(fileUploadPolicyList);
        return BasicResponseDto.ok();
    }
}
