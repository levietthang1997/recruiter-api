package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.*;
import com.chechen.entity.*;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.CompanyProfileMapper;
import com.chechen.mapper.JobInfoMapper;
import com.chechen.obj.Oauth2UserPrincipal;
import com.chechen.repository.*;
import com.chechen.services.CompanyService;
import com.chechen.services.vm.company.UpdateCompanyBenefitDetailVM;
import com.chechen.services.vm.company.UpdateCompanyProfileVM;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
@Service
public class CompanyServiceImpl implements CompanyService {
    private final Logger logger = LogManager.getLogger();

    private final CompanyProfileRepository companyProfileRepository;
    private final AccountInfoRepository accountInfoRepository;
    private final IndustryRepository industryRepository;
    private final CompanyProfileMapper companyProfileMapper;
    private final CompanyBenefitRepository companyBenefitRepository;
    private final CompanyBenefitDetailRepository companyBenefitDetailRepository;
    private final FileManagementRepository fileManagementRepository;
    private final JobInfoRepository jobInfoRepository;
    private final JobInfoMapper jobInfoMapper;

    public CompanyServiceImpl(CompanyProfileRepository companyProfileRepository, AccountInfoRepository accountInfoRepository,
                              IndustryRepository industryRepository, CompanyProfileMapper companyProfileMapper,
                              CompanyBenefitRepository companyBenefitRepository, CompanyBenefitDetailRepository companyBenefitDetailRepository,
                              FileManagementRepository fileManagementRepository, JobInfoRepository jobInfoRepository,
                              JobInfoMapper jobInfoMapper) {
        this.companyProfileRepository = companyProfileRepository;
        this.accountInfoRepository = accountInfoRepository;
        this.industryRepository = industryRepository;
        this.companyProfileMapper = companyProfileMapper;
        this.companyBenefitRepository = companyBenefitRepository;
        this.companyBenefitDetailRepository = companyBenefitDetailRepository;
        this.fileManagementRepository = fileManagementRepository;
        this.jobInfoRepository = jobInfoRepository;
        this.jobInfoMapper = jobInfoMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto<CompanyProfileDto> updateProfile(UpdateCompanyProfileVM request) throws BusinessException {
        logger.info("====> Update company info, request={}", request);
        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userId = principal.getUserId();
        Optional<AccountInfo> accountInfoOptional = accountInfoRepository.findByKeycloakUserId(userId, Constant.STR_N);
        if (accountInfoOptional.isEmpty())
            throw new BusinessException("Không tìm thấy tài khoản");
        AccountInfo accountInfo = accountInfoOptional.get();
        CompanyProfile companyProfile = accountInfo.getCompanyProfile();
        companyProfile.setCompanyName(request.getCompanyName());
        companyProfile.setCompanyAddress(request.getCompanyAddress());
        companyProfile.setCompanySize(request.getCompanySize());
        companyProfile.setCompanyEmail(request.getCompanyEmail());
        companyProfile.setCcMail(request.getCcMail());
        // lấy danh sách ngành nghề
        if (request.getCompanyIndustries() != null) {
            if (request.getCompanyIndustries().size() > 0) {
                Set<Industry> industryList = industryRepository.findAllByIdInAndIsDeleted(request.getCompanyIndustries(), Constant.STR_N);
                if (request.getCompanyIndustries().size() > 0 && industryList.isEmpty())
                    throw new BusinessException("Không tìm thấy ngành nghề công ty");
                Map<String, Industry> cpIndustryMapVM = new HashMap<>();
                for (Industry item : industryList) {
                    cpIndustryMapVM.put(item.getId(), item);
                }

                Map<String, Industry> companyProfileIndustryMap = new HashMap<>();
                List<Industry> lstRemove = new ArrayList<>();
                for (Industry item : companyProfile.getCompanyIndustries()) {
                    companyProfileIndustryMap.put(item.getId(), item);
                    if (cpIndustryMapVM.get(item.getId()) == null)
                        lstRemove.add(item);
                }
                for (Industry item : industryList) {
                    if (companyProfileIndustryMap.get(item.getId()) == null)
                        companyProfile.getCompanyIndustries().add(item);
                }
                for (Industry item : lstRemove)
                    companyProfile.getCompanyIndustries().remove(item);
            } else {
                companyProfile.getCompanyIndustries().clear();
            }
        }
        // lưu thông tin giờ làm việc và website
        companyProfile.setWorkingHours(request.getWorkingHours());
        companyProfile.setWebsite(request.getWebsite());
        // lấy danh sách lợi ích
        if (request.getBenefitDetails() != null) {
            if (request.getBenefitDetails().size() > 0) {
                Set<CpBenefit> benefits = companyBenefitRepository.findAllByIdInAndIsDeleted(request.getBenefitDetails().stream().map(UpdateCompanyBenefitDetailVM::getBenefitId).collect(Collectors.toList()), Constant.STR_N);
                if (request.getBenefitDetails().size() > 0 && benefits.isEmpty())
                    throw new BusinessException("Không tìm thấy benefit");
                Map<String, CpBenefit> benefitMapVM = new HashMap<>();
                for (CpBenefit item : benefits) {
                    benefitMapVM.put(item.getId(), item);
                }
                Map<String, UpdateCompanyBenefitDetailVM> benefitDetailVMMap = new HashMap<>();
                for (UpdateCompanyBenefitDetailVM item : request.getBenefitDetails()) {
                    benefitDetailVMMap.put(item.getBenefitId(), item);
                }
                Map<String, CompanyBenefitDetail> benefitCompanyProfileMap = new HashMap<>();
                for (CompanyBenefitDetail companyBenefitDetailItem : companyProfile.getBenefitDetails()) {
                    benefitCompanyProfileMap.put(companyBenefitDetailItem.getBenefit().getId(), companyBenefitDetailItem);
                }
                List<CompanyBenefitDetail> lstBenefitRemove = new ArrayList<>();
                if (benefits.size() > 0) {
                    for (CompanyBenefitDetail benefitDetail : companyProfile.getBenefitDetails()) {
                        CpBenefit benefitFoundVM = benefitMapVM.get(benefitDetail.getBenefit().getId());
                        if (benefitFoundVM != null) {
                            benefitDetail.setDetail(benefitDetailVMMap.get(benefitFoundVM.getId()).getDetail());
                        } else {
                            lstBenefitRemove.add(benefitDetail);
                        }
                    }
                    for (CpBenefit benefit : benefits) {
                        CompanyBenefitDetail detailFound = benefitCompanyProfileMap.get(benefit.getId());
                        if (detailFound == null) {
                            CompanyBenefitDetail companyBenefitDetail = new CompanyBenefitDetail();
                            companyBenefitDetail.setBenefit(benefit);
                            companyBenefitDetail.setCompanyProfile(companyProfile);
                            companyBenefitDetail.setDetail(benefitDetailVMMap.get(benefit.getId()).getDetail());
                            companyProfile.getBenefitDetails().add(companyBenefitDetail);
                        }
                    }
                    for (CompanyBenefitDetail companyBenefitDetail : lstBenefitRemove) {
                        companyProfile.getBenefitDetails().remove(companyBenefitDetail);
                    }
                } else
                    throw new BusinessException("Không tìm thấy danh sách lợi ích công ty");
            } else {
                companyProfile.getBenefitDetails().clear();
            }
        }


        // Lưu lại benefit detail
        if (companyProfile.getBenefitDetails().size() > 0)
            companyBenefitDetailRepository.saveAll(companyProfile.getBenefitDetails());

        // logo
        if (Strings.isNotEmpty(request.getFileLogoId())) {
            Optional<FileManagement> logoOptional = fileManagementRepository.findByIdAndCreatedByAndIsDeleted(request.getFileLogoId(), principal.getUserId(), Constant.STR_N);
            if (logoOptional.isPresent()) {
                companyProfile.setFileLogoId(request.getFileLogoId());
                companyProfile.setViewLinkLogo(logoOptional.get().getViewLink());
            } else
                throw new BusinessException("File logo không tồn tại");
        } else {
            companyProfile.setFileLogoId(null);
            companyProfile.setViewLinkLogo(null);
        }
        companyProfile.setDescription(request.getDescription());
        // gallery
        if (request.getGallery() != null) {
            if (request.getGallery().size() > 0) {
                Set<FileManagement> galleryList = fileManagementRepository.findAllByIdInAndIsDeleted(request.getGallery(), Constant.STR_N);
                Map<String, FileManagement> galleryMapVM = new HashMap<>();
                for (FileManagement item : galleryList) {
                    galleryMapVM.put(item.getId(), item);
                }

                Map<String, FileManagement> companyProfileGalleryMap = new HashMap<>();
                List<FileManagement> lstRemove = new ArrayList<>();
                for (FileManagement item : companyProfile.getGallery()) {
                    companyProfileGalleryMap.put(item.getId(), item);
                    if (galleryMapVM.get(item.getId()) == null)
                        lstRemove.add(item);
                }
                for (FileManagement item : galleryList) {
                    if (companyProfileGalleryMap.get(item.getId()) == null)
                        companyProfile.getGallery().add(item);
                }
                for (FileManagement item : lstRemove)
                    companyProfile.getGallery().remove(item);
            } else {
                companyProfile.getGallery().clear();
            }
        }

        // thumbnail
        if (Strings.isNotEmpty(request.getThumbnailLogoId())) {
            Optional<FileManagement> fileThumbNailOptional = fileManagementRepository.findByIdAndCreatedByAndIsDeleted(request.getThumbnailLogoId(), principal.getUserId(), Constant.STR_N);
            if (fileThumbNailOptional.isPresent()) {
                companyProfile.setThumbnailLogoId(request.getThumbnailLogoId());
                companyProfile.setViewLinkThumbnail(fileThumbNailOptional.get().getViewLink());
            } else
                throw new BusinessException("File thumnail logo không tồn tại");
        }
        companyProfile.setThumbnailDescription(request.getThumbnailDescription());
        companyProfile.setLinkedIn(request.getLinkedIn());
        companyProfile.setFacebook(request.getFacebook());
        // thông tin người đại diện
        companyProfile.setFirstNameRepresentative(request.getFirstNameRepresentative());
        companyProfile.setLastNameRepresentative(request.getLastNameRepresentative());
        companyProfile.setHeadline(request.getHeadline());
        companyProfile.setPhoneRepresentative(request.getPhoneRepresentative());
        companyProfile.setEmailRepresentative(request.getEmailRepresentative());
        //save
        companyProfileRepository.save(companyProfile);
        return BasicResponseDto.ok(companyProfileMapper.toDto(companyProfile));
    }

    @Override
    @Transactional
    public BasicResponseDto<CompanyProfileDto> getProfileCompany() throws BusinessException {
        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<CompanyProfile> companyProfileOptional = companyProfileRepository.findByAccountInfoKeycloakUserId(principal.getUserId());
        if (companyProfileOptional.isEmpty())
            throw new BusinessException("Không tìm thấy thông tin công ty của tài khoản, vui lòng thử lại sau");
        CompanyProfile companyProfile = companyProfileOptional.get();
        return BasicResponseDto.ok(companyProfileMapper.toDto(companyProfile));
    }

    @Override
    @Transactional
    public BasicResponseDto<List<CompanyProfileDtoV2>> getAllCompany() {
        List<CompanyProfile> companyProfiles = companyProfileRepository.findAllCustom();
        return BasicResponseDto.ok(companyProfileMapper.toLstDtoV2(companyProfiles));
    }

    @Override
    @Transactional
    public BasicResponseDto<CompanyPublicProfileDto> getPublicProfile(String companyId) throws BusinessException {
        Optional<CompanyProfile> companyProfileOpt = companyProfileRepository.findById(companyId);
        if (companyProfileOpt.isEmpty())
            throw new BusinessException("Company not found!");
        CompanyProfile companyProfile = companyProfileOpt.get();
        CompanyPublicProfileDto publicProfileDto = companyProfileMapper.toPublicProfileDto(companyProfile);
        Pageable pageable = PageRequest.of(0, 4);
        List<JobInfo> jobInfoList = jobInfoRepository.findAllByIsDeletedAndApproveStatusAndCompanyProfileIdOrderByPriorityJobDesc(pageable, Constant.STR_N, Constant.PENDING_TASK.APPROVED.getCode(), companyId);
        List<JobInfoDtoV2> companyJob = jobInfoMapper.toDtoV2List(jobInfoList);
        publicProfileDto.setPopularJob(companyJob);
        publicProfileDto.setVacancies(jobInfoRepository.sumAllVacanciesByCompanyProfileIdAndIsDeleted(companyId, Constant.STR_N));
        return BasicResponseDto.ok(publicProfileDto);
    }
}
