package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.PaginationDto;
import com.chechen.dto.SkillDto;
import com.chechen.entity.Skill;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.SkillMapper;
import com.chechen.repository.SearchRepository;
import com.chechen.repository.SkillRepository;
import com.chechen.services.SkillService;
import com.chechen.services.vm.skill.CreateSkillVM;
import com.chechen.services.vm.skill.DeleteSkillVM;
import com.chechen.services.vm.skill.SearchSkillVM;
import com.chechen.services.vm.skill.UpdateSkillVM;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SkillServiceImpl implements SkillService {

    private final SkillRepository skillRepository;
    private final SkillMapper skillMapper;
    private final SearchRepository<Skill, SearchSkillVM> searchRepository;

    @Override
    @Transactional
    public BasicResponseDto save(CreateSkillVM request) {
        Skill skill = new Skill();
        skill.setName(request.getName());
        skill.setIsDeleted(Constant.STR_N);
        skillRepository.save(skill);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto update(UpdateSkillVM request) throws  BusinessException {
        var skillOptional = skillRepository.findById(request.getId());

        if (skillOptional.isEmpty()) {
            throw new BusinessException("Không tìm thấy kỹ năng");
        }
        var skill = skillOptional.get();
        skill.setName(request.getName());
        skillRepository.save(skill);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto delete(DeleteSkillVM request) throws  BusinessException {
        var skills = skillRepository.findAllById(request.getListId());
        if (skills.size() > 0) {
            for (Skill skillItem : skills)
                skillItem.setIsDeleted(Constant.STR_Y);
            skillRepository.saveAll(skills);
        } else {
            throw new BusinessException("Không tìm thấy kỹ năng");
        }
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto<PaginationDto<SkillDto>> search(SearchSkillVM searchSkillVM) {
        searchSkillVM.setIsDeleted(Constant.STR_N);
        PaginationDto<Skill> result = searchRepository.searchData(searchSkillVM, Skill.class);
        return BasicResponseDto.ok(skillMapper.paginationToDto(result));
    }

    @Override
    @Transactional
    public BasicResponseDto<List<SkillDto>> findAll() {
        List<SkillDto> response = skillMapper.toListDto(skillRepository.findAllByIsDeleted(Constant.STR_N));
        return BasicResponseDto.ok(response);
    }
}
