package com.chechen.services.impl;

import com.chechen.repository.CvInformationRepository;
import com.chechen.services.CvInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class CvInformationServiceImpl implements CvInformationService {
    private final Logger logger = LogManager.getLogger();
    private final CvInformationRepository cvInformationRepository;

    public CvInformationServiceImpl(CvInformationRepository cvInformationRepository) {
        this.cvInformationRepository = cvInformationRepository;
    }
}
