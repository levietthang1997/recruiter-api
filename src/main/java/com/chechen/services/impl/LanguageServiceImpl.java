package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.LanguageDto;
import com.chechen.dto.LevelDto;
import com.chechen.dto.PaginationDto;
import com.chechen.entity.Language;
import com.chechen.entity.Level;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.LanguageMapper;
import com.chechen.repository.LanguageRepository;
import com.chechen.repository.SearchRepository;
import com.chechen.services.LanguageService;
import com.chechen.services.vm.language.CreateLanguageVM;
import com.chechen.services.vm.language.DeleteLanguageVM;
import com.chechen.services.vm.language.SearchLanguageVM;
import com.chechen.services.vm.language.UpdateLanguageVM;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LanguageServiceImpl implements LanguageService {

    private final LanguageRepository languageRepository;
    private final SearchRepository<Language, SearchLanguageVM> searchRepository;
    private final LanguageMapper languageMapper;

    public LanguageServiceImpl(LanguageRepository languageRepository, SearchRepository<Language, SearchLanguageVM> searchRepository,
                               LanguageMapper languageMapper) {
        this.languageRepository = languageRepository;
        this.searchRepository = searchRepository;
        this.languageMapper = languageMapper;
    }

    @Override
    @Transactional
    public BasicResponseDto save(CreateLanguageVM request) {
        Language language = new Language();
        language.setName(request.getName());
        language.setIsDeleted(Constant.STR_N);
        languageRepository.save(language);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto update(UpdateLanguageVM request) throws BusinessException {
        var languageOptional = languageRepository.findById(request.getId());

        if (languageOptional.isEmpty()) {
            throw new BusinessException("Không tìm thấy language");
        }
        var language = languageOptional.get();
        language.setName(request.getName());
        languageRepository.save(language);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto<PaginationDto<LanguageDto>> search(SearchLanguageVM request) {
        request.setIsDeleted(Constant.STR_N);
        PaginationDto<Language> result = searchRepository.searchData(request, Language.class);
        return BasicResponseDto.ok(languageMapper.paginationToDto(result));
    }

    @Override
    @Transactional
    public BasicResponseDto delete(DeleteLanguageVM request) throws BusinessException {
        var languageList = languageRepository.findAllById(request.getListId());
        if (languageList.size() > 0) {
            for (Language languageItem : languageList)
                languageItem.setIsDeleted(Constant.STR_Y);
            languageRepository.saveAll(languageList);
        } else {
            throw new BusinessException("Không tìm thấy language");
        }
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto<List<LanguageDto>> findAll() {
        List<LanguageDto> response = languageMapper.toListDto(languageRepository.findAllByIsDeleted(Constant.STR_N));
        return BasicResponseDto.ok(response);
    }
}
