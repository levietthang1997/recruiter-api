package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.JobCategoryDto;
import com.chechen.dto.JobRoleDto;
import com.chechen.dto.PaginationDto;
import com.chechen.entity.JobCategory;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.JobCategoryMapper;
import com.chechen.repository.JobCategoryRepository;
import com.chechen.repository.SearchRepository;
import com.chechen.services.JobCategoryService;
import com.chechen.services.vm.jobcategory.CreateJobCategoryVM;
import com.chechen.services.vm.jobcategory.DeleteJobCategoryVM;
import com.chechen.services.vm.jobcategory.SearchJobCategoryVM;
import com.chechen.services.vm.jobcategory.UpdateJobCategoryVM;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class JobCategoryServiceImpl implements JobCategoryService {

    private final JobCategoryRepository jobCategoryRepository;
    private final SearchRepository<JobCategory, SearchJobCategoryVM> searchRepository;
    private final JobCategoryMapper jobCategoryMapper;

    public JobCategoryServiceImpl(JobCategoryRepository jobCategoryRepository, SearchRepository<JobCategory, SearchJobCategoryVM> searchRepository, JobCategoryMapper jobCategoryMapper) {
        this.jobCategoryRepository = jobCategoryRepository;
        this.searchRepository = searchRepository;
        this.jobCategoryMapper = jobCategoryMapper;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public BasicResponseDto save(CreateJobCategoryVM request) {
        var jobCategory = new JobCategory();
        jobCategory.setName(request.getName());
        jobCategory.setIsDeleted(Constant.STR_N);
        jobCategoryRepository.save(jobCategory);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public BasicResponseDto update(UpdateJobCategoryVM request) throws BusinessException {
        var jobCategoryOptional = jobCategoryRepository.findById(request.getId());

        if (jobCategoryOptional.isEmpty()) {
            throw new BusinessException("Không tìm thấy job category");
        }
        var jobCategory = jobCategoryOptional.get();
        jobCategory.setName(request.getName());
        jobCategoryRepository.save(jobCategory);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public BasicResponseDto delete(DeleteJobCategoryVM request) throws BusinessException {
        var jobCategories = jobCategoryRepository.findAllById(request.getListId());
        if (jobCategories.size() > 0) {
            for (JobCategory jobCategoryItem : jobCategories)
                jobCategoryItem.setIsDeleted(Constant.STR_Y);
            jobCategoryRepository.saveAll(jobCategories);
        } else {
            throw new BusinessException("Không tìm thấy Job category");
        }
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto<PaginationDto<JobCategoryDto>> search(SearchJobCategoryVM request) {
        request.setIsDeleted(Constant.STR_N);
        PaginationDto<JobCategory> result = searchRepository.searchData(request, JobCategory.class);
        return BasicResponseDto.ok(jobCategoryMapper.paginationToDto(result));
    }

    @Override
    @Transactional
    public BasicResponseDto<List<JobCategoryDto>> findAll() {
        List<JobCategoryDto> response = jobCategoryMapper.toListDto(jobCategoryRepository.findAllByIsDeleted(Constant.STR_N));
        return BasicResponseDto.ok(response);
    }
}
