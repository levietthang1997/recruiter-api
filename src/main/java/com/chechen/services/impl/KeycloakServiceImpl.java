package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.entity.AccountInfo;
import com.chechen.exception.BusinessException;
import com.chechen.repository.AccountInfoRepository;
import com.chechen.repository.CompanyProfileRepository;
import com.chechen.repository.HeadhuntProfileRepository;
import com.chechen.services.KeycloakService;
import com.chechen.services.vm.userinfo.UserRegisterVM;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author thanglv on 05/06/2022
 * @project recruiter-api
 */
@Service
public class KeycloakServiceImpl implements KeycloakService {
    private final Logger logger = LogManager.getLogger();

    private final AccountInfoRepository accountInfoRepository;
    private final CompanyProfileRepository companyProfileRepository;
    private final HeadhuntProfileRepository headhuntProfileRepository;

    public KeycloakServiceImpl(AccountInfoRepository accountInfoRepository,  CompanyProfileRepository companyProfileRepository, HeadhuntProfileRepository headhuntProfileRepository) {
        this.accountInfoRepository = accountInfoRepository;
        this.companyProfileRepository = companyProfileRepository;
        this.headhuntProfileRepository = headhuntProfileRepository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto registerNewAccount(String authUrl, String realm, String username, String password, String clientId, UserRegisterVM request, AccountInfo accountInfo) throws BusinessException {
        var keycloak = Keycloak.getInstance(authUrl, realm, username, password, clientId);
        // user resource
        var usersResource = keycloak.realm(realm).users();
        List<UserRepresentation> userFound = usersResource.search(request.getUsername().toLowerCase());
        if (userFound != null && userFound.size() > 0)
            throw new BusinessException("Username đã tồn tại!");

        userFound = usersResource.search(null, null, null, request.getEmail().toLowerCase(), 0, 10);
        if (userFound != null && userFound.size() > 0)
            throw new BusinessException("Email đã tồn tại!");

        // create new user
        var userReg = new UserRepresentation();
        userReg.setFirstName(request.getFirstName());
        userReg.setLastName(request.getLastName());
        userReg.setEmail(request.getEmail().toLowerCase());
        userReg.setUsername(request.getUsername().toLowerCase());
        userReg.setEnabled(true);

        // set role
        if (Constant.REGISTRATION_TYPE.COMPANY.equals(request.getRegistrationType())) {
            userReg.singleAttribute(Constant.USER_INFO.ATTRIBUTE.COMPANY, request.getCompany());
        }
        userReg.singleAttribute(Constant.USER_INFO.ATTRIBUTE.PHONE, request.getPhone());
        try (Response response = usersResource.create(userReg)) {
            String userId = CreatedResponseUtil.getCreatedId(response);
            // Lưu thông tin ở db chính
            try {
                accountInfo.setKeycloakUserId(userId);
                accountInfoRepository.save(accountInfo);
            } catch (Exception e) {
                logger.error("Error", e);
                logger.info("Deleting account keycloak....");
                usersResource.delete(userId).close();
                throw new BusinessException("Create account info failed");
            }

            // set password
            var credentialPassword = new CredentialRepresentation();
            credentialPassword.setType(CredentialRepresentation.PASSWORD);
            credentialPassword.setValue(request.getPassword());
            credentialPassword.setTemporary(false);

            var userResourceCreated = usersResource.get(userId);
            userResourceCreated.resetPassword(credentialPassword);
            // set role
            var rolesResource = keycloak.realm(realm).roles();
            List<RoleRepresentation> userCreatedRole = new ArrayList<>();
            var userRealmRole = rolesResource.get(Constant.ROLE.USER).toRepresentation();
            userCreatedRole.add(userRealmRole);

            if (Constant.REGISTRATION_TYPE.COMPANY.equals(request.getRegistrationType())) {
                var companyRealmRole = rolesResource.get(Constant.ROLE.COMPANY).toRepresentation();
                userCreatedRole.add(companyRealmRole);
            } else {
                var headhuntRealmRole = rolesResource.get(Constant.ROLE.HEADHUNT).toRepresentation();
                userCreatedRole.add(headhuntRealmRole);
            }
            userResourceCreated.roles().realmLevel().add(userCreatedRole);
            return BasicResponseDto.ok();
        } catch (Exception e) {
            logger.error("Error", e);
            return BasicResponseDto.failed();
        }
    }
}
