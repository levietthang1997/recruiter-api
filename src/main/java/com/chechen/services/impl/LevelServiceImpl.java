package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.LevelDto;
import com.chechen.dto.PaginationDto;
import com.chechen.entity.Level;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.LevelMapper;
import com.chechen.repository.LevelRepository;
import com.chechen.repository.SearchRepository;
import com.chechen.services.LevelService;
import com.chechen.services.vm.level.CreateLevelVM;
import com.chechen.services.vm.level.DeleteLevelVM;
import com.chechen.services.vm.level.SearchLevelVM;
import com.chechen.services.vm.level.UpdateLevelVM;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Service
@RequiredArgsConstructor
public class LevelServiceImpl implements LevelService {

    private final LevelRepository levelRepository;
    private final LevelMapper levelMapper;
    private final SearchRepository<Level, SearchLevelVM> searchRepository;

    @Override
    @Transactional
    public BasicResponseDto save(CreateLevelVM request) {
        Level level = new Level();
        level.setName(request.getName());
        level.setIsDeleted(Constant.STR_N);
        levelRepository.save(level);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto update(UpdateLevelVM request) throws BusinessException {
        var levelOptional = levelRepository.findById(request.getId());

        if (levelOptional.isEmpty()) {
            throw new BusinessException("Không tìm thấy level");
        }
        var level = levelOptional.get();
        level.setName(request.getName());
        levelRepository.save(level);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto delete(DeleteLevelVM request) throws BusinessException {
        var levels = levelRepository.findAllById(request.getListId());
        if (levels.size() > 0) {
            for (Level levelItem : levels)
                levelItem.setIsDeleted(Constant.STR_Y);
            levelRepository.saveAll(levels);
        } else {
            throw new BusinessException("Không tìm thấy level");
        }
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto<PaginationDto<LevelDto>> search(SearchLevelVM searchLevelVM) {
        searchLevelVM.setIsDeleted(Constant.STR_N);
        PaginationDto<Level> result = searchRepository.searchData(searchLevelVM, Level.class);
        return BasicResponseDto.ok(levelMapper.paginationToDto(result));
    }

    @Override
    @Transactional
    public BasicResponseDto<List<LevelDto>> findAll() {
        List<LevelDto> response = levelMapper.toListDto(levelRepository.findAllByIsDeleted(Constant.STR_N));
        return BasicResponseDto.ok(response);
    }
}
