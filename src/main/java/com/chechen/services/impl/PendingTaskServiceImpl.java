package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.PaginationDto;
import com.chechen.dto.PendingTaskDto;
import com.chechen.entity.JobInfo;
import com.chechen.entity.PendingTask;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.PendingTaskMapper;
import com.chechen.obj.Oauth2UserPrincipal;
import com.chechen.repository.JobInfoRepository;
import com.chechen.repository.PendingTaskRepository;
import com.chechen.services.PendingTaskService;
import com.chechen.services.vm.pendingtask.SearchPendingTaskJobVM;
import com.chechen.services.vm.pendingtask.UpdateStatusPendingJobVM;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * @author thanglv on 30/06/2022
 * @project recruiter-api
 */
@Service
public class PendingTaskServiceImpl implements PendingTaskService {

    private final PendingTaskRepository pendingTaskRepository;
    private final PendingTaskMapper pendingTaskMapper;
    private final JobInfoRepository jobInfoRepository;

    public PendingTaskServiceImpl(PendingTaskRepository pendingTaskRepository, PendingTaskMapper pendingTaskMapper, JobInfoRepository jobInfoRepository) {
        this.pendingTaskRepository = pendingTaskRepository;
        this.pendingTaskMapper = pendingTaskMapper;
        this.jobInfoRepository = jobInfoRepository;
    }

    @Override
    public BasicResponseDto<PaginationDto<PendingTaskDto>> getPendingTaskJob(SearchPendingTaskJobVM request) {
        Pageable pageable = PageRequest.of(request.getPage(), request.getSize());

        if (request.getCompanyId() == null) {
            Page<PendingTask> pendingTaskPage;
            PaginationDto<PendingTaskDto> paginationDto = new PaginationDto<>();
            if (Strings.isEmpty(request.getWfStatusCd())) {
                pendingTaskPage = pendingTaskRepository.findAllByIsDelete(pageable, Constant.STR_N);
            } else {
                pendingTaskPage = pendingTaskRepository.findAllByIsDeleteAndWfStatusCode(pageable, Constant.STR_N, request.getWfStatusCd());
            }

            paginationDto.setPage(request.getPage());
            paginationDto.setSize(request.getSize());
            paginationDto.setTotal(pendingTaskPage.getTotalElements());
            paginationDto.setData(pendingTaskMapper.toListDto(pendingTaskPage.getContent()));
            return BasicResponseDto.ok(paginationDto);
        } else {
            List<PendingTask> pendingTaskList;
            PaginationDto<PendingTaskDto> paginationDto = new PaginationDto<>();
            if (Strings.isEmpty(request.getWfStatusCd())) {
                pendingTaskList = pendingTaskRepository.findAllByIsDeleteAndCompanyId(Constant.STR_N, request.getCompanyId(), request.getSize(), request.getPage()* request.getSize());
                paginationDto.setTotal(pendingTaskRepository.countByIsDeleteAndWfStatusCodeAndCompanyId(Constant.STR_N, Constant.PENDING_TASK.PENDING_APPROVE.getCode(), request.getCompanyId()));
            } else {
                pendingTaskList = pendingTaskRepository.findAllByIsDeleteAndWfStatusCodeAndCompanyId(Constant.STR_N, request.getWfStatusCd(), request.getCompanyId(), request.getSize(), request.getPage()* request.getSize());
                paginationDto.setTotal(pendingTaskRepository.countByIsDeleteAndCompanyId(Constant.STR_N, request.getCompanyId()));
            }

            paginationDto.setPage(request.getPage());
            paginationDto.setSize(request.getSize());
            paginationDto.setData(pendingTaskMapper.toListDto(pendingTaskList));
            return BasicResponseDto.ok(paginationDto);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto updateStatusPendingJob(UpdateStatusPendingJobVM request) throws BusinessException {
        if (!Constant.PENDING_TASK.APPROVED.getCode().equals(request.getWfStatusCd())
                && !Constant.PENDING_TASK.REJECTED.getCode().equals(request.getWfStatusCd())
                && !Constant.PENDING_TASK.CLOSED.getCode().equals(request.getWfStatusCd())) {
            throw new BusinessException("Job chỉ nhận 3 trạng thái Phê duyệt, Từ chối và Đóng");
        }
        if (request.getReward().compareTo(new BigDecimal("0")) < 0)
            throw new BusinessException("reward phải lớn hơn 0");

        Optional<PendingTask> pendingTaskOptional = pendingTaskRepository.findById(request.getPendingTaskId());
        if (pendingTaskOptional.isEmpty())
            throw new BusinessException("Không tìm thấy công việc cần duyệt, vui lòng thử lại");
        PendingTask pendingTask = pendingTaskOptional.get();
        Optional<JobInfo> jobInfoOptional = jobInfoRepository.findByIdAndIsDeleted(pendingTask.getString1(), Constant.STR_N);
        if (jobInfoOptional.isEmpty())
            throw new BusinessException("Không tìm thấy công việc cần duyệt, vui lòng thử lại");

        JobInfo jobInfo = jobInfoOptional.get();
        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        pendingTask.setApproveBy(principal.getUserId());
        pendingTask.setWfStatusCode(request.getWfStatusCd());
        pendingTaskRepository.save(pendingTask);

        jobInfo.setApproveStatus(request.getWfStatusCd());
        jobInfo.setReward(request.getReward());
        jobInfo.setPriorityJob(request.getPriorityJob());
        jobInfoRepository.save(jobInfo);
        return BasicResponseDto.ok();
    }
}
