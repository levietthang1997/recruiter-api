package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.LocationDto;
import com.chechen.dto.PaginationDto;
import com.chechen.entity.Location;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.LocationMapper;
import com.chechen.repository.LocationRepository;
import com.chechen.repository.SearchRepository;
import com.chechen.services.LocationService;
import com.chechen.services.vm.location.CreateLocationVM;
import com.chechen.services.vm.location.DeleteLocationVM;
import com.chechen.services.vm.location.SearchLocationVM;
import com.chechen.services.vm.location.UpdateLocationVM;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LocationServiceImpl implements LocationService {

    private final LocationRepository locationRepository;
    private final LocationMapper locationMapper;
    private final SearchRepository<Location, SearchLocationVM> searchRepository;

    public LocationServiceImpl(LocationRepository locationRepository, LocationMapper locationMapper, SearchRepository<Location, SearchLocationVM> searchRepository) {
        this.locationRepository = locationRepository;
        this.locationMapper = locationMapper;
        this.searchRepository = searchRepository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto save(CreateLocationVM request) {
        Location location = new Location();
        location.setName(request.getName());
        location.setIsDeleted(Constant.STR_N);
        locationRepository.save(location);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto update(UpdateLocationVM request) throws BusinessException {
        var locationOptional = locationRepository.findById(request.getId());
        if (locationOptional.isEmpty()) {
            throw new BusinessException("Lỗi không tìm thấy location");
        }
        var location = locationOptional.get();
        location.setName(request.getName());
        locationRepository.save(location);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto delete(DeleteLocationVM request) throws BusinessException {
        var locations = locationRepository.findAllById(request.getListId());
        if (locations.size() > 0) {
            for(Location locationItem : locations)
                locationItem.setIsDeleted(Constant.STR_Y);
            locationRepository.saveAll(locations);
        } else {
            throw new BusinessException("Không tìm thấy location");
        }
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto<PaginationDto<LocationDto>> search(SearchLocationVM searchLocationVM) {
        searchLocationVM.setIsDeleted(Constant.STR_N);
        PaginationDto<Location> result = searchRepository.searchData(searchLocationVM, Location.class);
        return BasicResponseDto.ok(locationMapper.toPaginationDto(result));
    }

    @Override
    @Transactional
    public BasicResponseDto<List<LocationDto>> findAll() {
        List<LocationDto> response = locationMapper.toListDto(locationRepository.findAllByIsDeleted(Constant.STR_N));
        return BasicResponseDto.ok(response);
    }
}
