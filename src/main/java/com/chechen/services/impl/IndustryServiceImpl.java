package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.IndustryDto;
import com.chechen.entity.Industry;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.IndustryMapper;
import com.chechen.repository.IndustryRepository;
import com.chechen.services.IndustryService;
import com.chechen.services.vm.company.CreateCompanyIndustryVM;
import com.chechen.services.vm.company.DeleteIndustryVM;
import com.chechen.services.vm.company.UpdateCompanyIndustryVM;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

/**
 * @author thanglv on 23/06/2022
 * @project recruiter-api
 */
@Service
public class IndustryServiceImpl implements IndustryService {

    private final IndustryRepository industryRepository;
    private final IndustryMapper industryMapper;

    public IndustryServiceImpl(IndustryRepository industryRepository, IndustryMapper industryMapper) {
        this.industryRepository = industryRepository;
        this.industryMapper = industryMapper;
    }

    @Override
    @Transactional
    public BasicResponseDto create(CreateCompanyIndustryVM request) {
        Industry industry = new Industry();
        industry.setName(request.getName());
        industry.setIsDeleted(Constant.STR_N);
        industryRepository.save(industry);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto update(UpdateCompanyIndustryVM request) throws BusinessException {
        Optional<Industry> cpIndustryOptional = industryRepository.findByIdAndIsDeleted(request.getId(), Constant.STR_N);
        if (cpIndustryOptional.isEmpty())
            throw new BusinessException("Mã ngành nghề không tồn tại");

        Industry industry = cpIndustryOptional.get();
        industry.setName(request.getName());
        industryRepository.save(industry);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto<Set<IndustryDto>> getAll() {
        return BasicResponseDto.ok(industryMapper.toListDto(industryRepository.findAllByIsDeleted(Constant.STR_N)));
    }

    @Override
    @Transactional
    public BasicResponseDto delete(DeleteIndustryVM request) throws BusinessException {
        var cpIndustryList = industryRepository.findAllByIdInAndIsDeleted(request.getListId(), Constant.STR_N);
        if (cpIndustryList.size() > 0) {
            for (Industry industryItem : cpIndustryList)
                industryItem.setIsDeleted(Constant.STR_Y);
            industryRepository.saveAll(cpIndustryList);
        } else {
            throw new BusinessException("Không tìm thấy ngành nghề công ty");
        }
        return BasicResponseDto.ok();
    }
}
