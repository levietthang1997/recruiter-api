package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.CpBenefitDto;
import com.chechen.entity.CpBenefit;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.CompanyBenefitMapper;
import com.chechen.repository.CompanyBenefitRepository;
import com.chechen.services.CompanyBenefitService;
import com.chechen.services.vm.company.CreateCompanyBenefitVM;
import com.chechen.services.vm.company.DeleteCompanyBenefitVM;
import com.chechen.services.vm.company.UpdateCompanyBenefitVM;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

/**
 * @author thanglv on 25/06/2022
 * @project recruiter-api
 */
@Service
public class CompanyBenefitServiceImpl implements CompanyBenefitService {

    private final CompanyBenefitRepository companyBenefitRepository;
    private final CompanyBenefitMapper companyBenefitMapper;

    public CompanyBenefitServiceImpl(CompanyBenefitRepository companyBenefitRepository, CompanyBenefitMapper companyBenefitMapper) {
        this.companyBenefitRepository = companyBenefitRepository;
        this.companyBenefitMapper = companyBenefitMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto create(CreateCompanyBenefitVM request) {
        CpBenefit cpBenefit = new CpBenefit();
        cpBenefit.setIcon(request.getIcon());
        cpBenefit.setName(request.getName());
        cpBenefit.setIsDeleted(Constant.STR_N);
        companyBenefitRepository.save(cpBenefit);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto update(UpdateCompanyBenefitVM request) throws BusinessException {
        Optional<CpBenefit> cpBenefitOptional = companyBenefitRepository.findByIdAndIsDeleted(request.getId(), Constant.STR_N);
        if (cpBenefitOptional.isEmpty())
            throw new BusinessException("Mã lợi ích công ty không tồn tại");

        CpBenefit cpBenefit = cpBenefitOptional.get();
        cpBenefit.setName(request.getName());
        cpBenefit.setIcon(request.getIcon());
        companyBenefitRepository.save(cpBenefit);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional
    public BasicResponseDto<Set<CpBenefitDto>> getAll() {
        return BasicResponseDto.ok(companyBenefitMapper.toListDto(companyBenefitRepository.findAllByIsDeleted(Constant.STR_N)));
    }

    @Override
    @Transactional
    public BasicResponseDto delete(DeleteCompanyBenefitVM request) throws BusinessException {
        var cpBenefitList = companyBenefitRepository.findAllByIdInAndIsDeleted(request.getListId(), Constant.STR_N);
        if (cpBenefitList.size() > 0) {
            for (CpBenefit cpBenefitItem : cpBenefitList)
                cpBenefitItem.setIsDeleted(Constant.STR_Y);
            companyBenefitRepository.saveAll(cpBenefitList);
        } else {
            throw new BusinessException("Không tìm thấy lợi ích công ty");
        }
        return BasicResponseDto.ok();
    }
}
