package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.*;
import com.chechen.entity.*;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.JobInfoMapper;
import com.chechen.obj.Oauth2UserPrincipal;
import com.chechen.repository.*;
import com.chechen.services.JobService;
import com.chechen.services.vm.jobinfo.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.search.engine.search.sort.dsl.SearchSortFactory;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * @author thanglv on 07/06/2022
 * @project recruiter-api
 */
@Service
public class JobServiceImpl implements JobService {
    private final Logger logger = LogManager.getLogger();

    private static final int DEFAULT_PAGE_SIZE = 20;
    private static final String SALARY_SORT_FIELD = "salary";
    private static final String REWARD_SORT_FIELD = "reward";
    private static final String ASC = "ASC";
    private static final String DESC = "DESC";

    private final SkillRepository skillRepository;
    private final LocationRepository locationRepository;
    private final LevelRepository levelRepository;
    private final JobInfoRepository jobInfoRepository;
    private final PendingTaskRepository pendingTaskRepository;
    private final CompanyProfileRepository companyProfileRepository;
    private final JobInfoPaginationRepository jobInfoPaginationRepository;
    private final JobInfoMapper jobInfoMapper;
    private final EntityManager entityManager;

    public JobServiceImpl(SkillRepository skillRepository, LocationRepository locationRepository,
                          LevelRepository levelRepository, JobInfoRepository jobInfoRepository,
                          PendingTaskRepository pendingTaskRepository,
                          CompanyProfileRepository companyProfileRepository,
                          JobInfoPaginationRepository jobInfoPaginationRepository, JobInfoMapper jobInfoMapper,
                          EntityManager entityManager) {
        this.skillRepository = skillRepository;
        this.locationRepository = locationRepository;
        this.levelRepository = levelRepository;
        this.jobInfoRepository = jobInfoRepository;
        this.pendingTaskRepository = pendingTaskRepository;
        this.companyProfileRepository = companyProfileRepository;
        this.jobInfoPaginationRepository = jobInfoPaginationRepository;
        this.jobInfoMapper = jobInfoMapper;
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto save(CreateJobInfoVM request) throws BusinessException {
        Set<String> listSkill = request.getSkills();
        Set<Skill> skills = skillRepository.findAllByIdInAndIsDeleted(listSkill, Constant.STR_N);
        if (skills.size() == 0)
            throw new BusinessException("Skill not found");
        Set<Location> locations = locationRepository.findAllByIdInAndIsDeleted(request.getLocations(), Constant.STR_N);
        if (locations.size() == 0)
            throw new BusinessException("Location not found");
        Set<Level> levels = levelRepository.findAllByIdInAndIsDeleted(request.getLevels(), Constant.STR_N);
        if (levels.size() == 0)
            throw new BusinessException("Level not found");

        if (!Constant.STR_N.equals(request.getIsNegotiableSalary()) && !Constant.STR_Y.equals(request.getIsNegotiableSalary()))
            throw new BusinessException("IsNegotiableSalary is Y or N");

        JobInfo jobInfo = new JobInfo();
        jobInfo.setTitle(request.getTitle());
        jobInfo.setSkills(skills);
        jobInfo.setIsNegotiableSalary(request.getIsNegotiableSalary());
        jobInfo.setSalaryFrom(request.getSalaryFrom());
        jobInfo.setSalaryTo(request.getSalaryTo());
        jobInfo.setVacancies(request.getVacancies());
        jobInfo.setTeamSize(request.getTeamSize());
        jobInfo.setDepartment(request.getDepartment());
        jobInfo.setJobType(request.getJobType());
        jobInfo.setLocations(locations);
        jobInfo.setAddress(request.getAddress());
        jobInfo.setJobOverView(request.getJobOverView());
        jobInfo.setJobRequirement(request.getJobRequirement());
        jobInfo.setPrioritySkill(request.getPrioritySkill());
        jobInfo.setWhyWorkingHere(request.getWhyWorkingHere());
        jobInfo.setInterviewingProcess(request.getInterviewingProcess());
        jobInfo.setLevels(levels);
        jobInfo.setNoticeToHeadhunter(request.getNoticeToHeadhunter());
        jobInfo.setEmployeeType(request.getEmployeeType());
        jobInfo.setIsDeleted(Constant.STR_N);
        jobInfo.setIsPlatform(request.getIsPlatform());
        jobInfo.setPriorityJob(Constant.DEFAULT_PRIORITY);
        jobInfo.setApproveStatus(Constant.PENDING_TASK.PENDING_APPROVE.getCode());
        String workFlowId = UUID.randomUUID().toString();
        jobInfo.setWorkFlowId(workFlowId);
        jobInfo.setCompanyJobStatus(Constant.COMPANY_JOB_STATUS.OPEN);
        //set mặc định tiền thưởng là 0 chờ admin duyệt
        jobInfo.setReward(new BigDecimal("0"));
        // lấy ra company profile của account
        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<CompanyProfile> companyProfileOptional = companyProfileRepository.findByAccountInfoKeycloakUserId(principal.getUserId());
        if (companyProfileOptional.isEmpty())
            throw new BusinessException("Không tìm thấy hồ sơ công ty, vui lòng thử lại");
        jobInfo.setCompanyProfile(companyProfileOptional.get());
        // lưu
        jobInfoRepository.save(jobInfo);

        // tạo pending task để cho admin phê duyệt, nếu mà là admin thì coi như được duyệt luôn
        PendingTask pendingTask = new PendingTask();
        pendingTask.setWfStatusCode(Constant.PENDING_TASK.PENDING_APPROVE.getCode());
        pendingTask.setString1(jobInfo.getId());
        pendingTask.setString2(jobInfo.getTitle());
        pendingTask.setIsDelete(Constant.STR_N);
        pendingTask.setType(Constant.PENDING_TASK_TYPE.JOB);
        pendingTaskRepository.save(pendingTask);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto update(UpdateJobInfoVM request) throws BusinessException {
        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Optional<CompanyProfile> companyProfileOptional = companyProfileRepository.findByAccountInfoKeycloakUserId(principal.getUserId());
        if (companyProfileOptional.isEmpty())
            throw new BusinessException("Không tìm thấy Job cần sửa");

        CompanyProfile companyProfile = companyProfileOptional.get();

        Optional<JobInfo> jobInfoOpt = jobInfoRepository.findByIdAndIsDeleted(request.getId(), Constant.STR_N);
        if (jobInfoOpt.isEmpty())
            throw new BusinessException("Không tìm thấy Job cần sửa");

        JobInfo jobInfo = jobInfoOpt.get();
        if (jobInfo.getCompanyProfile().getId().equals(companyProfile.getId()))
            throw new BusinessException("Không tìm thấy job cần sửa");

        if (Constant.PENDING_TASK.APPROVED.getCode().equals(jobInfo.getApproveStatus()))
            throw new BusinessException("Không thể sửa Job đã được duyệt");

        List<String> listSkillId = request.getSkills();
        List<Skill> skills = skillRepository.findAllById(listSkillId);
        if (skills.size() == 0)
            throw new BusinessException("Skill not found");
        List<Location> locations = locationRepository.findAllById(request.getLocations());
        if (locations.size() == 0)
            throw new BusinessException("Location not found");
        List<Level> levels = levelRepository.findAllById(request.getLevels());
        if (levels.size() == 0)
            throw new BusinessException("Level not found");

        if (!Constant.STR_N.equals(request.getIsNegotiableSalary()) && !Constant.STR_Y.equals(request.getIsNegotiableSalary()))
            throw new BusinessException("IsNegotiableSalary is Y or N");

        jobInfo.getSkills().clear();
        jobInfo.getSkills().addAll(skills);

        jobInfo.getLocations().clear();
        jobInfo.getLocations().addAll(locations);

        jobInfo.getLevels().clear();
        jobInfo.getLevels().addAll(levels);

        jobInfo.setTitle(request.getTitle());
        jobInfo.setIsNegotiableSalary(request.getIsNegotiableSalary());
        jobInfo.setSalaryFrom(request.getSalaryFrom());
        jobInfo.setSalaryTo(request.getSalaryTo());
        jobInfo.setVacancies(request.getVacancies());
        jobInfo.setTeamSize(request.getTeamSize());
        jobInfo.setDepartment(request.getDepartment());
        jobInfo.setJobType(request.getJobType());
        jobInfo.setAddress(request.getAddress());
        jobInfo.setJobOverView(request.getJobOverView());
        jobInfo.setJobRequirement(request.getJobRequirement());
        jobInfo.setPrioritySkill(request.getPrioritySkill());
        jobInfo.setWhyWorkingHere(request.getWhyWorkingHere());
        jobInfo.setInterviewingProcess(request.getInterviewingProcess());
        jobInfo.setNoticeToHeadhunter(request.getNoticeToHeadhunter());
        jobInfo.setEmployeeType(request.getEmployeeType());

        jobInfoRepository.save(jobInfo);

        Optional<PendingTask> pendingTaskOptional = pendingTaskRepository.findByString1(jobInfo.getId());
        if (pendingTaskOptional.isEmpty())
            throw new BusinessException("Lỗi không tìm thấy giao dịch");
        PendingTask pendingTask = pendingTaskOptional.get();
        pendingTask.setString2(jobInfo.getTitle());
        pendingTaskRepository.save(pendingTask);

        return BasicResponseDto.ok();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto delete(DeleteJobInfoVM request) throws BusinessException {
        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Optional<CompanyProfile> companyProfileOptional = companyProfileRepository.findByAccountInfoKeycloakUserId(principal.getUserId());
        if (companyProfileOptional.isEmpty())
            throw new BusinessException("Không tìm thấy Job cần sửa");

        CompanyProfile companyProfile = companyProfileOptional.get();

        Optional<JobInfo> jobInfoOpt = jobInfoRepository.findByIdAndIsDeleted(request.getId(), Constant.STR_N);
        if (jobInfoOpt.isEmpty())
            throw new BusinessException("Không tìm thấy Job cần sửa");

        JobInfo jobInfo = jobInfoOpt.get();
        if (jobInfo.getCompanyProfile().getId().equals(companyProfile.getId()))
            throw new BusinessException("Không tìm thấy job cần sửa");

        jobInfo.setIsDeleted(Constant.STR_N);
        jobInfoRepository.save(jobInfo);
        return BasicResponseDto.ok();
    }

    @Override
    @Transactional(
            readOnly = true
    )
    public BasicResponseDto<PaginationDto<JobInfoDtoV2>> search(SearchJobInfoVM request) {
        logger.info("Normal user search job...");
        logger.info("Query data: {}", request);
        SearchSession searchSession = Search.session(entityManager);
        var queryFinalStep = searchSession.search(JobInfo.class).where(f -> f.bool(b -> {
            if (request.getSalaryFrom() != null)
                b.must(f.range().field("salaryFrom").greaterThan(request.getSalaryFrom()));

            if (request.getSalaryTo() != null)
                b.must(f.range().field("salaryTo").lessThan(request.getSalaryTo()));

            if (request.getLocation() != null)
                b.must(f.match().field("locations.id").matching(request.getLocation()));

            if (request.getEmployeeType() != null)
                b.must(f.match().field("employeeType").matching(request.getEmployeeType()));

            if (request.getKeyword() != null) {
                b.must(f.match().fields("title", "address", "jobOverView", "jobRequirement", "prioritySkill",
                                "whyWorkingHere", "interviewingProcess", "locations.name", "skills.name", "jobOverView",
                                "prioritySkill", "whyWorkingHere", "interviewingProcess", "levels.name", "companyProfile.companyName",
                                "companyProfile.companyPhone", "companyProfile.companyAddress", "companyProfile.companyEmail",
                                "companyProfile.ccMail", "companyProfile.website", "companyProfile.companyIndustries.name",
                                "companyProfile.linkedIn", "companyProfile.facebook", "companyProfile.firstNameRepresentative",
                                "companyProfile.lastNameRepresentative", "companyProfile.emailRepresentative",
                                "companyProfile.phoneRepresentative", "department", "jobType")
                        .matching(request.getKeyword()));
            }

            b.must(f.match().field("approveStatus").matching(Constant.PENDING_TASK.APPROVED.getCode()))
                    .must(f.match().field("isDeleted").matching(Constant.STR_N));
        }));
        queryFinalStep.sort(SearchSortFactory::score);

        if (SALARY_SORT_FIELD.equals(request.getSort())) {
            if (DESC.equals(request.getOrder())) {
                queryFinalStep.sort(f -> f.field("salaryTo").desc());
                queryFinalStep.sort(f -> f.field("salaryFrom").desc());
            } else {
                queryFinalStep.sort(f -> f.field("salaryTo").asc());
                queryFinalStep.sort(f -> f.field("salaryFrom").asc());
            }
        } else if (REWARD_SORT_FIELD.equals(request.getSort())) {
            if (DESC.equals(request.getOrder())) {
                queryFinalStep.sort(f -> f.field("reward").desc());
            } else {
                queryFinalStep.sort(f -> f.field("reward").asc());
            }
        }

        if (request.getPage() == null)
            request.setPage(0);
        List<JobInfo> jobInfoList = queryFinalStep.fetchHits(request.getPage() * DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE);
        List<JobInfoDtoV2> data = jobInfoMapper.toDtoV2List(jobInfoList);
        PaginationDto<JobInfoDtoV2> paginationDto = new PaginationDto<>();
        paginationDto.setPage(request.getPage());
        paginationDto.setTotal(queryFinalStep.fetchTotalHitCount());
        paginationDto.setSize(DEFAULT_PAGE_SIZE);
        paginationDto.setData(data);
        return BasicResponseDto.ok(paginationDto);
    }

    @Override
    @Transactional
    public BasicResponseDto<PaginationDto<JobInfoDto>> searchJobInfoCompanyOwn(SearchJobInfoCompanyOwnVM request) {
        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Pageable pageable = PageRequest.of(request.getPage(), request.getSize(), Sort.by("createDate").descending());
        List<JobInfo> jobInfoList = jobInfoPaginationRepository.findAllCustom(pageable,  request.getTitle(), request.getPendingTaskStatus(),  request.getJobType() , Constant.STR_N, principal.getUserId());
        long countData = jobInfoPaginationRepository.findCustomCount(request.getTitle(), request.getPendingTaskStatus(), request.getJobType(), Constant.STR_N, principal.getUserId());
        PaginationDto<JobInfoDto> paginationDto = new PaginationDto<>();
        paginationDto.setPage(request.getPage());
        paginationDto.setSize(request.getSize());
        paginationDto.setTotal(countData);
        paginationDto.setData(jobInfoMapper.toDtoList(jobInfoList));
        return BasicResponseDto.ok(paginationDto);
    }

    @Override
    @Transactional
    public BasicResponseDto<List<JobInfoDtoV2>> get10JobHomePage() {
        Pageable pageable = PageRequest.of(0, 10);
        List<JobInfo> jobInfoList = jobInfoRepository.findAllByIsDeletedAndApproveStatusOrderByPriorityJobDesc(pageable, Constant.STR_N, Constant.PENDING_TASK.APPROVED.getCode());
        return BasicResponseDto.ok(jobInfoMapper.toDtoV2List(jobInfoList));
    }

    @Override
    @Transactional
    public BasicResponseDto<JobInfoDtoV3> getJobPendingDetail(String pendingTaskId) throws BusinessException {
        Optional<PendingTask> pendingTaskOptional = pendingTaskRepository.findById(pendingTaskId);
        if (pendingTaskOptional.isEmpty())
            throw new BusinessException("Không tìm thấy công việc cần duyệt, vui lòng thử lại");
        PendingTask pendingTask = pendingTaskOptional.get();
        Optional<JobInfo> jobInfoOptional = jobInfoRepository.findByIdAndIsDeleted(pendingTask.getString1(), Constant.STR_N);
        if (jobInfoOptional.isEmpty())
            throw new BusinessException("Không tìm thấy công việc cần duyệt, vui lòng thử lại");
        return BasicResponseDto.ok(jobInfoMapper.toDtoV3(jobInfoOptional.get()));
    }

    @Override
    @Transactional
    public BasicResponseDto<JobInfoDtoV4> getJobInfoDetail(String jobId) throws BusinessException {
        Optional<JobInfo> jobInfoOptional = jobInfoRepository.findByIdAndIsDeleted(jobId, Constant.STR_N);
        if (jobInfoOptional.isEmpty())
            throw new BusinessException("Không tìm thấy công việc!");
        JobInfoDtoV4 jobInfoDtoV4 = jobInfoMapper.toDtoV4(jobInfoOptional.get());
        return BasicResponseDto.ok(jobInfoDtoV4);
    }
}
