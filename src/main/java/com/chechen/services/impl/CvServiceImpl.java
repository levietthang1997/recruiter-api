package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.CvDetectionDto;
import com.chechen.dto.CvInformationDto;
import com.chechen.dto.MsCvDetectionDto;
import com.chechen.entity.CvInformation;
import com.chechen.entity.FileManagement;
import com.chechen.entity.JobCategory;
import com.chechen.entity.Location;
import com.chechen.exception.BusinessException;
import com.chechen.obj.Oauth2UserPrincipal;
import com.chechen.repository.FileManagementRepository;
import com.chechen.repository.JobCategoryRepository;
import com.chechen.repository.LocationRepository;
import com.chechen.services.CvService;
import com.chechen.services.FileManagementService;
import com.chechen.services.vm.cv.CreateCvVM;
import com.chechen.services.vm.file.UploadCvVM;
import com.chechen.utils.HttpClientPool;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Service
public class CvServiceImpl implements CvService {
    private final Logger logger = LogManager.getLogger();

    @Value("${upload.private-path}")
    private String uploadPathPrivate;

    @Value("${upload.public-path}")
    private String uploadPathPublic;

    @Value("${resume-api.url}")
    private String resumeApiUrl;

    private final FileManagementService fileManagementService;
    private final ObjectMapper mapper;
    private final FileManagementRepository fileManagementRepository;
    private final LocationRepository locationRepository;
    private final JobCategoryRepository jobCategoryRepository;

    public CvServiceImpl(FileManagementService fileManagementService, ObjectMapper mapper, FileManagementRepository fileManagementRepository, LocationRepository locationRepository,
                         JobCategoryRepository jobCategoryRepository) {
        this.fileManagementService = fileManagementService;
        this.mapper = mapper;
        this.fileManagementRepository = fileManagementRepository;
        this.locationRepository = locationRepository;
        this.jobCategoryRepository = jobCategoryRepository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto<CvDetectionDto> uploadCv(UploadCvVM request) throws BusinessException, IOException {
        MultipartFile fileUpload = request.getFile();
        fileManagementService.validateFile(request.getFile());
        FileManagement fileInputCv = fileManagementService.uploadSingleFile(fileUpload, false);
        String urlRequest = resumeApiUrl + "?filePath=" + URLEncoder.encode(fileInputCv.getPath(), StandardCharsets.UTF_8);
        // call service xử lý file cv
        String resumeResponse = HttpClientPool.sendGet(urlRequest, 120_000, "application/json");
        MsCvDetectionDto msResponse = mapper.readValue(resumeResponse, MsCvDetectionDto.class);
        logger.info("=====> Ms resume service response: {}", msResponse);
        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        File file = new File(msResponse.getHiddenResume());
        if (!file.exists())
            throw new BusinessException("Processing cv failed");

        Path currentFile = Paths.get(file.getAbsolutePath());
        LocalDateTime now = LocalDateTime.now();
        String subDir = now.getYear() + "/" + now.getMonthValue() + "/" + now.getDayOfMonth() + "/" + principal.getUserId();
        Path targetDir = Paths.get(uploadPathPublic + subDir);
        if (Files.notExists(targetDir))
            Files.createDirectories(targetDir);
        Path targetPath = Files.move(currentFile, Paths.get(targetDir + "/" + file.getName()), StandardCopyOption.REPLACE_EXISTING);

        String ext = FilenameUtils.getExtension(file.getName());
        FileManagement fileManagement = new FileManagement();
        fileManagement.setFileExt(ext);
        fileManagement.setIsDeleted(Constant.STR_N);
        fileManagement.setContentType(fileUpload.getContentType());
        fileManagement.setName(file.getName());
        fileManagement.setPath(targetPath.toAbsolutePath().toString());
        fileManagement.setSize(file.length());
        fileManagement.setCreatedBy(principal.getUserId());
        fileManagement.setCreatedDate(new Date());
        fileManagement.setIsPublic(Constant.STR_Y);
        fileManagementRepository.save(fileManagement);

        CvDetectionDto cvDetectionDto = new CvDetectionDto();
        cvDetectionDto.setName(msResponse.getName());
        cvDetectionDto.setEmail(msResponse.getEmail());
        cvDetectionDto.setWebsite(msResponse.getWebsite());
        cvDetectionDto.setPhone(msResponse.getPhone());
        cvDetectionDto.setFileIdHidden(fileManagement.getId());
        cvDetectionDto.setFileIdOriginal(fileInputCv.getId());

        return BasicResponseDto.ok(cvDetectionDto);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BasicResponseDto<CvInformationDto> createCv(CreateCvVM request) throws BusinessException {
        // validate location xem có hợp lệ hay không
        Set<Location> locations = locationRepository.findAllByIdInAndIsDeleted(request.getLocations(), Constant.STR_N);
        if (locations.size() == 0)
            throw new BusinessException("Location not found");

        Set<JobCategory> jobCategories = jobCategoryRepository.findAllByIdInAndIsDeleted(request.getJobCategories(), Constant.STR_N);
        if (jobCategories.size() == 0)
            throw new BusinessException("JobCategory not found");


        return null;
    }
}
