package com.chechen.services.impl;

import com.chechen.constant.Constant;
import com.chechen.dto.BasicResponseDto;
import com.chechen.dto.HeadhuntBrandDto;
import com.chechen.dto.HeadhuntProfileDto;
import com.chechen.entity.*;
import com.chechen.exception.BusinessException;
import com.chechen.mapper.HeadhuntBrandMapper;
import com.chechen.mapper.HeadhuntProfileMapper;
import com.chechen.obj.Oauth2UserPrincipal;
import com.chechen.repository.*;
import com.chechen.services.HeadhuntService;
import com.chechen.services.vm.headhunt.UpdateHeadhuntBrandVM;
import com.chechen.services.vm.headhunt.UpdateHeadhuntProfileVM;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class HeadhuntServiceImpl implements HeadhuntService {

    private final Logger logger = LogManager.getLogger();

    @Value("${keycloak.auth-url}")
    private String keycloakAuthUrl;
    @Value("${keycloak.realm}")
    private String keycloakRealm;
    @Value("${keycloak.username}")
    private String keycloakUser;
    @Value("${keycloak.password}")
    private String keycloakPassword;
    @Value("${keycloak.client-id}")
    private String keycloakClientId;

    private final AccountInfoRepository accountInfoRepository;
    private final IndustryRepository industryRepository;
    private final HeadhuntProfileRepository headhuntProfileRepository;
    private final FileManagementRepository fileManagementRepository;
    private final HeadhuntProfileMapper headhuntProfileMapper;
    private final HeadhuntBrandMapper headhuntBrandMapper;
    private final JobRoleRepository jobRoleRepository;
    private final LevelRepository levelRepository;
    private final SkillRepository skillRepository;
    private final LanguageRepository languageRepository;
    private final LocationRepository locationRepository;
    private final HeadhuntBrandRepository headhuntBrandRepository;

    public HeadhuntServiceImpl(AccountInfoRepository accountInfoRepository, IndustryRepository industryRepository, HeadhuntProfileRepository headhuntProfileRepository,
                               FileManagementRepository fileManagementRepository, HeadhuntProfileMapper headhuntProfileMapper, HeadhuntBrandMapper headhuntBrandMapper,
                               JobRoleRepository jobRoleRepository, LevelRepository levelRepository, SkillRepository skillRepository, LanguageRepository languageRepository,
                               LocationRepository locationRepository, HeadhuntBrandRepository headhuntBrandRepository) {
        this.accountInfoRepository = accountInfoRepository;
        this.industryRepository = industryRepository;
        this.headhuntProfileRepository = headhuntProfileRepository;
        this.fileManagementRepository = fileManagementRepository;
        this.headhuntProfileMapper = headhuntProfileMapper;
        this.headhuntBrandMapper = headhuntBrandMapper;
        this.jobRoleRepository = jobRoleRepository;
        this.levelRepository = levelRepository;
        this.skillRepository = skillRepository;
        this.languageRepository = languageRepository;
        this.locationRepository = locationRepository;
        this.headhuntBrandRepository = headhuntBrandRepository;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public BasicResponseDto<HeadhuntProfileDto> updateProfile(UpdateHeadhuntProfileVM request) throws BusinessException {
        logger.info("====> Update headhunt profile");
        // check gender
        if (!Constant.GENDER.NONE.equals(request.getGender()) && !Constant.GENDER.MALE.equals(request.getGender()) && !Constant.GENDER.FEMALE.equals(request.getGender()))
            throw new BusinessException("Gender invalid");

        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userId = principal.getUserId();
        Optional<AccountInfo> accountInfoOptional = accountInfoRepository.findByKeycloakUserId(userId, Constant.STR_N);
        if (accountInfoOptional.isEmpty())
            throw new BusinessException("Account not found");

        AccountInfo accountInfo = accountInfoOptional.get();
        String oldPhone = accountInfo.getPhone();
        // hệ thống sẽ không cho headhunt update email vì email như user name có thể đăng nhập và nó là duy nhất
        HeadhuntProfile headhuntProfile = accountInfo.getHeadhuntProfile();
        headhuntProfile.setFirstName(request.getFirstName());
        headhuntProfile.setLastName(request.getLastName());
        headhuntProfile.setHotline(request.getHotline());
        headhuntProfile.setAddress(request.getAddress());
        headhuntProfile.setGender(request.getGender());
        headhuntProfile.setLinkedIn(request.getLinkedIn());
        headhuntProfile.setIntroduction(request.getIntroduction());

        // update avatar
        if (request.getAvatarId() != null) {
            Optional<FileManagement> avatarOptional = fileManagementRepository.findByIdAndCreatedByAndIsDeleted(request.getAvatarId(), principal.getUserId(), Constant.STR_N);
            if (avatarOptional.isEmpty())
                throw new BusinessException("AvatarId not found");
            else {
                FileManagement avatarFile = avatarOptional.get();
                headhuntProfile.setAvatarId(avatarFile.getId());
                headhuntProfile.setAvatarUrl(avatarFile.getViewLink());
            }
        }
        headhuntProfileRepository.save(headhuntProfile);
        accountInfo.setPhone(request.getPhone());
        accountInfoRepository.save(accountInfo);

        if (!request.getPhone().equals(oldPhone)) {
            var keycloak = Keycloak.getInstance(keycloakAuthUrl, keycloakRealm, keycloakUser, keycloakPassword, keycloakClientId);
            var usersResource = keycloak.realm(keycloakRealm).users().get(accountInfo.getKeycloakUserId());
            var userReg = new UserRepresentation();
            userReg.singleAttribute(Constant.USER_INFO.ATTRIBUTE.PHONE, request.getPhone());
            usersResource.update(userReg);
        }
        HeadhuntProfileDto headhuntProfileDto = headhuntProfileMapper.toDto(headhuntProfile);
        headhuntProfileDto.setEmail(accountInfo.getEmail());
        headhuntProfileDto.setPhone(accountInfo.getPhone());
        return BasicResponseDto.ok(headhuntProfileDto);
    }

    @Override
    @Transactional
    public BasicResponseDto<HeadhuntProfileDto> getProfile() throws BusinessException {
        logger.info("====> Get headhunt profile");
        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userId = principal.getUserId();
        Optional<AccountInfo> accountInfoOptional = accountInfoRepository.findByKeycloakUserId(userId, Constant.STR_N);
        if (accountInfoOptional.isEmpty())
            throw new BusinessException("Account not found");
        AccountInfo accountInfo = accountInfoOptional.get();
        HeadhuntProfileDto headhuntProfileDto = headhuntProfileMapper.toDto(accountInfo.getHeadhuntProfile());
        headhuntProfileDto.setEmail(accountInfo.getEmail());
        headhuntProfileDto.setPhone(accountInfo.getPhone());
        return BasicResponseDto.ok(headhuntProfileDto);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public BasicResponseDto<HeadhuntBrandDto> getBrandInfo() throws BusinessException {
        logger.info("====> Get headhunt profile");
        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userId = principal.getUserId();
        Optional<AccountInfo> accountInfoOptional = accountInfoRepository.findByKeycloakUserId(userId, Constant.STR_N);
        if (accountInfoOptional.isEmpty())
            throw new BusinessException("Account not found");
        AccountInfo accountInfo = accountInfoOptional.get();
        HeadhuntBrand headhuntBrand = accountInfo.getHeadhuntProfile().getHeadhuntBrand();
        HeadhuntBrandDto headhuntBrandDto = headhuntBrandMapper.toDto(headhuntBrand);
        return BasicResponseDto.ok(headhuntBrandDto);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public BasicResponseDto<HeadhuntBrandDto> updateBrandInfo(UpdateHeadhuntBrandVM request) throws BusinessException {
        Oauth2UserPrincipal principal = (Oauth2UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userId = principal.getUserId();
        Optional<AccountInfo> accountInfoOptional = accountInfoRepository.findByKeycloakUserId(userId, Constant.STR_N);
        if (accountInfoOptional.isEmpty())
            throw new BusinessException("Account not found");

        AccountInfo accountInfo = accountInfoOptional.get();
        HeadhuntBrand headhuntBrand = accountInfo.getHeadhuntProfile().getHeadhuntBrand();
        // hình ảnh
        if (request.getBrandLogoId() != null) {
            Optional<FileManagement> brandLogoOptional = fileManagementRepository.findByIdAndCreatedByAndIsDeleted(request.getBrandLogoId(), principal.getUserId(), Constant.STR_N);
            if (brandLogoOptional.isEmpty())
                throw new BusinessException("Brand logo not found");
            else {
                FileManagement brandLogo = brandLogoOptional.get();
                headhuntBrand.setBrandLogoId(brandLogo.getId());
                headhuntBrand.setViewLinkBrandLogo(brandLogo.getViewLink());
            }
        }
        headhuntBrand.setName(request.getName());
        headhuntBrand.setPhone(request.getPhone());
        headhuntBrand.setTeamLeadEmail(request.getTeamLeadEmail());
        headhuntBrand.setAddress(request.getAddress());
        headhuntBrand.setCcMail(request.getCcMail());

        // thông tin liên hệ
        headhuntBrand.setContactFirstName(request.getContactFirstName());
        headhuntBrand.setContactLastName(request.getContactLastName());
        headhuntBrand.setContactTitle(request.getContactTitle());
        headhuntBrand.setContactPhone(request.getContactPhone());
        headhuntBrand.setContactEmail(request.getContactEmail());

        // Hồ sơ
        // -- thông báo tuyển dụng
        headhuntBrand.setMessage(request.getMessage());
        // các lĩnh vực mà hr biết
        if (request.getJobRoles() != null) {
            if (request.getJobRoles().size() > 0) {
                Set<JobRole> jobRoleList = jobRoleRepository.findAllByIdInAndIsDeleted(request.getJobRoles(), Constant.STR_N);
                if (request.getJobRoles().size() > 0 && jobRoleList.isEmpty())
                    throw new BusinessException("Không tìm thấy job role");
                Map<String, JobRole> jobRoleMapVM = new HashMap<>();
                for (JobRole item : jobRoleList) {
                    jobRoleMapVM.put(item.getId(), item);
                }

                Map<String, JobRole> companyBrandJobRoleMap = new HashMap<>();
                List<JobRole> lstRemove = new ArrayList<>();
                for (JobRole item : headhuntBrand.getJobRoles()) {
                    companyBrandJobRoleMap.put(item.getId(), item);
                    if (jobRoleMapVM.get(item.getId()) == null)
                        lstRemove.add(item);
                }
                for (JobRole item : jobRoleList) {
                    if (companyBrandJobRoleMap.get(item.getId()) == null)
                        headhuntBrand.getJobRoles().add(item);
                }
                for (JobRole item : lstRemove)
                    headhuntBrand.getJobRoles().remove(item);
            } else {
                headhuntBrand.getJobRoles().clear();
            }
        }

        // vị trí mà headhunt có thể giúp (level)
        if (request.getLevels() != null) {
            if (request.getLevels().size() > 0) {
                Set<Level> levelList = levelRepository.findAllByIdInAndIsDeleted(request.getLevels(), Constant.STR_N);
                if (request.getLevels().size() > 0 && levelList.isEmpty())
                    throw new BusinessException("Không tìm thấy level");
                Map<String, Level> levelInput = new HashMap<>();
                for (Level item : levelList) {
                    levelInput.put(item.getId(), item);
                }

                Map<String, Level> existsLevelMap = new HashMap<>();
                List<Level> lstRemove = new ArrayList<>();
                for (Level item : headhuntBrand.getLevels()) {
                    existsLevelMap.put(item.getId(), item);
                    if (levelInput.get(item.getId()) == null)
                        lstRemove.add(item);
                }
                for (Level item : levelList) {
                    if (existsLevelMap.get(item.getId()) == null)
                        headhuntBrand.getLevels().add(item);
                }
                for (Level item : lstRemove)
                    headhuntBrand.getLevels().remove(item);
            } else {
                headhuntBrand.getLevels().clear();
            }
        }

        // ngành mà hr am hiểu
        if (request.getHeadhuntBrandIndustries() != null) {
            if (request.getHeadhuntBrandIndustries().size() > 0) {
                Set<Industry> industryList = industryRepository.findAllByIdInAndIsDeleted(request.getHeadhuntBrandIndustries(), Constant.STR_N);
                if (request.getHeadhuntBrandIndustries().size() > 0 && industryList.isEmpty())
                    throw new BusinessException("Không tìm thấy industry");
                Map<String, Industry> industryInput = new HashMap<>();
                for (Industry item : industryList) {
                    industryInput.put(item.getId(), item);
                }

                Map<String, Industry> existsIndustryMap = new HashMap<>();
                List<Industry> lstRemove = new ArrayList<>();
                for (Industry item : headhuntBrand.getHeadhuntBrandIndustries()) {
                    existsIndustryMap.put(item.getId(), item);
                    if (industryInput.get(item.getId()) == null)
                        lstRemove.add(item);
                }
                for (Industry item : industryList) {
                    if (existsIndustryMap.get(item.getId()) == null)
                        headhuntBrand.getHeadhuntBrandIndustries().add(item);
                }
                for (Industry item : lstRemove)
                    headhuntBrand.getHeadhuntBrandIndustries().remove(item);
            } else {
                headhuntBrand.getHeadhuntBrandIndustries().clear();
            }
        }
        // 10 kỹ năng hàng đầu mà hr có thể tiếp cận
        if (request.getSkills() != null) {
            if (request.getSkills().size() > 0) {
                Set<Skill> skillList = skillRepository.findAllByIdInAndIsDeleted(request.getSkills(), Constant.STR_N);
                if (request.getSkills().size() > 0 && skillList.isEmpty())
                    throw new BusinessException("Không tìm thấy skill");
                Map<String, Skill> skillInput = new HashMap<>();
                for (Skill item : skillList) {
                    skillInput.put(item.getId(), item);
                }

                Map<String, Skill> existsSkillMap = new HashMap<>();
                List<Skill> lstRemove = new ArrayList<>();
                for (Skill item : headhuntBrand.getSkills()) {
                    existsSkillMap.put(item.getId(), item);
                    if (skillInput.get(item.getId()) == null)
                        lstRemove.add(item);
                }
                for (Skill item : skillList) {
                    if (existsSkillMap.get(item.getId()) == null)
                        headhuntBrand.getSkills().add(item);
                }
                for (Skill item : lstRemove)
                    headhuntBrand.getSkills().remove(item);
            } else {
                headhuntBrand.getSkills().clear();
            }
        }
        // ngôn ngữ hr thành thạo
        if (request.getHeadhuntBrandLanguage() != null) {
            if (request.getHeadhuntBrandLanguage().size() > 0) {
                Set<Language> languageList = languageRepository.findAllByIdInAndIsDeleted(request.getHeadhuntBrandLanguage(), Constant.STR_N);
                if (request.getHeadhuntBrandLanguage().size() > 0 && languageList.isEmpty())
                    throw new BusinessException("Không tìm thấy language");
                Map<String, Language> languageInput = new HashMap<>();
                for (Language item : languageList) {
                    languageInput.put(item.getId(), item);
                }

                Map<String, Language> existsLanguageMap = new HashMap<>();
                List<Language> lstRemove = new ArrayList<>();
                for (Language item : headhuntBrand.getHeadhuntBrandLanguage()) {
                    existsLanguageMap.put(item.getId(), item);
                    if (languageInput.get(item.getId()) == null)
                        lstRemove.add(item);
                }
                for (Language item : languageList) {
                    if (existsLanguageMap.get(item.getId()) == null)
                        headhuntBrand.getHeadhuntBrandLanguage().add(item);
                }
                for (Language item : lstRemove)
                    headhuntBrand.getHeadhuntBrandLanguage().remove(item);
            } else {
                headhuntBrand.getHeadhuntBrandLanguage().clear();
            }
        }
        // khu vực headhunt tuyển dụng
        if (request.getLocations() != null) {
            if (request.getLocations().size() > 0) {
                Set<Location> locationList = locationRepository.findAllByIdInAndIsDeleted(request.getHeadhuntBrandLanguage(), Constant.STR_N);
                if (request.getLocations().size() > 0 && locationList.isEmpty())
                    throw new BusinessException("Không tìm thấy location");
                Map<String, Location> locationInput = new HashMap<>();
                for (Location item : locationList) {
                    locationInput.put(item.getId(), item);
                }

                Map<String, Location> existsLocationMap = new HashMap<>();
                List<Location> lstRemove = new ArrayList<>();
                for (Location item : headhuntBrand.getLocations()) {
                    existsLocationMap.put(item.getId(), item);
                    if (locationInput.get(item.getId()) == null)
                        lstRemove.add(item);
                }
                for (Location item : locationList) {
                    if (existsLocationMap.get(item.getId()) == null)
                        headhuntBrand.getLocations().add(item);
                }
                for (Location item : lstRemove)
                    headhuntBrand.getLocations().remove(item);
            } else {
                headhuntBrand.getLocations().clear();
            }
        }
        // facebook, linkedIn, skype
        headhuntBrand.setFacebook(request.getFacebook());
        headhuntBrand.setLinkedIn(request.getLinkedIn());
        headhuntBrand.setSkype(request.getSkype());
        headhuntBrandRepository.save(headhuntBrand);
        return BasicResponseDto.ok();
    }
}
