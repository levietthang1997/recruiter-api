package com.chechen.obj;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @author thanglv on 29/05/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
@ToString
public class AudiencePermission implements Serializable {
    private List<String> scopes;
    private String rsid;
    private String rsname;
}
