package com.chechen.obj;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @author thanglv on 29/05/2022
 * @project recruiter-api
 */
@Data
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenAuthorization implements Serializable {
    private List<AudiencePermission> permissions;
}
