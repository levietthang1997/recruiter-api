package com.chechen.obj;

import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author thanglv on 04/06/2022
 * @project recruiter-api
 */
@ToString
public class Oauth2UserPrincipal implements OAuth2User {
    private final String username;
    private final String userId;
    private final String email;
    private final String fullName;
    private final List<GrantedAuthority> roles;

    public Oauth2UserPrincipal(String username, String userId, String email, String fullName, List<GrantedAuthority>  roles) {
        this.username = username;
        this.userId = userId;
        this.email = email;
        this.fullName = fullName;
        this.roles = roles;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return Map.of();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getName() {
        return this.username;
    }

    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getFullName() {
        return fullName;
    }
}
