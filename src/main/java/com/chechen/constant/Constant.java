package com.chechen.constant;

/**
 * @author thanglv on 17/05/2022
 * @project recruiter-api
 */
public interface Constant {
    Integer ACTIVE = 1;
    Integer DEACTIVATE = 0;
    String STR_Y = "Y";
    String STR_N = "N";
    interface USER_INFO {
        String REG_TYPE_COMPANY = "COMPANY";
        String REG_TYPE_HEADHUNT = "HEADHUNT";

        interface ATTRIBUTE {
            String PHONE = "phone";
            String COMPANY = "company";
        }
    }
    interface ROLE {
        String ADMIN = "admin";
        String USER = "user";
        String COMPANY = "company";
        String HEADHUNT = "headhunt";
    }

    interface AUTHORITY {
        String ADMIN = "ROLE_admin";
        String USER = "ROLE_user";
        String COMPANY = "ROLE_company";
        String HEADHUNT = "ROLE_headhunt";
    }

    interface REGISTRATION_TYPE {
        String COMPANY = "COMPANY";
        String HEADHUNT = "HEADHUNT";
    }

    interface JOB_INFO {
        interface JOB_TYPE {
            String IT = "IT";
            String NON_IT = "NON_IT";
            String HIGH_LEVEL = "HIGH_LEVEL";
            String ALL = "ALL";
        }
    }

    enum PENDING_TASK {
        APPROVED("APPROVED", "Đã phê duyệt"),
        PENDING_APPROVE("PENDING_APPROVE", "Đang chờ phê duyệt"),
        REJECTED("REJECTED", "Bị từ chối"),
        CLOSED("CLOSED", "Đã đóng");

        final String code;
        final String name;

        PENDING_TASK(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

    interface COMPANY_SIZE {
        int SIZE_1 = 1;
        int SIZE_2 = 2;
        int SIZE_3 = 3;
        int SIZE_4 = 4;
        int SIZE_5 = 5;
        int SIZE_6 = 6;
        int SIZE_7 = 7;
        int SIZE_8 = 8;
    }

    int DEFAULT_PRIORITY = 1;

    interface SYSTEM_PARAM {

    }

    interface PENDING_TASK_TYPE {
        String JOB = "JOB";
    }

    interface COMPANY_JOB_STATUS {
        String OPEN = "OPEN";
        String CLOSE = "CLOSE";
    }

    interface GENDER {
        String NONE = "NONE";
        String MALE ="MALE";
        String FEMALE = "FEMALE";
    }

    public static final String ANONYMOUS_USER = "anonymousUser";
}
