package com.chechen.constant;

public class UrlConstant {
    private UrlConstant() {
    }

    public static final String API_VERSION = "/api/v1";
    public static final String API_SKILL = "/skills";
    public static final String API_LEVEL = "/level";
    public static final String API_USER_INFO = "/user-info";
    public static final String API_LOCATION = "/location";
    public static final String API_JOB = "/job";
    public static final String API_FILE_MANAGEMENT = "/file-management";
    public static final String API_FILE_UPLOAD_POLICY = "/file-upload-policy";
    public static final String API_COMPANY = "/company";
    public static final String API_COMPANY_INDUSTRY = "/company-industry";
    public static final String API_COMPANY_BENEFIT = "/company-benefit";
    public static final String PENDING_TASK = "/pending-task";
    public static final String API_HEADHUNT = "/headhunt";
    public static final String API_JOB_ROLE = "/job-role";
    public static final String API_JOB_CATEGORY = "/job-category";
    public static final String API_LANGUAGE = "/language";
    public static final String CV = "/cv";

}
