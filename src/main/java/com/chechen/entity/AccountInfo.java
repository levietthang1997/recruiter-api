package com.chechen.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author thanglv on 09/06/2022
 * @project recruiter-api
 * account đồng bộ sang keycloak
 */
@Table(name = "account_info", indexes = {
        @Index(name = "account_info_idx_wf_ username", columnList = "username"),
        @Index(name = "account_info_idx_email", columnList = "email")
})
@Indexed(index = "account_info")
@Entity
@Data
public class AccountInfo implements Serializable {
    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    @Column(length = 200)
    private String username;

    // email này là email login không thể đổi được
    @Column(length = 200)
    private String email;

    @Column(length = 15)
    private String phone;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_account_company_id")
    private CompanyProfile companyProfile;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_account_headhunt_id")
    private HeadhuntProfile headhuntProfile;

    @Column(nullable = false)
    private String userType;

    @Column(nullable = false)
    private String isDeleted;

    @Column(length = 100, nullable = false)
    private String keycloakUserId;
}
