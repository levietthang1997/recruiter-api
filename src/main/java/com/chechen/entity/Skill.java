package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "skill")
@Indexed(index = "skill")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Skill extends SuperEntity implements Serializable {
    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    @FullTextField(analyzer = "ngram_analyzer")
    @Column(nullable = false, length = 250)
    private String name;

    @ManyToMany(mappedBy = "skills", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<JobInfo> jobInfos;

    @ManyToMany(mappedBy = "skills", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<HeadhuntBrand> headhuntBrands;

    @FullTextField(analyzer = "default_analyzer")
    @Column(name = "is_deleted", nullable = false)
    private String isDeleted;

}
