package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.GenericField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author thanglv on 08/06/2022
 * @project recruiter-api
 */
@Indexed(index = "company_profile")
@Entity
@Table(name = "company_profile")
@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
public class CompanyProfile extends SuperEntity {

    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    // tên công ty
    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 500)
    private String companyName;

    // điện thoại công ty
    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 20, unique = true)
    private String companyPhone;

    // địa chỉ
    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 500)
    private String companyAddress;

    // quy mô nhân sự
    private Integer companySize;

    // business email công ty, email này không hề liên quan tới email của user lúc đăng ký, email user đăng ký thì không thể sửa
    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 100)
    private String companyEmail;

    // cc mail
    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 100)
    private String ccMail;

    // website công ty
    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 100)
    private String website;

    // ngành nghề kinh doanh của công ty
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "company_profile_industry", joinColumns = {
            @JoinColumn(name = "fk_profile_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_industry_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    @BatchSize(size=20)
    private Set<Industry> companyIndustries = new HashSet<>();

    // Số giờ làm việc trong ngày
    @GenericField
    private Integer workingHours;

    // phúc lợi của công ty
    @IndexedEmbedded
    @OneToMany(mappedBy = "companyProfile", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @BatchSize(size=20)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<CompanyBenefitDetail> benefitDetails = new HashSet<>();

    @OneToOne(mappedBy = "companyProfile", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private AccountInfo accountInfo;

    // logo công ty
    private String fileLogoId;

    @Column(length = 2000)
    private String viewLinkLogo;

    // mô tả qua về công ty
    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 1000)
    private String description;

    // gallery ảnh công ty
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    @BatchSize(size=20)
    private Set<FileManagement> gallery = new HashSet<>();

    // ảnh hiển thị thumbnail lúc xem chi tiết công ty (chạy ở slider)
    // cái này dùng để hiển thị cho phần share ở face book. Khi share thì bài viết
    // face book sẽ có ảnh công ty, Tên công ty (title bold) và description công ty
    @Column(length = 1000)
    private String thumbnailLogoId;

    @Column(length = 1000)
    private String viewLinkThumbnail;

    @Column(length = 1000)
    private String thumbnailDescription;

    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 1000)
    private String linkedIn;

    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 1000)
    private String facebook;

    // thông tin văn phòng đại diện của công ty
    @Column(length = 200)
    @FullTextField(analyzer = "ngram_analyzer")
    private String firstNameRepresentative;

    @Column(length = 200)
    @FullTextField(analyzer = "ngram_analyzer")
    private String lastNameRepresentative;

    @Column(length = 1000)
    private String headline;

    // email này không liên quan tới email user đăng ký, email user đăng ký thì không thể sửa
    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 200, nullable = false)
    private String emailRepresentative;

    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 20, nullable = false)
    private String phoneRepresentative;

    @OneToMany(mappedBy = "companyProfile", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @BatchSize(size=20)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<JobInfo> jobs = new HashSet<>();
}
