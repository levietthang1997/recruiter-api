package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * @author thanglv on 09/06/2022
 * @project recruiter-api
 * bảng này để lưu lại thông tin của cv
 */
@Table(name = "cv_information")
@Indexed(index = "cv_information")
@Entity
@Data
@NoArgsConstructor
public class CvInformation {

    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    // họ ứng viên
    private String firstName;

    // tên ứng viên
    private String lastName;

    // email ứng viên
    private String email;

    // số điện thoại ứng viên
    private String phone;

    // ngày sinh nhật ứng viên
    private Date birthDay;

    // website ứng viên
    @Column(length = 1000)
    private String website;

    // tiêu đề hiển thị ở cv
    @Column(length = 1000)
    private String displayTitle;

    // khu vực tuyển dụng
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "cv_location", joinColumns = {
            @JoinColumn(name = "fk_cv_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_location_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<Location> locations = new HashSet<>();

    // Danh mục công việc (ở màn upload cv: UI/UX, backend dev, frontend dev, teacher)
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "cv_job_category", joinColumns = {
            @JoinColumn(name = "fk_cv_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_job_category_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<JobCategory> jobCategories = new HashSet<>();

    // thời gian bắt đầu làm
    private String candidateAvailability;

    // Loại công việc (IT hay non IT)
    private String jobType;

    // Có thoả thuận mức lương hay không , Y là có, N là Không
    @FullTextField(analyzer = "default_analyzer")
    private String isNegotiableSalary;

    // lương thoả thuận cao nhất
    private BigDecimal salaryExpectMax;

    // lương thoả thuận thấp nhất
    private BigDecimal salaryExpectMin;

    // sơ lược về cv
    private String summary;
    // fileId của cv gốc
    private String fileIdCvOriginal;
    // fileId của cv bị ẩn thông tin
    private String fileIdCvHidden;
}
