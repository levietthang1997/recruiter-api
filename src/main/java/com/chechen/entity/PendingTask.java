package com.chechen.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * @author thanglv on 08/06/2022
 * @project recruiter-api
 */
@Table(name = "pending_task", indexes = {
        @Index(name = "pending_task_idx_wf_status_code", columnList = "wfStatusCode"),
        @Index(name = "pending_task_idx_string1", columnList = "string1"),
        @Index(name = "pending_task_idx_approve_by", columnList = "approveBy"),
        @Index(name = "pending_task_idx_created_by", columnList = "createdBy"),
})
@Entity
@Data
@SuperBuilder
@EqualsAndHashCode
@NoArgsConstructor
public class PendingTask extends SuperEntity {

    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    // lưu job id cần phê duyệt
    private String string1;
    // Lưu tên job cần phê duyệt
    private String string2;
    // trạng thái mã code
    private String wfStatusCode;
    // người tạo
    private String createdBy;
    //người duyệt
    private String approveBy;
    // delete hay chưa
    @Column(name = "is_deleted", nullable = false)
    private String isDelete;

    private String type;
}
