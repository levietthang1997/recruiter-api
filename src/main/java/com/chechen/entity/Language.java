package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Table(name = "language")
@Entity
@Data
@SuperBuilder
@EqualsAndHashCode
@NoArgsConstructor
public class Language extends SuperEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;
    // KR, VI
    private String code;
    // Korean, Vietnamese
    private String name;

    @ManyToMany(mappedBy = "headhuntBrandLanguage", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    @BatchSize(size = 20)
    private Set<HeadhuntBrand> headhuntBrands;

    @FullTextField(analyzer = "default_analyzer")
    @Column(name = "is_deleted", nullable = false)
    private String isDeleted;
}
