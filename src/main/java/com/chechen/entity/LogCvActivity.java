package com.chechen.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author thanglv on 09/06/2022
 * @project recruiter-api
 * bảng này để lưu lại trạng thái của một cv từ khí bắt đầu khởi tạo cho tới khi cv được trả tiền
 */
//@Table(name = "log_cv_activity")
//@Entity
@Data
@NoArgsConstructor
public class LogCvActivity implements Serializable {
    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    private String cvId;

    // mã trạng thái của giao dịch
    @Column(nullable = false)
    private String activityCd;

    // tên trạng thái của giao dịch
    private String activityNm;

    // lưu lại file upload
    private FileManagement fileManagement;
}
