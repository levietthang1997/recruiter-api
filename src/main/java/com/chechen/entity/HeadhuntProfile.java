package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Entity
@Table(name = "headhunt_profile")
@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
public class HeadhuntProfile extends SuperEntity {

    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    @Column(length = 100)
    private String firstName;

    @Column(length = 100)
    private String lastName;

    // điện thoại công ty
    @Column(length = 15)
    private String hotline;

    // địa chỉ
    @Column(length = 2000)
    private String address;

    @Column(length = 10)
    private String gender;

    @FullTextField(analyzer = "default_analyzer")
    @Column(length = 1000)
    private String linkedIn;

    // giới thiệu về hr (maxlength giới hạn api là 3000)
    @FullTextField(analyzer = "default_analyzer")
    @Column(
            name = "overview"
            , columnDefinition="TEXT"
    )
    private String introduction;

    private String avatarId;

    @Column(length = 2000)
    private String avatarUrl;

    @OneToOne(mappedBy = "headhuntProfile", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private AccountInfo accountInfo;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_headhunt_profile_brand_id")
    private HeadhuntBrand headhuntBrand;
}
