package com.chechen.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;

import javax.persistence.*;

/**
 * @author thanglv on 18/06/2022
 * @project recruiter-api
 */
@Entity
@Table(name = "file_upload_policy")
@Data
@NoArgsConstructor
@ToString
public class FileUploadPolicy extends SuperEntity {

    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    @Column(length = 50, nullable = false)
    private String ext;

    @Column(nullable = false)
    private Long maxUploadSize;
}
