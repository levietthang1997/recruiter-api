package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.engine.backend.types.Sortable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.GenericField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * @author thanglv on 26/03/2022
 * @project recruiter-api
 */
@Indexed(index = "job_info")
@Table(name = "job_info")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JobInfo extends SuperEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    // tìm kiếm bằng platform recruitery hoặc là không
    private String isPlatform;

    // Tên job (Tuyển CEO, Tuyển CTO...)
    @FullTextField(analyzer = "default_analyzer")
    @Column(name = "title", length = 1000)
    private String title;

    // kỹ năng required của job (C++, Java, ...)
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "job_skill", joinColumns = {
            @JoinColumn(name = "fk_job_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_skill_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<Skill> skills = new HashSet<>();

    // Có thoả thuận mức lương hay không , Y là có, N là Không
    @FullTextField(analyzer = "default_analyzer")
    private String isNegotiableSalary;

    // mức lương max
    @GenericField(sortable = Sortable.YES)
    @Column(name = "salary_to")
    private BigDecimal salaryTo;

    // mức lương min
    @GenericField(sortable = Sortable.YES)
    @Column(name = "salary_from")
    private BigDecimal salaryFrom;

    // số lượng vị trí còn trống muốn tuyển (đang trống 50 => muốn tuyển thêm 50)
    @GenericField
    private Integer vacancies;

    // số lượng thành viên của nhóm trong công ty hiện có
    @GenericField
    private Integer teamSize;

    // phòng ban
    @FullTextField(analyzer = "default_analyzer")
    @Column(length = 500)
    private String department;

    // loại job it, non it, high level
    @FullTextField(analyzer = "default_analyzer")
    private String jobType;

    // nơi làm việc (tỉnh thành phố) Hà Nội, Singapore...
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "job_location", joinColumns = {
            @JoinColumn(name = "fk_job_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_location_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<Location> locations = new HashSet<>();

    // địa chỉ chi tiết
    @FullTextField(analyzer = "default_analyzer")
    @Column(
            name = "address"
            , length = 1000
    )
    private String address;

    // mô tả qua về job
    @FullTextField(analyzer = "default_analyzer")
    @Column(
            name = "overview"
            , columnDefinition="TEXT"
    )
    private String jobOverView;

    // yêu cầu về kỹ năng
    @FullTextField(analyzer = "default_analyzer")
    @Column(
            name = "job_requirement"
            , columnDefinition="TEXT"
    )
    private String jobRequirement;

    // những kỹ năng được ưu tiên
    @FullTextField(analyzer = "default_analyzer")
    @Column(
            name = "priority_skill"
            , columnDefinition="TEXT"
    )
    private String prioritySkill;

    // lý do tại sao nên làm ở công ty
    @FullTextField(analyzer = "default_analyzer")
    @Column(
            name = "why_working_here"
            , columnDefinition="TEXT"
    )
    @JsonIgnore
    private String whyWorkingHere;

    // quá trình phỏng vấn
    @FullTextField(analyzer = "default_analyzer")
    @Column(name = "interviewing_process", columnDefinition="TEXT")
    private String interviewingProcess;

    // loại thời gian làm việc (fulltime, partime, other)
    @FullTextField(analyzer = "default_analyzer")
    @Column(name = "employee_type", length = 10)
    private String employeeType;

    // level
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "job_level", joinColumns = {
            @JoinColumn(name = "fk_job_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_level_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<Level> levels = new HashSet<>();

    // Thông báo cho headhunter
    @FullTextField(analyzer = "default_analyzer")
    @Column(
            name = "notice_to_headhunter"
            , columnDefinition="TEXT"
    )
    private String noticeToHeadhunter;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_company_profile_id")
    @IndexedEmbedded
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    @BatchSize(size = 20)
    private CompanyProfile  companyProfile;

    //  admin sẽ set độ ưu tiên của job để hiển thị lên trang chủ
    private Integer priorityJob;

    // chỉ hiển thị những job đã được duyệt khi tìm kiếm
    @FullTextField(analyzer = "default_analyzer")
    private String approveStatus;

    @FullTextField(analyzer = "default_analyzer")
    private String companyStatus;

    // company set trạng thái của job (close tìm kiếm hoặc là open)
    private String companyJobStatus;

    @Column(name = "work_flow_id", length = 100, unique = true)
    private String workFlowId;

    // tiền thưởng dành cho hr, sẽ set khi admin cấu hình
    @GenericField(sortable = Sortable.YES)
    private BigDecimal reward;

    @FullTextField(analyzer = "default_analyzer")
    @Column(name = "is_deleted", nullable = false)
    private String isDeleted;
}
