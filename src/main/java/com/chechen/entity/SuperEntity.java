package com.chechen.entity;

import com.chechen.constant.Constant;
import com.chechen.obj.Oauth2UserPrincipal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NoArgsConstructor
@SuperBuilder
public class SuperEntity {

    @Column(name = "create_by", length = 100)
    private String createBy;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @Column(name = "modify_by", length = 100)
    private String modifyBy;

    @Column(name = "modify_date")
    private LocalDateTime modifyDate;

    @PrePersist
    protected void onCreate() {
        this.createDate = LocalDateTime.now();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && !Constant.ANONYMOUS_USER.equalsIgnoreCase(authentication.getName())) {
            Oauth2UserPrincipal principal = (Oauth2UserPrincipal) authentication.getPrincipal();
            if (principal != null)
                this.createBy = principal.getUserId();
        }
    }

    @PreUpdate
    protected void onUpdate() {
        this.modifyDate = LocalDateTime.now();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && !Constant.ANONYMOUS_USER.equalsIgnoreCase(authentication.getName())) {
            Oauth2UserPrincipal principal = (Oauth2UserPrincipal) authentication.getPrincipal();
            if (principal != null)
                this.modifyBy = principal.getUserId();
        }
    }
}
