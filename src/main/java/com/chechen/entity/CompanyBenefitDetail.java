package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;

import javax.persistence.*;

/**
 * @author thanglv on 08/06/2022
 * @project recruiter-api
 */
@Indexed(index = "company_benefit_detail")
@Table(
        name = "company_benefit_detail",
        uniqueConstraints = {
        @UniqueConstraint(columnNames={"company_profile_id", "cp_benefit_id"})
})
@Entity
@Data
@NoArgsConstructor
public class CompanyBenefitDetail {

    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_profile_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private CompanyProfile companyProfile;

    @IndexedEmbedded
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cp_benefit_id")
    private CpBenefit benefit;

    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 2000)
    private String detail;
}
