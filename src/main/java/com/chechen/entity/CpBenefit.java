package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author thanglv on 08/06/2022
 * @project recruiter-api
 * bảng này lưu phúc lợi của công ty
 */
@Indexed(index = "cp_benefit")
@Entity
@Table(name = "cp_benefit")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
@ToString(callSuper = true)
public class CpBenefit extends SuperEntity {

    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    @OneToMany(mappedBy = "benefit")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<CompanyBenefitDetail> details = new HashSet<>();

    @Column(nullable = false, length = 500)
    @FullTextField(analyzer = "ngram_analyzer")
    private String name;

    private String icon;

    @Column(name = "is_deleted", nullable = false)
    private String isDeleted;
}
