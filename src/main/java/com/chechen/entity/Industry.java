package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import java.util.Set;

/**
 * @author thanglv on 08/06/2022
 * @project recruiter-api
 */
@Indexed(index = "industry")
@Entity
@Table(name = "industry")
@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
public class Industry extends SuperEntity {

    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 200, nullable = false)
    private String name;

    @ManyToMany(mappedBy = "companyIndustries", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    @BatchSize(size = 20)
    private Set<CompanyProfile> companyProfiles;

    @ManyToMany(mappedBy = "headhuntBrandIndustries", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    @BatchSize(size = 20)
    private Set<HeadhuntBrand> headhuntBrands;

    @Column(name = "is_deleted", nullable = false)
    private String isDeleted;
}
