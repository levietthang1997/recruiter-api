package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
/**
 * Bảng này dùng để thêm vào headhunt brand (vai trò của job mà headhunt biết, khác với job title)
 * Vai trò của ứng viên trong công việc đó
 */

@Table(name = "job_role")
@Entity
@Indexed(index = "job_role")
@Data
@EqualsAndHashCode
@NoArgsConstructor
public class JobRole implements Serializable {
    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    private String name;

    @ManyToMany(mappedBy = "jobRoles", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<HeadhuntBrand> headhuntBrand;

    @FullTextField(analyzer = "default_analyzer")
    @Column(name = "is_deleted", nullable = false)
    private String isDeleted;
}