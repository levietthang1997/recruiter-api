package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Table(name = "headhunt_brand")
@Entity
@Data
@SuperBuilder
@EqualsAndHashCode
@NoArgsConstructor
public class HeadhuntBrand implements Serializable {
    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    // mybrand
    private String brandLogoId;
    private String viewLinkBrandLogo;
    private String name;
    private String phone;
    private String teamLeadEmail;
    private String address;
    private String ccMail;

    // contact
    private String contactFirstName;
    private String contactLastName;
    private String contactTitle;
    private String contactPhone;
    private String contactEmail;

    // profile
    private String message;

    @OneToOne(mappedBy = "headhuntBrand", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private HeadhuntProfile headhuntProfile;

    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "headhunt_brand_job_role", joinColumns = {
            @JoinColumn(name = "fk_headhunt_brand_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_job_role_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<JobRole> jobRoles;

    // level
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "headhunt_brand_job_level", joinColumns = {
            @JoinColumn(name = "fk_headhunt_brand_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_level_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<Level> levels = new HashSet<>();

    // ngành nghề mà hr am hiểu
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "headhunt_brand_industry", joinColumns = {
            @JoinColumn(name = "fk_headhunt_brand_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_industry_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    @BatchSize(size=20)
    private Set<Industry> headhuntBrandIndustries = new HashSet<>();

    // kỹ năng mà hr am hiểu của job (C++, Java, ...)
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "headhunt_brand_skill", joinColumns = {
            @JoinColumn(name = "fk_headhunt_brand_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_skill_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<Skill> skills = new HashSet<>();

    // ngôn ngữ thành thạo
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "headhunt_brand_language", joinColumns = {
            @JoinColumn(name = "fk_headhunt_brand_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_industry_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    @BatchSize(size=20)
    private Set<Language> headhuntBrandLanguage = new HashSet<>();

    // khu vực tuyển dụng
    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "headhunt_brand_location", joinColumns = {
            @JoinColumn(name = "fk_headhunt_brand_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "fk_location_id")
    })
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<Location> locations = new HashSet<>();

    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 1000)
    private String linkedIn;

    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 1000)
    private String facebook;

    @FullTextField(analyzer = "ngram_analyzer")
    @Column(length = 1000)
    private String skype;
}
