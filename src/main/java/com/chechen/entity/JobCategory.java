package com.chechen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Bảng này để insert cùng với cv
 * Loại công việc khác với job role là vai trò của ứng viên khi làm công việc đó
 */
@Table(name = "job_category")
@Entity
@Indexed(index = "job_category")
@Data
@SuperBuilder
@EqualsAndHashCode
@NoArgsConstructor
public class JobCategory implements Serializable {
    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    private String name;

    @ManyToMany(mappedBy = "jobRoles", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<HeadhuntBrand> headhuntBrand;

    @ManyToMany(mappedBy = "jobCategories",fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    private Set<CvInformation> cvInformations;

    @FullTextField(analyzer = "default_analyzer")
    @Column(name = "is_deleted", nullable = false)
    private String isDeleted;
}
