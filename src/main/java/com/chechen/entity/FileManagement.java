package com.chechen.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author thanglv on 09/06/2022
 * @project recruiter-api
 */
@Table(name = "file_management")
@Entity
@Data
@NoArgsConstructor
public class FileManagement implements Serializable {
    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
    private String id;

    @Column(length = 500)
    private String name;
    private Long size;
    @Column(length = 1000)
    private String path;
    @Column(length = 10)
    private String fileExt;
    @Column(length = 100)
    private String contentType;
    private String createdBy;
    private Date createdDate;
    private String updateBy;
    private String updateDate;
    @Column(length = 1000)
    private String viewLink;
    private String isPublic;
    private String isDeleted;
}
