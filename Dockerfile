FROM openjdk:11.0.11-jre

ADD /target/recruiter-api.jar app.jar

ENTRYPOINT exec java -Xms128m -Xmx2048m -Dfile.encoding=utf-8 -jar app.jar
